#!flask/bin/python
from app import create_app
create_app(config_name='config').run(
    host='0.0.0.0',  # to make host publically available, add parameter: host='0.0.0.0'
    threaded=True
)
