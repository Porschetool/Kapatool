import os
import uuid

#!/usr/bin/env python
# Enable debug mode.
DEBUG = True
# Enable threaded mode
THREADED = True
# Enable HTML Minimization
MINIFY_PAGE = True

# Secret key for session management.
SECRET_KEY = WTF_CSRF_SECRET_KEY = 'pq3h8ph8athbldbsv072354b124naasd'  # str(uuid.uuid4())

# SQL Settings
# Path to the SQLite Database file.
# Options are: sqlite:///relative/path/to/file
# and sqlite:////absolute/path/to/file
SQLALCHEMY_DATABASE_URI = 'sqlite:///../porsche.db'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# JSON Settings
JSON_AS_ASCII = False  # enables umlauts to be shipped via JSON, http://stackoverflow.com/a/24403223

# File upload settings
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'tmp')
ALLOWED_EXTENSIONS = set(['zip'])  # ZIP Files necessary to update database
