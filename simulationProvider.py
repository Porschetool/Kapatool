from sqlalchemy import engine, create_engine
from datetime import date, datetime, timedelta
from flask import Flask

import random
import time
import Pyro4
import signal
import sys
import uuid
import traceback

from app import connection
from app.simulationmanagement import classes

# libraries necessary to update APOS
import os.path
import pandas as pd
import string
import zipfile

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# Create App
app = Flask(__name__)
app.config.from_object('config')
with app.app_context():
    app.config.update(SQLALCHEMY_DATABASE_URI='sqlite:///porsche.db')
    connection.init_app(app)
    print 'App context initialized'

    from app.db.models import *  # User, Group, Setting, Machine, Calendar, CalendarSubscription, RepeatingCalendarEntry, Job, Operation, Patch, PlanTime
    from app.simulationmanagement.dbToSimulationTranslation import *  # getShopCalendarSimObject, createCalendarFromDatabaseObject, createAvailabilitiesBasedOnDatabaseObject, createLaborResourcesFromDatabaseObject, createMachineResourcesFromDatabaseObject, createJobFromDatabaseObject, calculateRecommendedReleaseDatetime, calculateRecommendedEndDatetime

    # if this is a UNIX operating system, switch to the selected timezone
    if sys.platform == "linux" or sys.platform == "linux2":
        tz = Setting.query.get('timezone')
        if tz is not None:
            os.environ['TZ'] = 'Europe/Berlin'
        else:
            os.environ['TZ'] = tz.formattedValue
        time.tzset()

    @Pyro4.expose
    class simulationProvider(object):
        """
        Class instantiates a simulationManager locally and exposes through Pyro4
        an interface to interact with the simulation. It also provides the funcationality to
        instantiate the simulation model from the database.

        Parameters
        ----------
        name : string
            Name of the simulation provider in the Pyro4 nameserver
        daemon : Pyro4.dameon
        app_context : flask.app_context
        """
        def __init__(self, name, daemon, app_context):
            self._name = name
            self._daemon = daemon
            self._app_context = app_context

            self.startDate = date.today()
            self.endDate = date.today() + timedelta(weeks=8)

            self._shopCalendar = None
            self._simulationManager = None

            self._databaseObjectDict = {}  # dictionary to allow quick access to simulation and database objects by type and id

            self._startSimulation = True  # tell simulation to run once at startup
            self._stop = False  # set interrupt variable to False
            self._busy = False  # indicator variable if simulation is currently running
            self._statusMessage = 'Idling'
            self.startSimulation()

        def loadShopFromDatabase(self):
            """
            Helper function that loads all relevant entries from the database, converts them to simulation
            class instances and presents in the end in self._simulationManager and readily set up simulation
            manager instance
            """
            self._databaseObjectDict = {}  # dictionary to allow quick access to database objects by type and id

            # query for shop calendars
            self._shopCalendar = getShopCalendarSimObject(
                startDate=self.startDate,
                endDate=self.endDate
            )

            # get machines from database
            queryResult = Machine.query.all()
            self._databaseObjectDict['machine'] = {m.id: m for m in queryResult}
            machineResources = map(
                lambda m: createMachineResourcesFromDatabaseObject(m, self.startDate - timedelta(weeks=4), self.endDate),
                queryResult
            )

            # get user with calendars (users who can work in the shop) from database
            queryResult = User.query.filter(User.role == 'Technician').all()
            self._databaseObjectDict['labor'] = {u.id: u for u in queryResult}
            laborResources = map(
                lambda u: createLaborResourcesFromDatabaseObject(u, self.startDate - timedelta(weeks=4), self.endDate),
                queryResult
            )

            # create dictionaries of labor and machine resource sets, so we can instantiate jobs and patches
            laborResourceDict = {l.id: l for l in laborResources}
            machineResourceDict = {m.id: m for m in machineResources}

            # load unfinished jobs from database
            queryResult = Job.query.filter(Job.completionDatetime.is_(None)).all()
            self._databaseObjectDict['job'] = {j.id: j for j in queryResult}
            jobs = map(
                lambda j: createJobFromDatabaseObject(j, self._shopCalendar, laborResourceDict, machineResourceDict),
                queryResult
            )
            # filter out jobs that could not be converted (i.e. jobs for which no start date could be identified)
            # these are None values in the list
            jobs = list(
                filter(
                    lambda x: x is not None,
                    jobs
                )
            )

            self._databaseObjectDict['operation'] = {o.id: o for j in queryResult for o in j.operations}
            self._databaseObjectDict['patch'] = {p.id: p for j in queryResult for o in j.operations for p in o.patches}

            self._simulationManager = classes.simulationManager(
                laborResources,
                machineResources,
                jobs
            )

        def runSimulation(self):
            self._statusMessage = 'Running Simulation'
            while self._startSimulation:
                start = time.time()

                self._startSimulation = False  # do not run until explicitly told again
                self._busy = True
                # load simulation from database
                self.loadShopFromDatabase()

                print 'Simulation intialized after {} seconds'.format(time.time()-start)
                start = time.time()

                # run simulation
                print 'simulation starts at: {}'.format(datetime.now())
                simStartDatetime = datetime.now()
                self._simulationManager.startSimulation(
                    simStartDatetime,
                    simStartDatetime + timedelta(weeks=8),  # run for 8 weeks
                    loopCondition=lambda: not self._stop  # ensures that simulation can be stopped externally
                )

                end = time.time()
                print 'simulation finished after {} seconds'.format(end-start)
                # check if the simulation was stopped externally, if so, we ignore the results
                if not self._stop:
                    # Delete all patches from database, that were purely based on plantimes, no actual times
                    Patch.query.filter(Patch.startDatetime.is_(None)).delete()
                    # commit deletions to database
                    connection.session.commit()
                    # iterate through all patches in the patch dataframe
                    for simPatch in self._simulationManager.getPatches():
                        # if patch id is not None, patch already exists in database
                        if simPatch.id is not None:
                            # if only planEndDatetime is set in db object, update it
                            dbPatch = self._databaseObjectDict['patch'][simPatch.id]
                            if dbPatch.endDatetime is None:  # only overwrite planEndDatetimes, not already finished patches
                                # patch cannot end in the past (since it is not finished yet)
                                dbPatch.planEndDatetime = max(simPatch.endDatetime, simStartDatetime)
                        else:  # patch id is None -> simulation patch not equivalent to patch in database
                            # create new patch, setting planStartDatetime and planEndDatetime
                            dbPatch = Patch(
                                operation=self._databaseObjectDict['operation'][simPatch.operation.id],
                                planStartDatetime=simPatch.startDatetime.replace(microsecond=0),
                                planEndDatetime=simPatch.endDatetime.replace(microsecond=0),
                                user=self._databaseObjectDict['labor'][simPatch.laborResource.id],
                                machine=self._databaseObjectDict['machine'][simPatch.machineResource.id]
                            )
                            connection.session.add(dbPatch)
                    # calculate expected end dates (including post-processing) for all jobs
                    for j in self._simulationManager.jobs:
                        dbJob = self._databaseObjectDict['job'][j.id]
                        lastPatchEnd = j.endDatetime
                        if lastPatchEnd is not None:  # job was actually finished
                            dbJob.expectedCompletionDatetime = self._shopCalendar.addWorkingHoursTo(
                                lastPatchEnd,  # find end of latest patch
                                float(dbJob.postProcessingWorkContent)/100
                            ).replace(microsecond=0)
                        else:
                            dbJob.expectedCompletionDatetime = None
                    # store jobs back into database
                    connection.session.commit()

                print 'Results written to database in {} seconds'.format(time.time()-end)
                self._stop = False  # make sure we can run the next simulation properly

            # set indicator variable that simulation is running to False
            self._busy = False
            self._statusMessage = 'Idling'

        @Pyro4.oneway
        def startSimulation(self):
            try:
                with self._app_context:
                    print 'Simulation request received at {}'.format(datetime.now())
                    self._startSimulation = True  # tell simulation to run another round
                    self._stop = False  # re-set stop sign
                    if not self._busy:  # we are currently not in the simlation loop, so we have to start it manually
                        self.runSimulation()
                    return
            except:
                self._busy = False
                self._stop = False
                self._statusMessage = 'Error while running the simulation.'
                print "Exception leading to simulation crash: "
                print traceback.format_exc()
                return

        @Pyro4.oneway
        def updateAPOS(self, filePath):
            """
            Function updates APOS Database with information from ZIP file stored at filePath

            Parameters
            ----------
            filePath : str
            """
            def mapCarModelLineToDict(l):
                """
                Map a line in an Car Model File to a dictionary that contains the information
                from this line as a dictionary.

                Line widths from the APOS Handbook.

                Parameters
                ----------
                l : string

                Returns
                -------
                d : dictionary
                """
                return {
                    'modelType_id': l[:8],
                    'name': l[18:]
                }

            def mapAPOSLineToDict(l):
                """
                Map a line in an APOS File to a dictionary that contains the information
                from this line as a dictionary.

                Line widths from the APOS Handbook.

                Parameters
                ----------
                l : string

                Returns
                -------
                d : dictionary
                """
                return {
                    'modelType_id': l[:8],
                    'year': l[8:14],
                    'operationId': l[14:24],
                    'tu': l[24:31],
                    'description': l[53:]
                }
            try:
                with self._app_context:
                    self._stop = False
                    self._busy = True
                    # load ZIP File
                    archive = zipfile.ZipFile(filePath, 'r')
                    self._statusMessage = 'Reading car models and labor operations'
                    laborOperations = set()
                    for f in archive.filelist:
                        if f.filename.endswith('BTYP.TXT'):
                            lBTYP = map(
                                mapCarModelLineToDict,
                                archive.read(f.filename).decode('utf-8').splitlines()
                            )
                            # convert list to dataframe
                            dfBTYP = pd.DataFrame(lBTYP).groupby('modelType_id').agg({'name': lambda x: x.iloc[0]}).reset_index()
                            # strip entries of leading/trailing spaces
                            for c in dfBTYP.columns.values:
                                dfBTYP[c] = dfBTYP[c].str.strip()
                        elif f.filename.endswith('.TXT'):
                            # extend set of labor operations
                            lAPOS = map(
                                mapAPOSLineToDict,
                                archive.read(f.filename).decode('utf-8').splitlines()
                            )
                            laborOperations = laborOperations.union([
                                (e['operationId'].strip(), e['description'].strip())
                                for e in lAPOS
                            ])

                    if self._stop:  # stop when process was halted
                        self._busy = False
                        self._statusMessage = 'Idling'
                        return

                    # updating car models
                    self._statusMessage = 'Updating Car Models'
                    map(
                        lambda d: connection.session.merge(
                            CarModel(
                                modelType=d['modelType_id'],
                                name=d['name']
                            )
                        ),
                        dfBTYP.to_dict('records')
                    )
                    del dfBTYP

                    if self._stop:  # stop when process was halted
                        self._busy = False
                        self._statusMessage = 'Idling'
                        return

                    # update Labor Operations by iterating over the set of tuples created above
                    self._statusMessage = 'Updating Labor Operations'
                    map(
                        lambda d: connection.session.merge(
                            LaborOperation(
                                operationId=d[0],
                                name=d[1]
                            )
                        ),
                        laborOperations
                    )
                    connection.session.commit()
                    del laborOperations

                    if self._stop:  # stop when process was halted
                        self._busy = False
                        self._statusMessage = 'Idling'
                        return

                    # update plan times
                    PlanTime.query.delete()
                    self._statusMessage = 'Updating Plan Times'
                    for f in archive.filelist:
                        if f.filename.endswith('.TXT') and not f.filename.endswith('BTYP.TXT'):
                            self._statusMessage = 'Updating Plan Times from {}'.format(f.filename)
                            # convert file contents to dataframe
                            df = pd.DataFrame(map(
                                mapAPOSLineToDict,
                                archive.read(f.filename).decode('utf-8').splitlines()
                            ))
                            # clean columns
                            for c in df.columns.values:
                                df[c] = df[c].str.strip()
                            # filter out entries where workcontent is not defined
                            df = df[df.tu != '']
                            # Convert relevant fields to int
                            df.year = pd.to_numeric(df.year, errors='coerce')
                            df.tu = pd.to_numeric(df.tu, errors='coerce')
                            # drop all NaN values that were created during the type conversion
                            df = df.dropna()

                            length = len(df)
                            i = 0
                            while i < length and not self._stop:
                                # get end of slice
                                end = min([i+10000, length])
                                # map dictionaries to Database Objects
                                map(
                                    lambda d: connection.session.add(
                                        PlanTime(
                                            carModel_modelType=d['modelType_id'],
                                            year=d['year'],
                                            laborOperation_operationId=d['operationId'],
                                            workContent=d['tu']
                                        )
                                    ),
                                    df.iloc[i:end].to_dict('records')
                                )
                                connection.session.commit()
                                i = end
                    del df
                    self._busy = False
                    self._statusMessage = 'Idling'
                    return
            except:
                print sys.exc_info()
                self._busy = False
                self._stop = False
                self._statusMessage = 'Error while updating APOS.'

        @Pyro4.oneway
        def stop(self):
            print 'Process stopped'
            self._stop = True
            return

        @Pyro4.oneway   # in case call returns much later than daemon.shutdown
        def shutdown(self):
            self.stop()
            print "shutting down"
            Pyro4.locateNS().remove(self._name)
            self._daemon.shutdown()
            return

        @property
        def name(self):
            return self._name

        @property
        def status(self):
            return self._statusMessage

        @property
        def busy(self):
            return self._busy

    if __name__ == "__main__":
        with Pyro4.Daemon() as daemon:
            name = 'provider.' + str(uuid.uuid4())[:8]
            print "Starting up {}".format(name)
            mySimulationProvider = simulationProvider(
                name,
                daemon,
                app_context=app.app_context()
            )
            print 'Registering with nameserver.'
            uri = daemon.register(mySimulationProvider)
            with Pyro4.locateNS() as ns:
                ns.register(name, uri)
            print 'Starting request loop.'
            daemon.requestLoop()
        # once the request loop is finished, stop the program
        sys.exit(0)
