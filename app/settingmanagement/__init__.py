from flask import Blueprint

settingmanagement = Blueprint(
    'settingmanagement',
    __name__,
    template_folder='templates',
    static_folder='static'
)

from . import views

