#!/usr/bin/env python
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, FieldList, FormField
from wtforms.validators import DataRequired, EqualTo, Length

from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField, QuerySelectField
from flask_admin.form.widgets import Select2Widget


class IndividualSettingForm(FlaskForm):
    id = HiddenField('id')
    label = HiddenField('label')
    value = StringField('Value', validators=[DataRequired()])

