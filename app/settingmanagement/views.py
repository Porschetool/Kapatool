from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from flask_login import login_required, login_user, logout_user, current_user

from . import settingmanagement
from forms import *

from app import connection
from app.db.models import Setting
from app.simulationmanagement import simulationConnector


@settingmanagement.route("/", methods=['GET', 'POST'])
@settingmanagement.route("/index", methods=['GET', 'POST'])
@login_required
def index():
    if not current_user.is_admin:
        flash("The requsted page is only available for users for admin rights.", 'danger')
        return redirect(url_for('index'))

    forms = [IndividualSettingForm(obj=s) for s in Setting.query.all()]
    return render_template(
        'forms/settings.edit.html',
        forms=forms
    )


@settingmanagement.route("/setting/<settingId>/edit", methods=['GET', 'POST'])
@login_required
def editSetting(settingId):
    if not current_user.is_admin:
        flash("The requsted page is only available for users for admin rights.", 'danger')
        return redirect(url_for('index'))

    # check if we received any form and update the setting
    form = IndividualSettingForm()
    if form.validate_on_submit():
        setting = Setting.query.get(form.id.data)
        form.populate_obj(setting)
        connection.session.commit()
        flash("{} changed.".format(form.label.data), 'success')
    else:
        flash("Setting could not be changed.", 'danger')
    return redirect(url_for('settingmanagement.index'))
