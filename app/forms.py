#!/usr/bin/env python
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, EqualTo, Length
# Set your classes here.


class LoginForm(FlaskForm):
    name = StringField(
        'Username',
        [DataRequired()]
    )
    password = PasswordField(
        'Password',
        [DataRequired()]
    )
