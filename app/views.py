from flask import Flask, Blueprint, request, session, g, redirect, url_for, abort, render_template, flash, current_app
from flask_login import login_required, login_user, logout_user, current_user
from forms import *
from app import backup_database, connection
from app.db.models import *
import urlparse
import datetime
import os
import shutil


def is_safe_url(target):
    """
    Check if passed target is a safe URL, to protect app from open redirect attacks.
    Source: http://flask.pocoo.org/snippets/62/

    Parameters
    ----------
    target : string

    Returns
    -------
    d : boolean
        Decision
    """
    ref_url = urlparse.urlparse(request.host_url)
    test_url = urlparse.urlparse(urlparse.urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


# create blueprint for the main views
main = Blueprint('main', __name__)


@main.route('/')
@main.route('/index')
@login_required
def index():
    if current_user.name == 'admin' and current_user.check_password('password'):
        flash('Please change your user credentials!', 'danger')
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))
    elif len(CarModel.query.all()) == 0 and current_user.is_admin:  # it seems we have APOS Data
        flash('It appears that no APOS data has been uploaded. You may do so now.', 'warning')
        return redirect(url_for('apomanagement.updateAPOS'))
    else:
        return redirect(url_for('shopfloormanagement.index'))


@main.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter(User.name==form.name.data).first()
        print user.pw_hash
        if not (user is not None and user.check_password(form.password.data)):
            flash('A user with this username and password could not be found.', 'warning')
            return render_template('forms/login.html', form=form)

        # User successfully logged in
        login_user(user)
        flash('Logged in successfully.', 'success')

        next = request.args.get('next')
        # is_safe_url should check if the url is safe for redirects.
        # See http://flask.pocoo.org/snippets/62/ for an example.
        if not is_safe_url(next):
            return abort(400)
        return redirect(next or url_for('main.index'))

    # No form submit was detected -> load login form
    return render_template('forms/login.html', form=form)


@main.route("/logout")
@login_required
def logout():
    logout_user()
    flash('Logged out successfully.', 'success')
    return redirect(url_for('main.login'))


@main.route('/backup')
@login_required
def backup():
    backup_database()
    return redirect(url_for('main.index'))


@main.route('/restore', defaults={'databasename': None})
@main.route('/restore/<databasename>')
@login_required
def restore(databasename):
    if not current_user.is_admin:
        flash('You are not allowed to perform this operation.', 'danger')
        return redirect(url_for('main.index'))
    # Make sure we do not access random files.
    if databasename is not None and not (databasename.startswith('porsche_') and databasename.endswith('.db')):
        flash('Invalid database filename.', 'danger')
        return redirect(url_for('main.restore'))
    # Check if a database name was specified
    if databasename is None or not os.path.isfile(os.path.dirname(__file__) + '/../' + databasename):
        databases = sorted([
            f
            for f in os.listdir(os.path.dirname(__file__) + '/../')
            if f.startswith('porsche_')
            and f.endswith('.db')
        ])
        return render_template('pages/restore.html', databases=databases)
    else:  # Valid path was defined
        if not (databasename.startswith('porsche_') and databasename.endswith('.db')):
            flash('Invalid database filename.', 'danger')
            return redirect(url_for('main.restore'))
        # Else:
        shutil.copy2(os.path.dirname(__file__) + '/../' + databasename, os.path.dirname(__file__) + '/../porsche.db')
        flash('Restored from database {}'.format(databasename), 'success')
        return redirect(url_for('main.backup'))


@main.route('/shutdown')
@login_required
def shutdown():
    if not current_user.is_admin:
        flash("You are not allowed to perform this operation.", 'danger')
        return redirect(url_for('main.index'))
    func = request.environ.get('werkzeug.server.shutdown')
    print func
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'
