from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify, Response, json
from flask_login import login_required, login_user, logout_user, current_user
from app import connection
from app.db.models import User
from . import api


@api.route(
    "/api",
    methods=['GET']
)
@api.route("/technicians", methods=['GET'])
def getTechnicians():
    technicians = User.query.filter(User.role == 'Technician').all()
    techniciansJson = []
    for t in technicians:
        techniciansJson.append({'id' : t.id, 'name' : t.name})
    
    return jsonify(techniciansJson)