from flask import Blueprint

api = Blueprint(
    'api',
    __name__,
    static_folder='static'
)

from . import patches, technicians, machines

#Todo: move the whole module to appropriate ones (remove this one afterwards). Because there exist api methods here and there, no need to extract them to one place.