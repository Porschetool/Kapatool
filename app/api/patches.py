from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify, Response, json
from flask_login import login_required, login_user, logout_user, current_user
from app import connection
from app.db.models import Patch
from . import api

#Todo: make a proper RESTful service. Change put method to accept the whole patch.
@api.route(
    "/api",
    methods=['PUT']
)
@api.route("/patches/<int:patchId>/technician", methods=['PUT'])
def putTechnician(patchId):
    patchToEdit = json.loads(request.data)
    technicianUserId = patchToEdit['technicianUserId']

    dbPatchToEdit = Patch.query.get(patchId)
    dbPatchToEdit.user_id = technicianUserId

    connection.session.commit()

    return Response(response = 'Technician has been changed', status=200)

@api.route("/patches/<int:patchId>/machine", methods=['PUT'])
def putMachine(patchId):
    patchToEdit = json.loads(request.data)
    machineId = patchToEdit['machineId']

    dbPatchToEdit = Patch.query.get(patchId)
    dbPatchToEdit.machine_id = machineId

    connection.session.commit()

    return Response(response = 'Machine has been changed', status=200)