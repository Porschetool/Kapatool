from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify, Response, json
from flask_login import login_required, login_user, logout_user, current_user
from app import connection
from app.db.models import Machine
from . import api


@api.route(
    "/api",
    methods=['GET']
)
@api.route("/machines", methods=['GET'])
def getMachines():
    machines = Machine.query.all()
    machinesJson = []
    for m in machines:
        machinesJson.append({'id' : m.id, 'name' : m.name})
    
    return jsonify(machinesJson)