from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from flask_login import login_required, login_user, logout_user, current_user

from . import machinemanagement
from forms import *

from app import connection
from app.db.models import Machine, LaborOperation
from app.simulationmanagement import simulationConnector


@machinemanagement.route("/")
@machinemanagement.route("/index")
@login_required
def index():
    return render_template(
        'pages/machine.index.html',
        machines=Machine.query.all()
    )


@machinemanagement.route("/machine/add", methods=['GET', 'POST'])
@login_required
def addMachine():
    if current_user.role not in ['Manager']:
        flash('You are not allowed to add machines.', 'danger')
        return redirect(url_for('machinemanagement.index'))
    form = MachineForm()
    if form.validate_on_submit():
        machine = Machine()
        form.populate_obj(machine)
        connection.session.add(machine)
        connection.session.commit()

        flash('New Machine {} created.'.format(machine.name), 'success')
        # start simulation to reflect changes
        simulationConnector.startSimulation()
        return redirect(url_for('machinemanagement.index'))

    return render_template(
        'forms/machine.add.html',
        form=form
    )


@machinemanagement.route("/machine/edit/<int:machineNumber>", methods=['GET', 'POST'])
@login_required
def editMachine(machineNumber):
    if current_user.role not in ['Manager']:
        flash('You are not allowed to edit this machine.', 'danger')
        return redirect(url_for('machinemanagement.index'))
    # retrieve machine from database
    machine = Machine.query.get(machineNumber)
    # load form
    form = MachineForm(obj=machine)
    if form.validate_on_submit():
        form.populate_obj(machine)
        connection.session.commit()
        # start simulation to reflect changes
        simulationConnector.startSimulation()
        flash('Machine {} edited.'.format(machine.name), 'success')

    # create list of all covered APOS
    coveredAPOS = filter(
        lambda a: machine.canPerformAPOS(a.operationId),
        LaborOperation.query.all()
    )

    # prepopulate form fields before rendering form
    return render_template(
        'forms/machine.edit.html',
        form=form,
        form_machine_id=machineNumber,
        coveredAPOS=coveredAPOS
    )
