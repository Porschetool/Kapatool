from flask import Blueprint

machinemanagement = Blueprint(
    'machinemanagement',
    __name__,
    template_folder='templates',
    static_folder='static'
)

from . import views

