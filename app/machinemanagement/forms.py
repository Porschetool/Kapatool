#!/usr/bin/env python
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, DecimalField, TextAreaField, IntegerField
from wtforms.validators import DataRequired, EqualTo, Length

from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField, QuerySelectField
from flask_admin.form.widgets import Select2Widget


class MachineForm(FlaskForm):
    name = StringField(
        'Name',
        validators=[DataRequired()],
        description='Name of workplace. For identification only.'
    )
    setupTime = IntegerField(
        'Setup Time',
        description='Setup time in minutes, applied whenever new job comes to this workplace.'
    )
    APOSRegEx = TextAreaField('Possible APOS')
