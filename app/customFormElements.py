#!/usr/bin/env python
from wtforms.widgets.core import HTMLString, html_params, escape
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import StringField, SelectField


class AJAXQuerySelectWidget(object):
    def __call__(self, field, **kwargs):
        ajax = kwargs.pop("ajax") if "ajax" in kwargs.keys() else field.ajax
        assert ajax is not None, 'No Ajax source defined for {}'.format(field.name)
        # to be able to pass attributes with minus signs, we have to create a dictionary first, http://stackoverflow.com/a/30407912
        kwargs.update({
            'name': field.name,
            'data-ajax--url': ajax,
            'id': kwargs["id_"]
        })
        if 'class_' in kwargs.keys():
            kwargs['class_'] += ' select2-ajax'
        else:
            kwargs['class_'] = 'select2-ajax'
        # when field allows multiple selection, then preset it here
        if field.__class__.__name__ == "SelectMultipleField":
            params["multiple"] = ""
        html = [u'<select %s>' % html_params(**kwargs)]
        #   html.append(u'<option selected>%s</option>' % escape(field.__class__.__name__))
        html.append(u'</select>')
        return HTMLString(u''.join(html))


class Select2AJAXSelectField(QuerySelectField):
    """
    Thin layer around the AJAXQuerySelectWidget to make sure, form data is parsed correctly.

    Based on:
    * http://flask-admin.readthedocs.io/en/v1.4.2/_modules/flask_admin/form/fields/#Select2Field
    * https://gist.github.com/imwilsonxu/1e7343426135b4a34ce78574012a8f62
    """
    def __init__(self, *args, **kwargs):
        self.ajax = kwargs.pop("ajax") if "ajax" in kwargs.keys() else None  # retrieve and remove key
        self.model = kwargs.pop("model")  # retrieve and remove key
        self.widget = AJAXQuerySelectWidget()
        # call parent constructor
        super(Select2AJAXSelectField, self).__init__(
            *args, **kwargs
        )

    def pre_validate(self, form):
        # Prevent "not a valid choice" error
        pass

    def process_formdata(self, value):
        if value is None or len(value) == 0:
            self.data = None
        else:
            self.data = self.model.query.get(value[0])

    def default(self):
        return ""





