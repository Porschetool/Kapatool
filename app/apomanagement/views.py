from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify
from flask_login import login_required, login_user, logout_user, current_user

from . import apomanagement

from flask import current_app
from app import connection
from app.simulationmanagement import simulationConnector

from forms import *
import os


@apomanagement.route("/")
@apomanagement.route("/index")
@login_required
def index():
    if not current_user.is_admin:
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))

    return render_template(
        'pages/apomanagement.index.html'
    )


@apomanagement.route('/update', methods=['GET', 'POST'])
@login_required
def updateAPOS():
    if not current_user.is_admin:
        return redirect(url_for('index'))
    form = APOSUploadForm()
    if form.validate_on_submit():
        f = form.zipFile.data
        filename = secure_filename(f.filename)
        filepath = os.path.join(
            current_app.config['UPLOAD_FOLDER'],  filename
        )
        f.save(filepath)
        simulationConnector.updateAPOS(filepath)  # update could be started
        flash('APOS Update initiated. Please check the status message in the overview to check process.', 'success')
        return redirect(url_for('simulationmanagement.index'))

    return render_template('forms/apos.update.html', form=form)


@apomanagement.route("/carmodel/")
@apomanagement.route("/carmodel/index")
@login_required
def indexCarModel():
    if not current_user.is_admin:
        return redirect(url_for('index'))

    cars = CarModel.query.all()
    return render_template(
        'pages/carmodel.index.html',
        carModels=cars
    )


@apomanagement.route("/carmodel/add", methods=['GET', 'POST'])
@login_required
def addCarModel():
    if not current_user.is_admin:
        return redirect(url_for('index'))
    form = CarModelAddForm()
    if form.validate_on_submit():
        carModel = CarModel()
        form.populate_obj(carModel)

        connection.session.add(carModel)
        connection.session.commit()

        flash('New Car Model {} created.'.format(carModel.name), 'success')

        return redirect(url_for('apomanagement.editCarModel', modelType=carModel.modelType))

    return render_template(
        'forms/carmodel.add.html',
        form=form
    )


@apomanagement.route("/carmodel/edit/<modelType>", methods=['GET', 'POST'])
@login_required
def editCarModel(modelType):
    if not current_user.is_admin:
        flash('You are not allowed to view this page.', 'warning')
        return redirect(url_for('main.index'))
    carModel = CarModel.query.get(modelType)
    form = CarModelEditForm(obj=carModel)
    del form.modelType  # form element not present, so do not take into account for validation
    if form.validate_on_submit():
        del form.planTimes  # should not be changed through this form

        form.populate_obj(carModel)
        connection.session.commit()

        flash('Car Model {} edited.'.format(carModel.name), 'success')

        return redirect(url_for('apomanagement.indexCarModel'))

    return render_template(
        'forms/carmodel.edit.html',
        form=form,
        item=carModel,
        entryAddForm=PlanTimeForm(),
        form_model_type=modelType
    )


@apomanagement.route("/carmodel/api", methods=['GET'])
@login_required
def apiCarModel():
    q = request.args.get('q', None, type=str)
    page = request.args.get('page', 1, type=int)
    if q is not None:
        items = CarModel.query.filter(CarModel.name.like("%" + q + "%")).paginate(page, 20, False)
    else:
        items = CarModel.query.paginate(page, 20, False)
    # translate result set into jsonifiable list
    itemsToReturn = [
        {'id': l.modelType, 'name': l.modelType + ': ' + l.name}
        for l in
        items.items
    ]
    return jsonify({
        'items': itemsToReturn,
        'total_count': items.total
    })


@apomanagement.route("/carmodel/api/byyear/<int:year>", methods=['GET'])
@login_required
def apiCarModelByYear(year):
    q = request.args.get('q', None, type=str)
    page = request.args.get('page', 1, type=int)

    if q is not None:
        items = CarModel.query.filter(
            CarModel.name.like("%" + q + "%")
        ).filter(
            CarModel.modelType.in_(
                PlanTime.query.filter(
                    PlanTime.year == year
                ).with_entities(
                    PlanTime.carModel_modelType
                )
            )
        ).paginate(page, 20, False)
    else:
        items = CarModel.query.filter(
            CarModel.modelType.in_(
                PlanTime.query.filter(
                    PlanTime.year == year
                ).with_entities(
                    PlanTime.carModel_modelType
                )
            )
        ).paginate(page, 20, False)
    # translate result set into jsonifiable list
    itemsToReturn = [
        {'id': l.modelType, 'name': l.modelType + ': ' + l.name}
        for l in
        items.items
    ]
    return jsonify({
        'items': itemsToReturn,
        'total_count': items.total
    })

@apomanagement.route("/api/availableyears/<carModelType>", methods=['GET'])
@login_required
def apiYearsByCarModel(carModelType):
    availableYears = connection.session.query(PlanTime.year).filter(PlanTime.carModel_modelType == carModelType).distinct().all()
    itemsToReturn = [year[0] for year in availableYears]
    
    return jsonify(
        itemsToReturn
    )


@apomanagement.route("/laboroperation/index")
@login_required
def indexLaborOperation():
    if not current_user.is_admin:
        return redirect(url_for('index'))

    return render_template(
        'pages/laboroperation.index.html',
        operations=LaborOperation.query.all()
    )


@apomanagement.route("/laboroperation/add", methods=['GET', 'POST'])
@login_required
def addLaborOperation():
    if not current_user.is_admin:
        return redirect(url_for('index'))
    form = LaborOperationAddForm()
    if form.validate_on_submit():
        laborOperation = LaborOperation()
        form.populate_obj(laborOperation)

        connection.session.add(laborOperation)
        connection.session.commit()

        flash('Labor Operation {} created.'.format(laborOperation.name), 'success')

        return redirect(url_for('apomanagement.indexLaborOperation'))

    return render_template(
        'forms/laboroperation.add.html',
        form=form
    )


@apomanagement.route("/laboroperation/api", methods=['GET'])
@login_required
def apiLaborOperation():
    q = request.args.get('q', None, type=str)
    page = request.args.get('page', 1, type=int)
    if q is not None:
        items = LaborOperation.query.filter(LaborOperation.name.like("%" + q + "%")).paginate(page, 20, False)
    else:
        items = LaborOperation.query.paginate(page, 20, False)
    # translate result set into jsonifiable list
    itemsToReturn = [
        {'id': l.operationId, 'name': l.operationId + ': ' + l.name}
        for l in
        items.items
    ]
    return jsonify({
        'items': itemsToReturn,
        'total_count': items.total
    })


@apomanagement.route("/plantime/add/<modelType>", methods=['POST'])
@login_required
def addPlanTime(modelType):
    if not current_user.role != 'Manager':
        flash('You are not allowed to view this page.', 'warning')
        return redirect(url_for('index'))
    form = PlanTimeForm()
    if form.validate_on_submit():
        planTime = PlanTime()
        form.populate_obj(planTime)
        planTime.carModel = CarModel.query.get(modelType)

        connection.session.add(planTime)
        connection.session.commit()

        flash('Plan time added.', 'success')

        return redirect(url_for('apomanagement.editCarModel', modelType=planTime.carModel.modelType))

    return render_template(
        'forms/laboroperation.add.html',
        form=form
    )


@apomanagement.route("/plantime/delete/<int:planTimeNumber>", methods=['GET'])
@login_required
def deletePlanTime(planTimeNumber):
    if not current_user.role != 'Manager':
        flash('You are not allowed to view this page.', 'warning')
        return redirect(url_for('index'))
    planTime = PlanTime.query.get(planTimeNumber)
    carModel = planTime.carModel.modelType
    connection.session.delete(planTime)
    connection.session.commit()

    flash('Plantime deleted', 'success')

    return redirect(url_for('apomanagement.index'))


@apomanagement.route("/plantime/api/bymodelyear/<modelType>/<int:year>", methods=['GET'])
@login_required
def apiPlanTimeByModelTypeYear(modelType, year):
    q = request.args.get('q', None, type=str)
    page = request.args.get('page', 1, type=int)
    if q is not None:
        items = PlanTime.query.join(LaborOperation).filter(
            LaborOperation.name.like("%" + q + "%")
        ).filter(
            PlanTime.carModel_modelType == modelType
        ).filter(
            PlanTime.year == year
        ).paginate(page, 20, False)
    else:
        items = PlanTime.query.filter(
            PlanTime.carModel_modelType == modelType
        ).filter(
            PlanTime.year == year
        ).paginate(page, 20, False)

    itemsToReturn = [
        {
            'id': l.id,
            'name': '{}: {} ({})'.format(
                l.laborOperation.operationId,
                l.laborOperation.name,
                l.workContent
            )
        }
        for l in items.items if l.laborOperation is not None
    ]
    return jsonify({
        'items': itemsToReturn,
        'total_count': items.total
    })
