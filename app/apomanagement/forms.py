#!/usr/bin/env python
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, FormField, FieldList, SelectField, IntegerField
from wtforms_components import TimeField
from wtforms.validators import DataRequired, EqualTo, Length

from wtforms.ext.sqlalchemy.fields import QuerySelectField

from app.db.models import CarModel, LaborOperation, PlanTime
from ..customFormElements import Select2AJAXSelectField

from werkzeug.utils import secure_filename
from flask_wtf.file import FileField, FileRequired


class APOSUploadForm(FlaskForm):
    zipFile = FileField(
        'APOS ZIP-File',
        validators=[FileRequired()],
        description='Upload here the ZIP File obtained from the Porsche Dealer intranet.'
    )


class LaborOperationAddForm(FlaskForm):
    operationId = StringField('Labor Operation', validators=[DataRequired()])
    name = StringField('Description', validators=[DataRequired()])


class PlanTimeForm(FlaskForm):
    carModel = Select2AJAXSelectField(
        'Model Type',
        model=CarModel,
        ajax="/apo/carmodel/api"
    )
    year = SelectField(
        'Year',
        choices=[(y, y) for y in range(2000, 2018)],
        coerce=int
    )
    laborOperation = Select2AJAXSelectField(
        'Labor Operation',
        model=LaborOperation,
        ajax="/apo/laboroperation/api"
    )
    planTime = IntegerField('Plan Time')


class CarModelAddForm(FlaskForm):
    modelType = StringField('Model Type', validators=[DataRequired()])
    name = StringField('Name', validators=[DataRequired()])


class CarModelEditForm(CarModelAddForm):
    planTimes = FieldList(
        FormField(PlanTimeForm),
        min_entries=0
    )
