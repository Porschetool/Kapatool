from flask import Blueprint

apomanagement = Blueprint(
    'apomanagement',
    __name__,
    template_folder='templates',
    static_folder='static'
)

from . import views

