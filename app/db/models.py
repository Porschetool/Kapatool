from app import connection as db
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import re
# tools to store salted passwords
from werkzeug.security import generate_password_hash, check_password_hash
# tools to use hash values as ids for plantimes
import hashlib
import numpy as np

user_group_table = db.Table(
    'User_Group',
    db.Column('user_id', db.Integer, db.ForeignKey('Users.id')),
    db.Column('group_id', db.Integer, db.ForeignKey('Groups.id'))
)

operation_group_table = db.Table(
    'Operation_Group',
    db.Column('operation_id', db.Integer, db.ForeignKey('Operations.id')),
    db.Column('group_id', db.Integer, db.ForeignKey('Groups.id'))
)

user_calendarentry_table = db.Table(
    'User_CalendarEntry',
    db.Column('user_id', db.Integer, db.ForeignKey('Users.id')),
    db.Column('CalendarEntry_id', db.Integer, db.ForeignKey('CalendarEntries.id'))
)


class User(db.Model):
    """
    Class provides login-functionality. Password hashing based on http://flask.pocoo.org/snippets/54/
    """
    __tablename__ = 'Users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    pw_hash = db.Column(db.String(120))
    role = db.Column(db.String(30))
    productivity = db.Column(db.Float)
    setupTime = db.Column(db.Integer)
    qualityInspector = db.Column(db.Boolean())
    admin = db.Column(db.Boolean())
    shopCalendar = db.Column(db.Boolean())
    allocationPriority = db.Column(db.Integer)
    groups = db.relationship(
        'Group',
        secondary=user_group_table,
        backref=db.backref('users', lazy='dynamic')
    )
    patches = db.relationship(
        "Patch",
        backref=db.backref('user'),
        order_by="Patch.startDatetime"
    )
    appointments = db.relationship(
        "Appointment",
        backref=db.backref('user'),
        lazy='dynamic'
    )
    qualityChecks = db.relationship(
        "Job",
        backref=db.backref('qualityCheckUser'),
        lazy='dynamic'
    )
    calendarSubscriptions = db.relationship(
        'CalendarSubscription',
        backref='user',
        order_by="CalendarSubscription.startDate"
    )

    def __init__(
        self,
        name="",
        password="",
        role="",
        qualityInspector=False,
        admin=False,
        productivity=1.0,
        setupTime=0,
        APOSRegEx="",
        groups=[],
        calendar_id=None,
        allocationPriority=0,
        shopCalendar=False
    ):
        self.name = name
        self.set_password(password)
        self.role = role
        self.qualityInspector = qualityInspector
        self.admin = admin
        self.groups = groups
        self.calendar_id = calendar_id
        self.productivity = productivity
        self.APOSRegEx = APOSRegEx
        self.setupTime = setupTime
        self.allocationPriority = allocationPriority
        self.shopCalendar = shopCalendar

    def get_id(self):
        return unicode(self.id)

    def set_password(self, password):
        self.pw_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.pw_hash, password)

    is_active = property(lambda self: True, lambda self, newValue: False)
    is_admin = property(lambda self: self.admin, lambda self, newValue: False)
    is_authenticated = property(lambda self: True, lambda self, newValue: False)
    is_anonymous = property(lambda self: False, lambda self, newValue: False)
    currentPatch = property(
        lambda self: Patch.query.filter(Patch.user == self).filter(Patch.startDatetime.isnot(None)).filter(Patch.endDatetime.is_(None)).first(),
        lambda self, newValue: False
    )
    """Return patch database object, the user is currently working on (if any). Otherwise returns None"""


class Setting(db.Model):
    __tablename__ = 'Settings'
    id = db.Column(db.String(120), primary_key=True)
    label = db.Column(db.String(120))
    value = db.Column(db.String(120))

    def getFormattedValue(self):
        """
        Return value formated as float if possible. OTherwise return original string representation
        """
        try:
            return float(self.value)
        except ValueError:
            return self.value

    # new property to allow read-only access to value, properly formatted as a float
    formattedValue = property(
        lambda self: self.getFormattedValue(),
        lambda self, newValue: False
    )


class Group(db.Model):
    __tablename__ = 'Groups'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    APOSRegEx = db.Column(db.Text())

    def __init__(self, name='', users=[], APOSRegEx=None):
        self.name = name
        self.users = users
        self.APOSRegEx = APOSRegEx

    def canPerformAPOS(self, apos):
        """
        Check if the group can handle a given APOS

        Parameters
        ----------
        apos : int

        Returns
        -------
        d : boolean
            Decision
        """
        apos = str(apos)
        if self.APOSRegEx is None:
            return False
        return any(  # one match is sufficient
            r is not None  # match means, RE object and not None is returned by match() function
            for r in map(  # execute match() on all RE objects
                lambda s: re.match('^' + s + '$', apos),
                self.APOSRegEx.splitlines()
            )
        )


class Machine(db.Model):
    __tablename__ = 'Machines'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    setupTime = db.Column(db.Integer)
    APOSRegEx = db.Column(db.Text())
    patches = db.relationship(
        "Patch",
        backref=db.backref('machine'),
        order_by="Patch.startDatetime"
    )

    def __init__(self, name="", setupTime=0.0, APOSRegEx=""):
        self.name = name
        self.setupTime = setupTime
        self.APOSRegEx = APOSRegEx

    def canPerformAPOS(self, apos):
        """
        Check if the machine can handle a given APOS

        Parameters
        ----------
        apos : int

        Returns
        -------
        d : boolean
            Decision
        """
        apos = str(apos)
        if self.APOSRegEx is None:
            return False
        return any(  # one match is sufficient
            r is not None  # match means, RE object and not None is returned by match() function
            for r in map(  # execute match() on all RE objects
                lambda s: re.match('^' + s + '$', apos),
                self.APOSRegEx.splitlines()
            )
        )

    currentPatch = property(
        lambda self: Patch.query.filter(Patch.machine == self).filter(Patch.startDatetime.isnot(None)).filter(Patch.endDatetime.is_(None)).first(),
        lambda self, newValue: False
    )
    """Return patch database object, the machine is currently working on (if any). Otherwise returns None"""


class Calendar(db.Model):
    __tablename__ = 'Calendars'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    repeatingEntries = db.relationship(
        'RepeatingCalendarEntry',
        backref='calendar',
        order_by="RepeatingCalendarEntry.dayOfWeek"
    )
    finalized = db.Column(db.Boolean())
    calendarSubscriptions = db.relationship(
        'CalendarSubscription',
        backref='calendar',
        order_by="CalendarSubscription.startDate"
    )

    def __init__(self, name="", finalized=False):
        self.name = name
        self.finalized = finalized


class CalendarSubscription(db.Model):
    __tablename__ = 'CalendarSubscriptions'
    id = db.Column(db.Integer, primary_key=True)
    calendar_id = db.Column(db.Integer, db.ForeignKey('Calendars.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('Users.id'))
    startDate = db.Column(db.Date)
    endDate = db.Column(db.Date, nullable=True)

    def __init__(self, calendar=None, user=None, startDate=None, endDate=None):
        self.calendar_id = calendar.id if calendar is not None else None
        self.user_id = user.id if user is not None else None
        self.startDate = startDate
        self.endDate = endDate


class RepeatingCalendarEntry(db.Model):
    __tablename__ = 'RepeatingCalendarEntries'
    id = db.Column(db.Integer, primary_key=True)
    # details about the repeating entry
    dayOfWeek = db.Column(db.Integer)
    startTime = db.Column(db.Time)
    endTime = db.Column(db.Time)
    # calendar, this entry is associated with
    calendar_id = db.Column(db.Integer, db.ForeignKey('Calendars.id'))

    def __init__(self, dayOfWeek, startTime, endTime, calendar_id):
        self.dayOfWeek = dayOfWeek
        self.startTime = startTime
        self.endTime = endTime
        self.calendar_id = calendar_id


class CalendarEntry(db.Model):
    __tablename__ = 'CalendarEntries'
    id = db.Column(db.Integer, primary_key=True)
    users = db.relationship(
        'User',
        secondary=user_calendarentry_table,
        backref=db.backref('calendarEntries', lazy='dynamic')
    )
    present = db.Column(db.Boolean())
    startDatetime = db.Column(db.DateTime)
    endDatetime = db.Column(db.DateTime)

    def __init__(self, startDatetime=None, endDatetime=None, users=[]):
        self.startDatetime = startDatetime
        self.endDatetime = endDatetime
        self.users = users


class CarModel(db.Model):
    __tablename__ = 'CarModels'
    modelType = db.Column(db.String(10), primary_key=True)
    name = db.Column(db.String(120))
    planTimes = db.relationship(
        "PlanTime",
        backref=db.backref('carModel'),
        lazy='dynamic'
    )
    jobs = db.relationship(
        'Job',
        backref='carModel',
        lazy='dynamic'
    )

    def __init__(self, modelType="", name=""):
        self.modelType = modelType
        self.name = name


class LaborOperation(db.Model):
    __tablename__ = 'LaborOperations'
    operationId = db.Column(db.String, primary_key=True)
    name = db.Column(db.String(120))
    planTimes = db.relationship(
        "PlanTime",
        backref=db.backref('laborOperation'),
        lazy='dynamic'
    )
    operations = db.relationship(
        "Operation",
        backref=db.backref('laborOperation'),
        lazy='dynamic'
    )

    def __init__(self, operationId="", name=""):
        self.operationId = operationId
        self.name = name


class PlanTime(db.Model):
    __tablename__ = 'PlanTimes'
    id = db.Column(db.Integer, primary_key=True)
    carModel_modelType = db.Column(db.String, db.ForeignKey('CarModels.modelType'))
    year = db.Column('year', db.Integer)
    laborOperation_operationId = db.Column(db.String, db.ForeignKey('LaborOperations.operationId'))
    workContent = db.Column(db.Integer)

    # Add Unique Combination constraint acc. to http://stackoverflow.com/a/10061143
    db.UniqueConstraint(
        'carModel_modelType',
        'year'
        'laborOperation_operationId',
        name='uix_1'
    )

    def __init__(self, carModel_modelType=None, year=0, laborOperation_operationId=None, workContent=0):
        self.carModel_modelType = carModel_modelType
        self.year = year
        self.laborOperation_operationId = laborOperation_operationId
        self.workContent = workContent


class Operation(db.Model):
    __tablename__ = 'Operations'
    id = db.Column(db.Integer, primary_key=True)
    job_id = db.Column(db.Integer, db.ForeignKey('Jobs.id'))
    position = db.Column(db.Integer)
    laborOperation_id = db.Column(db.Integer, db.ForeignKey('LaborOperations.operationId'))
    workContent = db.Column(db.Integer)
    description = db.Column(db.String(120))
    restartAfterDatetime = db.Column(db.DateTime, nullable=True)
    groups = db.relationship(
        'Group',
        secondary=operation_group_table,
        backref=db.backref('operations', lazy='dynamic')
    )
    patches = db.relationship(
        "Patch",
        backref=db.backref('operation')
    )

    def __init__(self, job=None, position=0, laborOperation=None, workContent=None, description=None, restartAfterDatetime=None, groups=None):
        self.job_id = None if job is None else job.id
        self.position = position
        self.laborOperation_id = None if laborOperation is None else laborOperation.id
        self.patches = []
        self.restartAfterDatetime = restartAfterDatetime
        self.groups = [] if groups is None else groups

    def predecessorsFinished(self):
        """
        Check whether all predecessors are finished and processing can hence begin on this operation

        Returns
        -------
        d : boolean
        """
        return all(
            [True] +  # in case there are no precedecessors, return True
            [
                o.shareCompleted == 1
                for o in self.job.operations
                if o.position < self.position
            ]
        )

    completedWorkContent = property(
        lambda self: sum([
            float(p.workContentCovered)
            for p in self.patches
            if p.endDatetime is not None
        ]),
        lambda self, newValue: False
    )
    """Actually completed work content in TUs, counting only patches that have been completed"""
    shareCompleted = property(
        lambda self: self.completedWorkContent / self.totalWorkContent if self.totalWorkContent > 0 else 0,
        lambda self, newValue: False
    )
    """Share of completed work, measured against total work content"""
    totalWorkContent = property(
        lambda self: self.workContent,
        lambda self, newValue: False
    )
    """Total work content in TUs"""
    outstandingWorkContent = property(
        lambda self: self.totalWorkContent - self.completedWorkContent,
        lambda self, newValue: False
    )
    """outstanding work content in TUs"""


class Patch(db.Model):
    __tablename__ = 'Patches'
    id = db.Column(db.Integer, primary_key=True)
    operation_id = db.Column(db.Integer, db.ForeignKey('Operations.id'))
    startDatetime = db.Column(db.DateTime)
    endDatetime = db.Column(db.DateTime)
    planStartDatetime = db.Column(db.DateTime, nullable=True)
    planEndDatetime = db.Column(db.DateTime, nullable=True)
    workContentCovered = db.Column(db.Float)
    user_id = db.Column(db.Integer, db.ForeignKey('Users.id'))
    machine_id = db.Column(db.Integer, db.ForeignKey('Machines.id'))

    def __init__(
        self,
        operation=None,
        workContentCovered=0,
        startDatetime=None,
        endDatetime=None,
        planStartDatetime=None,
        planEndDatetime=None,
        user=None,
        machine=None
    ):
        self.operation_id = None if operation is None else operation.id
        self.workContentCovered = workContentCovered
        self.startDatetime = startDatetime
        self.endDatetime = endDatetime
        self.planStartDatetime = planStartDatetime
        self.planEndDatetime = planEndDatetime
        self.user_id = user.id if user is not None else None
        self.machine_id = machine.id if machine is not None else None

    def getLength(self):
        """
        Get patch length in hours

        Returns
        -------
        l : float
        """
        end = self.endDatetime if self.endDatetime is not None else self.planEndDatetime
        start = self.startDatetime if self.startDatetime is not None else self.planStartDatetime
        if end is None or start is None:
            return None
        d = end - start
        # calculate length of patch in hours
        return d.days*24 + float(d.seconds)/3600

    length = property(
        lambda self: self.getLength(),
        lambda self, newValue: False
    )
    """Read only access to patch length in hours"""


class Job(db.Model):
    __tablename__ = 'Jobs'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    carModel_modelType = db.Column(db.String, db.ForeignKey('CarModels.modelType'))
    year = db.Column('year', db.Integer)
    chassisNumber = db.Column(db.String(120))
    telephoneNumber = db.Column(db.String(120))
    mileage = db.Column(db.String(120))
    licensePlate = db.Column(db.String(120))
    releaseDatetime = db.Column(db.DateTime, nullable=True)
    postProcessingWorkContent = db.Column(db.Integer)
    pickUpAppointment_id = db.Column(db.Integer, db.ForeignKey("Appointments.id"))
    dropOffAppointment_id = db.Column(db.Integer, db.ForeignKey("Appointments.id"))
    expectedCompletionDatetime = db.Column(db.DateTime, nullable=True)
    completionDatetime = db.Column(db.DateTime, nullable=True)
    returnDatetime = db.Column(db.DateTime, nullable=True)
    qualityCheckDatetime = db.Column(db.DateTime, nullable=True)
    qualityCheckUser_id = db.Column(db.String, db.ForeignKey('Users.id'))
    operations = db.relationship(
        "Operation",
        backref=db.backref('job'),
        order_by="Operation.position"
    )

    def __init__(
        self,
        name="",
        carModel=None,
        year=0,
        dropOffAppoinment=None,
        pickUpAppointment=None,
        releaseDatetime=None,
        completionDatetime=None,
        postProcessingWorkContent=0
    ):
        self.name = name
        self.carModel = carModel
        self.carModel_modelType = None if carModel is None else carModel.modelType
        self.year = year
        self.releaseDatetime = releaseDatetime
        self.dropOffAppoinment_id = dropOffAppoinment.id if dropOffAppoinment is not None else None
        self.pickUpAppointment_id = pickUpAppointment.id if pickUpAppointment is not None else None
        self.completionDatetime = completionDatetime
        self.postProcessingWorkContent = postProcessingWorkContent

    def getCurrentPatch(self):
        """
        Return patch database object, that is currently working on this job (if any). Otherwise returns None

        Returns
        -------
        p : Patch
        """
        # Find all ongoing patches
        ongoingPatches = Patch.query.filter(Patch.startDatetime.isnot(None)).filter(Patch.endDatetime.is_(None)).all()
        # filter for jobs
        ongoingPatches = filter(
            lambda p: p.operation.job == self,
            ongoingPatches
        )
        if len(ongoingPatches) > 0:
            return ongoingPatches[0]
        else:
            return None

    def getJobStatus(self):
        """
        Returns an integer status indicator as follows:
        1 - Job not released
        2 - Job released
        3 - Processing in shop completed
        4 - Quality check passed
        5 - Ready for return (washing completed)
        6 - Returned
        """
        if self.releaseDatetime is None:
            return 1
        elif self.shareCompleted < 1 and not np.isclose(self.shareCompleted, 1.0):
            if self.pickUpAppointment.endDatetime < datetime.now():
                return 7
            else:
                return 2
        elif self.qualityCheckDatetime is None:
            return 3
        elif self.completionDatetime is None:
            return 4
        elif self.returnDatetime is None:
            return 5
        else:
            return 6

    totalWorkContent = property(
        lambda self: sum([o.totalWorkContent for o in self.operations]),
        lambda self, newValue: False
    )
    shareCompleted = property(
        lambda self: sum([
            o.shareCompleted*o.totalWorkContent
            for o in self.operations
        ])/sum([
            o.totalWorkContent
            for o in self.operations
        ]) if sum([
            o.totalWorkContent
            for o in self.operations
        ]) > 0 else 0,
        lambda self, newValue: False
    )
    lastPatchEnd = property(
        lambda self: max(*[p.endDatetime for o in self.operations for p in o.patches]),
        lambda self, newValue: False
    )
    """Read-only access on end of last patch. This is not the end of the job,
    since post-processing (washing, etc.) may be required"""
    currentPatch = property(
        lambda self: self.getCurrentPatch(),
        lambda self, newValue: False
    )
    """Return patch database object, that is currently working on this job (if any). Otherwise returns None"""
    outstandingWorkContent = property(
        lambda self: sum([o.outstandingWorkContent for o in self.operations]) if len(self.operations) > 0 else 0,
        lambda self, newValue: False
    )
    """Read-only access on remanining workcontent in TUs"""
    jobStatus = property(
        lambda self: self.getJobStatus(),
        lambda self, newValue: False
    )
    """Read-only access on job status (integer value)"""
    onTime = property(
        lambda self: self.completionDatetime <= self.pickUpAppointment.startDatetime if self.completionDatetime is not None and self.pickUpAppointment is not None else None,
        lambda self, newValue: False
    )
    """Read-only indicator if job was completed on time.
    Defined as: All operations finished before the pick up appointment"""


class Appointment(db.Model):
    __tablename__ = 'Appointments'
    id = db.Column(db.Integer, primary_key=True)
    # job_id = db.Column(db.Integer, db.ForeignKey('Jobs.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('Users.id'))
    startDatetime = db.Column(db.DateTime)
    endDatetime = db.Column(db.DateTime)
    inbound = db.Column(db.Boolean())
    prepared = db.Column(db.Boolean())
    pickUpJob = db.relationship(
        'Job',
        backref=db.backref('pickUpAppointment'),
        foreign_keys='Job.pickUpAppointment_id',
        uselist=False
    )
    dropOffJob = db.relationship(
        'Job',
        backref=db.backref('dropOffAppointment'),
        foreign_keys='Job.dropOffAppointment_id',
        uselist=False
    )

    def __init__(self, startDatetime, endDatetime, inbound, user=None):
        self.user_id = None if user is None else user.id
        self.startDatetime = startDatetime
        self.endDatetime = endDatetime
        self.inbond = inbound
