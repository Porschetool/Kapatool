from flask import Blueprint

shopfloormanagement = Blueprint(
    'shopfloormanagement',
    __name__,
    template_folder='templates',
    static_folder='static'
)

from . import views
