#!/usr/bin/env python
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, DecimalField, PasswordField, BooleanField, FormField, FieldList, SelectField, DateTimeField
from wtforms_components import TimeField
from wtforms.validators import DataRequired, EqualTo, Length, NumberRange, Optional, ValidationError

from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField, QuerySelectField
from ..customFormElements import Select2AJAXSelectField

import functools  # to create curried functions

from app.db.models import Job, Operation, Patch, CarModel, PlanTime, User, Machine, Setting, Group

import datetime  # to access current year


class JobForm(FlaskForm):
    """
    Minimal form to simply ask for the model year of the job a
    """
    name = StringField('Customer Name', validators=[DataRequired()])
    licensePlate = StringField('License Plate', validators=[DataRequired()])
    carModel = Select2AJAXSelectField(
        label='Model Type',
        model=CarModel,
        ajax="/apos/carmodel/api",
        validators=[DataRequired()]
    )
    year = SelectField(
        'Year (APOS data available for highlighted years)',
        choices=[(y, y) for y in range(2000, datetime.datetime.now().year+1)],
        coerce=int,
        validators=[DataRequired()]
    )
    chassisNumber = StringField('VIN')
    mileage = StringField('Mileage')
    telephoneNumber = StringField('Telephone Number', validators=[DataRequired()])


class JobSearchForm(FlaskForm):
    """
    Search form for Job properties
    """
    name = StringField('Customer Name')
    licensePlate = StringField('License Plate')
    carModel = Select2AJAXSelectField(
        label='Model Type',
        model=CarModel,
        ajax="/apos/carmodel/api"
    )
    year = SelectField(
        'Year',
        choices=[('', 'No selection')] + [(y, y) for y in range(2000, datetime.datetime.now().year+1)]
    )
    chassisNumber = StringField('VIN')
    mileage = StringField('Mileage')
    telephoneNumber = StringField('Telephone Number')


# Create new validator to compare start and endtimes
# c.f. http://hplgit.github.io/web4sciapps/doc/pub/._web4sa_flask012.html
def laterDateTime_F(form, field, earlierTimeField):
    """
    Validator function that compares the values of two datetime form fields

    Parameters
    ----------
    form : FlaskForm
        Form instance that contains both fields to be compared
    field : Flask Form Field
        Instance of the field to which the validator is applied
    earlierTimeField : string
        Name of the other time field

    Returns
    -------
    result : boolean
        Result of the validation
    """
    if getattr(form, earlierTimeField).data > field.data:
        raise ValidationError(
            'Field {} has to hold higher value than field {}'.format(
                field.label,
                getattr(form, earlierTimeField).label
            )
        )


# now we define a partial function to hide the additional parameter
def laterDateTime(earlierTimeField):
    """
    Custom form validator that ensures that the datetime field it is applied to has a higher value
    then earlierTimeField

    Parameters
    ----------
    earlierTimeField : string
        Name of the other form field

    Returns
    -------
    validator : callable
        Callable validator
    """
    return functools.partial(
        laterDateTime_F,
        earlierTimeField=earlierTimeField
    )


class StartPatchNowForm(FlaskForm):
    """
    Reduced version of the patch form that assumes that patch end now (and was already started)
    """
    # only users in role technician that are currently not busy can be assigned
    user = QuerySelectField(
        query_factory=lambda: filter(
            lambda u: u.currentPatch is None,
            User.query.filter(User.role == 'Technician').all()
        ),
        get_label=lambda g: g.name
    )
    machine = QuerySelectField(
        query_factory=lambda: Machine.query.all(),
        get_label=lambda g: g.name
    )


class EndPatchNowForm(FlaskForm):
    """
    Reduced version of the patch form that assumes that patch starts (and does not end)
    """
    workContentCovered = IntegerField(
        'Hours of Work Content Covered',
        validators=[
            DataRequired(),
            NumberRange(min=0, max=1)
        ]
    )
    restartAfterDatetime = DateTimeField(
        'Work paused until',
        validators=[Optional()],
        format=Setting.query.get('defaultDatetimeFormat').formattedValue,
        description='Do not plan starting this operation until after this point in time. Set a value here, if the task e.g. requires spare parts that need to be ordered.'
    )


def OperationForm(taylorToObj=None, operationPosition=0, *args, **kwargs):
    """
    Function wrapper that creates a custom FlaskForm class where fields can be taylored to a
    passed object.

    Implementation based on http://stackoverflow.com/a/31175306/1662259

    Parameters
    ----------
    taylorToObj : Job
        Job instance to which the form is to be taylored.

    Returns
    -------
    form : class
        Instantiatable form class
    """
    class ActualOperationForm(FlaskForm):
        pass

    # add Fields
    setattr(
        ActualOperationForm,
        'position',
        IntegerField(
            label='Position',
            description='Sequence in the job. Operations are executed starting with lowest position. > 1 operations may have the same position. In this case, either one may be processed first.',
            default = operationPosition
        )
    )
    setattr(
        ActualOperationForm,
        'planTime',
        Select2AJAXSelectField(
            label='APOS',
            model=PlanTime,
            ajax="/apos/plantime/api/bymodelyear/{}/{}".format(taylorToObj.carModel.modelType, taylorToObj.year),  # overwritten in template
            validators=[Optional()]
        )
    )
    setattr(
        ActualOperationForm,
        'workContent',
        IntegerField(label='Work Content [TU]', validators=[Optional()])
    )
    setattr(
        ActualOperationForm,
        'description',
        StringField(label='Additional description')
    )
    setattr(
        ActualOperationForm,
        'restartAfterDatetime',
        DateTimeField(
            label='Wait until',
            format=Setting.query.get('defaultDatetimeFormat').formattedValue,
            validators=[Optional()],
            description='Do not plan starting this operation until after this point in time. Set a value here, if the task e.g. requires spare parts that need to be ordered.'
        )
    )
    setattr(
        ActualOperationForm,
        'groups',
        QuerySelectMultipleField(
            'Assign to Group(s)',
            query_factory=lambda: Group.query.all(),
            get_label=lambda g: g.name
        )
    )

    return ActualOperationForm(*args, **kwargs)


class AppointmentForm(FlaskForm):
    user = QuerySelectField(
        query_factory=lambda: User.query.filter(User.role == 'ServiceAdvisor').all(),
        get_label=lambda g: g.name
    )
    startDatetime = DateTimeField(
        'Start of Work',
        format=Setting.query.get('defaultDatetimeFormat').formattedValue,
        validators=[DataRequired()]
    )
    duration = IntegerField(
        'Duration',
        validators=[
            DataRequired()
        ],
        default=30
    )
