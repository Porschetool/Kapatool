from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, jsonify
from flask_login import login_required, login_user, logout_user, current_user

from app import connection
from app.db.models import Appointment, Job, Operation, Patch
from . import shopfloormanagement
from forms import *
from app.simulationmanagement import simulationConnector
from app.simulationmanagement.dbToSimulationTranslation import *

import datetime
import math
import re
from numpy import cumsum, zeros
from collections import Counter


@shopfloormanagement.route(
    "/index",
    methods=['GET', 'POST'],
    defaults={'startDate': None}
)
@shopfloormanagement.route("/index/<startDate>", methods=['GET', 'POST'])
@login_required
def index(startDate):
    unreturnedJobs = Job.query.filter(Job.returnDatetime.is_(None)).all()
    unreturnedJobs.sort(key=lambda x: x.jobStatus, reverse=False)

    if startDate is None:
        startDate = datetime.date.today()
    else:
        startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    endDate = startDate + datetime.timedelta(days=1)

    return render_template(
        'pages/shopfloormanagement.index.html',
        unreturnedJobs=unreturnedJobs,
        capacityProfile=createCapacityProfile(startDate, startDate + datetime.timedelta(weeks=2)),
        currentDatetime=datetime.datetime.now(),
        startDate=startDate,
        urlTemplate=url_for(
            'shopfloormanagement.index',
            startDate=endDate.strftime('%Y-%m-%d')
        ).replace(endDate.strftime('%Y-%m-%d'), '%Y-%m-%d')
    )

@shopfloormanagement.route(
    '/api/patchevents/<startDate>',
    methods=['GET']
)
def getPatchEvents(startDate):
    startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    endDate = startDate + datetime.timedelta(days=1)
    shopSchedule = createSchedule(
        startDate=startDate,
        endDate=endDate,
        dHour=0.5,
        resources=User.query.filter(User.role == 'Technician').all(),
        events=Patch.query.filter(Patch.endDatetime <= endDate).filter(Patch.endDatetime >= startDate).union(
            Patch.query.filter(Patch.planEndDatetime <= endDate).filter(Patch.planEndDatetime >= startDate)
        ).all()
    )

    patchEvents = {}
    #todo: add isCurrentPatchNull
    patchEvents['currentUser'] =  {"id": current_user.id, "role": current_user.role, "isCurrentPatchNull" : current_user.currentPatch is None}

    eventsByResource = []
    for resourceId, eventsForResource in shopSchedule['events'].iteritems():
        for e in eventsForResource:
            eventsByResource.append(getPatchEventByResource(e,resourceId))

    patchEvents['events'] = eventsByResource

    return jsonify(patchEvents)

@shopfloormanagement.route(
    '/api/appointmentevents/<startDate>',
    methods=['GET']
)
def getAppointmentEvents(startDate):
    startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    endDate = startDate + datetime.timedelta(days=1)

    # create appointment schedule
    appointmentSchedule = createSchedule(
        startDate=startDate,
        endDate=endDate,
        dHour=0.5,
        resources=User.query.filter(User.role == 'ServiceAdvisor').all(),
        events=Appointment.query.filter(
            Appointment.endDatetime <= endDate
        ).filter(
            Appointment.endDatetime >= startDate
        ).all()
    )

    eventsByResource = []
    for resourceId, eventsForResource in appointmentSchedule['events'].iteritems():
        for e in eventsForResource:
            eventsByResource.append(getAppointmentEventByResource(e,resourceId))

    return jsonify(eventsByResource)

@shopfloormanagement.route(
    '/api/shopschedule/<startDate>',
    methods=['GET']
)
def getShopSchedule(startDate):
    startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    endDate = startDate + datetime.timedelta(days=1)

    shopSchedule = createSchedule(
        startDate=startDate,
        endDate=endDate,
        dHour=0.5,
        resources=User.query.filter(User.role == 'Technician').all(),
        events=Patch.query.filter(Patch.endDatetime <= endDate).filter(Patch.endDatetime >= startDate).union(
            Patch.query.filter(Patch.planEndDatetime <= endDate).filter(Patch.planEndDatetime >= startDate)
        ).all()
    )
    return getJsonSchedule(shopSchedule)

def getJsonSchedule(schedule):
    eventsByResource = []
    for resourceId, eventsForResource in schedule['events'].iteritems():
        for e in eventsForResource:
            eventsByResource.append(getEventByResource(e,resourceId))
    jsonSchedule = {}
    jsonSchedule['events'] = eventsByResource

    #change resource simulation id to resource id (seems like it is the same thing)
    availabilities = []
    for resourceId, availability in schedule['availabilities'].iteritems():
        if availability:
            for a in availability:
                availabilities.append(
                    {'resourceId': resourceId, 
                    'from': a[0], 
                    'to':a[1]})
    jsonSchedule['availabilities'] = availabilities

    resources = []
    for resourceId, resourceName in schedule['resources'].iteritems():
        resources.append({'id':resourceId, 'name': resourceName})
    jsonSchedule['resources'] = resources
    jsonSchedule['timestamps'] = schedule['timestamps']

    return jsonify(jsonSchedule)

def getEventByResource(e, resourceId):
    startDateTime = e.startDatetime if e.startDatetime is not  None else e.planStartDatetime
    endDateTime = e.endDatetime if e.endDatetime is not None else e.planEndDatetime
    return {'id':e.id,
        'resourceId': resourceId,
        'endDateTime': endDateTime, 
        'startDateTime':startDateTime}

def getAppointmentEventByResource(e, resourceId):
    eventType = 'dropOff' if e.dropOffJob is not None else 'pickUp'
    isPrepared = False
    name = None
    licensePlate = None
    carModelName = None
    jobId = None

    if eventType == 'dropOff':
        isPrepared = e.prepared if e.prepared is True else False
        name = e.dropOffJob.name
        licensePlate = e.dropOffJob.licensePlate
        carModelName = e.dropOffJob.carModel.name
        jobId = e.dropOffJob.id
    elif eventType == 'pickUp':
        # if e.pickUpJob.completed:  Job does not have completed attribute
        #     isPrepared = True
        if e.pickUpJob.expectedCompletionDatetime is not None and e.pickUpJob.expectedCompletionDatetime <= e.startDatetime:
            isPrepared = True
        else:
            isPrepared = False
        name = e.pickUpJob.name
        licensePlate = e.pickUpJob.licensePlate
        carModelName = e.pickUpJob.carModel.name
        jobId = e.pickUpJob.id

    return {'id': e.id,
        'type' : eventType,
        'name':name,
        'isPrepared': isPrepared,
        'startDateTime': e.startDatetime,
        'endDateTime': e.endDatetime,
        'licensePlate': licensePlate,
        'carModelName':carModelName,
        'jobId': jobId}

def getPatchEventByResource(e, resourceId):
    startDateTime = e.startDatetime if e.startDatetime is not None else e.planStartDatetime
    endDateTime = e.endDatetime if e.endDatetime is not None else e.planEndDatetime
    status = ""
    if e.endDatetime is None:
        if e.startDatetime is None:
            status = "scheduled"
        else:
            status = "inprocess"
    else:
        status = "completed"
    isReleased = e.operation.job.releaseDatetime is not None
    readyToReportWork = False
    readyToStartWork = False
    materialsReady = True
    if (e.operation.job.releaseDatetime is not None and
           e.operation.predecessorsFinished() and status != 'completed' and
           current_user.role
           in ['Manager', 'Technician']):
        #   if patch is purely scheduled, job currently not busy, and operation not waiting on material, give opportunity to start working #}
            if (e.operation.job.currentPatch is None and status == 'scheduled' and (current_user.role == 'Manager' or current_user.current_patch is None)):
                readyToStartWork = True
                if (e.operation.restartAfterDatetime is not None and e.operation.restartAfterDatetime >= currentDatetime):
                    materialsReady = False
            elif (status == 'inprocess' and (current_user.role == 'Manager' or e.user == current_user)):
                readyToReportWork = True

    return {'id':e.id,
        'endDateTime': endDateTime, 
        'startDateTime': startDateTime,
        'machineId': e.machine.id,
        'machineName':e.machine.name,
        'userId':e.user.id,
        'status': status,
        'readyToReportWork': readyToReportWork,
        'readyToStartWork': readyToStartWork,
        'materialsReady': materialsReady,
        'operation':
            {'id' : e.operation.id,
            'description': e.operation.description,
            'isReleased': isReleased,
            'jobId':e.operation.job.id,
            'jobCarModelName': e.operation.job.carModel.name,
            'jobLicensePlate': e.operation.job.licensePlate,
            'restartAfterDateTime': e.operation.restartAfterDatetime}}


@shopfloormanagement.route(
    '/api/appointmentschedule/<startDate>',
    methods=['GET']
)
def getAppointmentSchedule(startDate):
    startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    endDate = startDate + datetime.timedelta(days=1)

    # create appointment schedule
    appointmentSchedule = createSchedule(
        startDate=startDate,
        endDate=endDate,
        dHour=0.5,
        resources=User.query.filter(User.role == 'ServiceAdvisor').all(),
        events=Appointment.query.filter(
            Appointment.endDatetime <= endDate
        ).filter(
            Appointment.endDatetime >= startDate
        ).all()
    )

    return getJsonSchedule(appointmentSchedule)


@shopfloormanagement.route(
    '/runsimulation',
    methods=['GET']
)
def run_simulation():
    """
    This helper function simply starts the simulation and returns to the index page.
    """
    simulationConnector.startSimulation()
    return redirect(url_for('shopfloormanagement.index'))


@shopfloormanagement.route(
    "/statistics",
    methods=['GET', 'POST'],
    defaults={'horizon': '3weeks'}
)
@shopfloormanagement.route("/statistics/<horizon>")
@login_required
def showStatistics(horizon):
    # calculate start and end point of analysis
    endDate = datetime.datetime.now().date()
    multiplier = int(re.findall('\d+', horizon)[0])  # get number from string and convert to int
    timeUnit = horizon.replace(str(multiplier), '')  # remove multiplier from string
    if timeUnit not in ['days', 'weeks'] or multiplier <= 0:
        flash('Invalid time horizon specified.', 'warning')
        return redirect(url_for('shopfloormanagement.showStatistics'))
    startDate = endDate - datetime.timedelta(**{timeUnit: multiplier})  # pass the components of the horizon URL parameter as function parameters to timedelta

    # convert startDate and endDate into datetime instances
    startDatetime = datetime.datetime.combine(startDate, datetime.datetime.min.time())
    endDatetime = datetime.datetime.combine(endDate, datetime.datetime.max.time())

    # get shopcalendar object
    shopCalendar = getShopCalendarSimObject(startDate, endDate)
    if shopCalendar is None:
        flash('No shop calendar found.', 'danger')
        return redirect(url_for('main.index'))
    # get all dates at which availabilities in the shop calendar start -> Shop Calendar days
    availabilities = shopCalendar.availabilitiesBetween(
        startDatetime,
        endDatetime
    )
    if len(availabilities) == 0:
        flash('No availabilities associated with shop calendar.', 'danger')
        return redirect(url_for('shopfloormanagement.index'))
    throughputDiagramDays = tuple([
        startDate + datetime.timedelta(days=i)
        for i in range((endDate - startDate).days + 1)
    ])
    # get dictionaries that map Dates to shop calendar days
    DatetoSCD = {d: i for i, d in enumerate(throughputDiagramDays)}

    # get all jobs that overlap the time range
    jobs = Job.query.filter(
        Job.releaseDatetime <= endDatetime
    ).filter(
        (
            Job.returnDatetime >= startDatetime
        ) | (
            Job.returnDatetime.is_(None)
        )
    ).all()
    # calculate cumulated input and output
    inputTU = zeros(len(throughputDiagramDays))
    outputTU = zeros(len(throughputDiagramDays))
    # iterate through jobs and add work content to input and output respectively
    for j in jobs:
        inputTU[DatetoSCD[max(j.releaseDatetime.date(), startDate)]] += j.totalWorkContent
        if j.returnDatetime is not None:
            outputTU[DatetoSCD[j.returnDatetime.date()]] += j.totalWorkContent
    # cumulate arrays
    inputTU = cumsum(inputTU)
    outputTU = cumsum(outputTU)

    # calculate on time performance of jobs
    jobsOnTime = len([j for j in jobs if j.onTime])
    jobsLate = len([j for j in jobs if j.onTime is not None and not j.onTime])

    flowFactors = [
        shopCalendar.workingHoursBetween(j.releaseDatetime, j.completionDatetime) / j.totalWorkContent * 100
        for j in jobs
        if j.completionDatetime is not None
    ]
    print flowFactors
    # round to one significant digit
    flowFactors = map(lambda v: round(v, 1), flowFactors)
    # count frequencies of occurences
    c = Counter(flowFactors)
    flowFactorKeys = sorted(c.keys())  # sort ascending
    # retrieve values in order of sorted keys and normalize
    flowFactorCumulatedValues = cumsum([float(c[k]) for k in flowFactorKeys])/len(flowFactors)

    # calculate capacity utilization profile
    capProfile = createCapacityProfile(startDate, endDate)
    # create utilization profile
    utilizationProfile = {
        d: {
            'overall': float(entry['usedCapacity']) / entry['presentCapacity'] if entry['presentCapacity'] > 0 else 0,
            'perGroup': {
                g: float(entry['usedCapacity_perGroup'][g]) / entry['presentCapacity_perGroup'][g] if entry['presentCapacity_perGroup'][g] > 0 else 0
                for g in entry['presentCapacity_perGroup'].keys()
            }
        }
        for d, entry in capProfile.items()
    }

    return render_template(
        'pages/stats.index.html',
        flowFactorKeys=flowFactorKeys,
        flowFactorCumulatedValues=flowFactorCumulatedValues,
        throughputDiagramDays=throughputDiagramDays,
        inputTU=inputTU,
        outputTU=outputTU,
        multiplier=multiplier,
        timeUnit=timeUnit,
        jobsOnTime=jobsOnTime,
        jobsLate=jobsLate,
        basisURL=url_for('shopfloormanagement.showStatistics'),
        utilizationProfile=utilizationProfile
    )


@shopfloormanagement.route("/job/add", methods=['GET', 'POST'])
@login_required
def addJob():
    form = JobForm()
    if form.validate_on_submit():
        job = Job()
        form.populate_obj(job)

        job.released = False  # by default, set job to unreleased
        # load default post-processing work content from settings
        job.postProcessingWorkContent = Setting.query.get('postProcessingWorkContent').formattedValue

        connection.session.add(job)
        connection.session.commit()

        flash('New Job {} created.'.format(job.name), 'success')
        # start simulation to reflect changes
        simulationConnector.startSimulation()
        return redirect(url_for('shopfloormanagement.editJob', jobNumber=job.id))
    else:
        # render actual job add form (all information but the model year)
        return render_template(
            'forms/job.add.html',
            form=form
        )


@shopfloormanagement.route("/job/<int:jobNumber>/edit", 
    methods=['GET', 'POST'],
    defaults={'startDate': None})
@shopfloormanagement.route("/job/<int:jobNumber>/edit/<startDate>", 
    methods=['GET', 'POST'])
@login_required
def editJob(jobNumber, startDate):
    if startDate is None:
        startDate = datetime.date.today()
    else:
        startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()

    job = Job.query.get(jobNumber)
    form = JobForm(year=job.year, obj=job)
    del form.carModel  # form element not present, so do not take into account for validation
    
    newOperationNumber = db.session.query(func.max(Operation.position)).filter(Operation.job_id == job.id).first()[0]
    if (newOperationNumber is not None):
        newOperationNumber += 1
    else:
        newOperationNumber = 0
    if form.validate_on_submit():
        if current_user.role not in ['Technician', 'ServiceAdvisor', 'ServiceAssistant']:
            flash('You are not allowed to edit this job.', 'danger')
            return render_template(
                'forms/job.edit.html',
                form=form,
                item=job,
                entryAddForm=OperationForm(taylorToObj=job),
                form_job_number=jobNumber
            )

        form.populate_obj(job)
        connection.session.commit()
        # start simulation to reflect changes
        simulationConnector.startSimulation()
        flash('Job {} edited.'.format(job.name), 'success')


    return render_template(
        'forms/job.edit.html',
        form=form,
        item=job,
        currentDatetime=datetime.datetime.now(),
        entryAddForm=OperationForm(taylorToObj=job, operationPosition = newOperationNumber),
        form_job_number=jobNumber,
        capacityProfile=createCapacityProfile(startDate, startDate + datetime.timedelta(weeks=2)),
        startDate=startDate,
        urlTemplate=url_for(
            'shopfloormanagement.editJob',
            jobNumber=jobNumber,
            startDate=startDate.strftime('%Y-%m-%d')
        ).replace(startDate.strftime('%Y-%m-%d'), '%Y-%m-%d')
    )


@shopfloormanagement.route("/job/<int:jobNumber>/delete", methods=['GET'])
@login_required
def deleteJob(jobNumber):
    if current_user.role not in ['Manager', 'ServiceAdvisor', 'ServiceAssistant']:
        flash('You are not allowed to edit this job.', 'danger')
        return redirect(url_for('shopfloormanagement.index'))
    job = Job.query.get(jobNumber)
    if job.shareCompleted > 0 or job.currentPatch is not None:  # work has already gone into the job
        flash('Work has already started reported on this job. It cannot be deleted. Close it instead.', 'danger')
    else:
        # Delete all patches
        [connection.session.delete(p) for o in job.operations for p in o.patches]
        # Delete all operations
        operationsToDelete = [connection.session.delete(o) for o in job.operations]
        # delete appointments
        if job.dropOffAppointment is not None:
            connection.session.delete(job.dropOffAppointment)
        if job.pickUpAppointment is not None:
            connection.session.delete(job.pickUpAppointment)
        connection.session.delete(job)
        connection.session.commit()
        flash('Job deleted.', 'success')
    return redirect(url_for('shopfloormanagement.index'))


@shopfloormanagement.route("/job/<int:jobNumber>/release", methods=['GET'])
@login_required
def releaseJob(jobNumber):
    if current_user.role not in ['Manager', 'ServiceAdvisor', 'ServiceAssistant']:
        flash('You are not allowed to edit this job.', 'danger')
        return redirect(url_for('shopfloormanagement.index'))
    job = Job.query.get(jobNumber)
    if job.releaseDatetime is None:

        # Set recommended (planned) dropoff appointment time in case it's empty
        if job.dropOffAppointment_id  is None:
            recommendedDropOffEndDatetime = calculateRecommendedReleaseDatetime(job)
            if recommendedDropOffEndDatetime is not None:
                recommendedDropOffAppointment = Appointment(
                startDatetime=recommendedDropOffEndDatetime - datetime.timedelta(minutes=30),
                endDatetime=recommendedDropOffEndDatetime,
                inbound=True,
                user=job.pickUpAppointment.user)
                job.dropOffAppointment = recommendedDropOffAppointment
                connection.session.commit()
                flash("""The drop-off appointment was empty. Therefore, using the calculated optimal date  {} appoinment was scheduled for 30 minutes. 
                    {} is assigned to serve the appointment. 
                    Please return to edit this job once the exact date, appointment duration and employee are known!""".format(recommendedDropOffAppointment.startDatetime, recommendedDropOffAppointment.user.name), 'success')
                 
        #Set recommended (planned) pickup appointment time in case it's empty
        if job.pickUpAppointment_id  is None:
            recommendedPickUpStartDatetime = calculateRecommendedEndDatetime(job)
            if recommendedPickUpStartDatetime is not None:
                recommendedPickUpAppointment = Appointment(
                startDatetime=recommendedPickUpStartDatetime,
                endDatetime=recommendedPickUpStartDatetime + datetime.timedelta(minutes=30),
                inbound=False,
                user=job.dropOffAppointment.user)
                job.pickUpAppointment = recommendedPickUpAppointment
                connection.session.commit()
                flash("""The pick-up appointment was empty. Therefore, using the calculated optimal date {} appoinment was scheduled for 30 minutes. 
                    {} is assigned to serve the appointment. 
                    Please return to edit this job once the exact date, appointment duration and employee are known!""".format(recommendedPickUpAppointment.startDatetime, recommendedPickUpAppointment.user.name), 'success')

        if job.pickUpAppointment_id is not None and job.dropOffAppointment_id is not None:
            job.releaseDatetime = datetime.datetime.now().replace(microsecond=0)
            connection.session.commit()
            # start simulation to reflect changes
            simulationConnector.startSimulation()
            flash('Job {} released.'.format(job.name), 'success')
            jobDaysDuration = (job.pickUpAppointment.endDatetime - job.dropOffAppointment.startDatetime).days
            if (jobDaysDuration >= 1):
                flash('The job will take {} days'.format(jobDaysDuration), 'warning')
        else:
            flash('Sorry, you cannot release the job not setting pick-up and drop-off appointments', 'danger')
    else:
        flash('Job {} has already been released.'.format(job.name), 'danger')
    return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobNumber))


@shopfloormanagement.route("/job/<int:jobNumber>/qualityCheck", methods=['GET'])
@login_required
def qualityCheckJob(jobNumber):
    if not current_user.qualityInspector:
        flash('You are not allowed to confirm quality.', 'danger')
        return redirect(url_for('shopfloormanagement.index'))
    job = Job.query.get(jobNumber)
    if job.qualityCheckDatetime is None:
        # set store quality check information
        job.qualityCheckDatetime = datetime.datetime.now().replace(microsecond=0)
        job.qualityCheckUser = current_user

        connection.session.commit()

        flash('Quality of job {} confirmed.'.format(job.name), 'success')
    else:
        flash('Quality of Job {} has already been confirmed.'.format(job.name), 'danger')
    return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobNumber))


@shopfloormanagement.route("/job/<int:jobNumber>/complete", methods=['GET'])
@login_required
def completeJob(jobNumber):
    if current_user.role not in ['Manager', 'ServiceAdvisor', 'ServiceAssistant']:
        flash('You are not allowed to edit this job.', 'danger')
        return redirect(url_for('shopfloormanagement.index'))
    job = Job.query.get(jobNumber)
    if job.completionDatetime is None:
        job.completionDatetime = datetime.datetime.now().replace(microsecond=0)
        connection.session.commit()
        # start simulation to reflect changes
        simulationConnector.startSimulation()

        flash('Job {} completed.'.format(job.name), 'success')
    else:
        flash('Job {} has already been marked as completed.'.format(job.name), 'danger')
    return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobNumber))


@shopfloormanagement.route("/job/<int:jobNumber>/return", methods=['GET'])
@login_required
def returnJob(jobNumber):
    if current_user.role not in ['Manager', 'ServiceAdvisor', 'ServiceAssistant']:
        flash('You are not allowed to return this car.', 'danger')
        return redirect(url_for('shopfloormanagement.index'))
    job = Job.query.get(jobNumber)
    if job.returnDatetime is None:
        job.returnDatetime = datetime.datetime.now().replace(microsecond=0)
        connection.session.commit()

        flash('Job {} marked as returned to customer.'.format(job.name), 'success')
    else:
        flash('Job {} has already been marked as returned to customer.'.format(job.name), 'danger')
    return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobNumber))


@shopfloormanagement.route("/job/find", methods=['GET', 'POST'])
def searchJobs():
    form = JobSearchForm()
    form.year.allow_blank = True

    if form.validate_on_submit():
        query = Job.query
        for field in form:
            if field.name == 'csrf_token' or field.data in ['', None]:
                continue
            if 'SelectField' in field.type:  # if this is a select box, enforce exact match
                query = query.filter_by(**{
                    field.name: field.data
                })
            else:  # otherwise: Full text search
                query = query.filter(getattr(Job, field.name).like('%{}%'.format(field.data)))
        jobs = query.all()
    else:
        jobs = []

    return render_template(
        'forms/job.search.html',
        form=form,
        jobs=jobs
    )


@shopfloormanagement.route("/job/<int:jobNumber>/addoperation", methods=['GET', 'POST'])
@login_required
def addOperationToJob(jobNumber):
    if current_user.role not in ['Manager', 'ServiceAdvisor', 'ServiceAssistant']:
        flash('You are not allowed to edit this job.', 'danger')
        return redirect(url_for('shopfloormanagement.index'))
    job = Job.query.get(jobNumber)
    form = OperationForm(job)
    if form.validate_on_submit():
        # store plan time submitted in form
        planTime = form.planTime.data
        # and remove it from form, since this is not an operation property
        del form.planTime

        operation = Operation(job=job)

        form.populate_obj(operation)
        # if no work content is specified, but plantime is referenced, copy work content from plantime
        if operation.workContent is None and planTime is not None:
            operation.workContent = planTime.workContent
        # if no work content is specified, but plantime is referenced, copy description from plantime
        if planTime is not None:
            if operation.description != '':
                operation.description = operation.description + ' - ' + planTime.laborOperation.name
            else:
                operation.description = planTime.laborOperation.name
        # make sure, workContent is not set (happens if no APOS was chosen and no work content was specified)
        if operation.workContent is None or operation.workContent == 0 or operation.description == '':
            flash('Please assign APOS or set work content and description manually.', 'danger')
            return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobNumber))
        connection.session.add(operation)
        connection.session.commit()

        # start simulation to reflect changes
        simulationConnector.startSimulation()

        flash('Operation added to {}.'.format(job.name), 'success')

        return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobNumber))
    else:
        flash('Failed to add operation.'.format(job.name), 'warning')
        return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobNumber))


@shopfloormanagement.route("/operation/edit/<int:operationNumber>", methods=['GET', 'POST'])
@login_required
def editOperation(operationNumber):
    if current_user.role not in ['Manager', 'ServiceAdvisor', 'ServiceAssistant']:
        return redirect(url_for('shopfloormanagement.index'))
    operation = Operation.query.get(operationNumber)
    job = operation.job
    form = OperationForm(taylorToObj=job, obj=operation)
    if form.validate_on_submit():
        form.populate_obj(operation)
        connection.session.add(operation)
        connection.session.commit()
        flash('Operation edited.', 'success')
        return redirect(url_for('shopfloormanagement.editJob', jobNumber=job.id))
    else:
        return render_template(
            'forms/operation.edit.html',
            operationNumber=operationNumber,
            form=form
        )


@shopfloormanagement.route("/operation/delete/<int:operationNumber>", methods=['GET'])
@login_required
def deleteOperation(operationNumber):
    if current_user.role not in ['Manager', 'ServiceAdvisor', 'ServiceAssistant']:
        return redirect(url_for('shopfloormanagement.index'))
    operation = Operation.query.get(operationNumber)
    jobId = operation.job.id
    if operation.shareCompleted > 0:
        flash('Cannot delete operation for which processing has already begun.', 'warning')
    else:
        connection.session.delete(operation)
        connection.session.commit()
        # start simulation to reflect changes
        simulationConnector.startSimulation()
        flash('Operation deleted.', 'success')
    return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobId))


@shopfloormanagement.route("/patch/<int:patchNumber>/<int:operationNumber>/<int:userNumber>/<int:machineNumber>/start", methods=['GET', 'POST'])
def startPatchNow(patchNumber, operationNumber, userNumber, machineNumber):
    if not current_user.role in ['Technician', 'Manager']:
        return redirect(url_for('shopfloormanagement.index'))
    patch = Patch.query.get(patchNumber)
    dummyPatch = False
    # if the patch ID was outdated or newly assigned to a different operation, create a dummy patch
    if patch is None or patch.operation.id != operationNumber:
        dummyPatch = True
        patch = Patch(
            operation=Operation.query.get(operationNumber),
            planStartDatetime=datetime.datetime.now(),
            planEndDatetime=datetime.datetime.now() + datetime.timedelta(hours=1),
            user=User.query.get(userNumber),
            machine=Machine.query.get(machineNumber)
        )
        connection.session.add(patch)
        connection.session.commit()
    # make sure, this patch has not been started (let alone been finished) before
    if patch.startDatetime is not None or patch.endDatetime is not None:
        flash('Cannot start working on this patch.', 'warning')
        redirect(url_for('shopfloormanagement.index'))
    form = StartPatchNowForm(obj=patch)

    if form.validate_on_submit():
        # update user and machine from form
        form.populate_obj(patch)
        # only allow start of new patch, when old patch is finished.
        if patch.user.currentPatch is not None or patch.machine.currentPatch is not None:
            flash('User and/or Workplace are currently assigned. Cannot assign new task.', 'warning')
            return redirect(url_for('shopfloormanagement.index'))
        # only allow to start patches on jobs not currently under processing
        if patch.operation.job.currentPatch is not None:
            flash('Job is currently in processing. Cannot start new work.', 'warning')
            return redirect(url_for('shopfloormanagement.index'))
        # we do not check here, if the operation was waiting for material. As the user has submitted
        # the form, we assume that there is at least some workcontent that can be completed on this operation
        # under the current circumstances.

        # set startDatetime and reset planStartDatetime
        patch.startDatetime = datetime.datetime.now()
        patch.planStartDatetime = None
        connection.session.commit()

        # start simulation to reflect changes
        simulationConnector.startSimulation()

        if not timeWithinWorkHours(patch.user, datetime.datetime.now()):
            flash('The patch is started out of working hours', 'warning')

        return redirect(url_for('shopfloormanagement.index'))
    else:
        availableWorkers = [
            w for w in User.query.filter(User.role == 'Technician').all()
            if w.currentPatch is None
        ]
        form.user.query_factory = lambda: availableWorkers
        availableMachines = [
            m for m in Machine.query.all()
            if m.currentPatch is None
        ]
        form.machine.query_factory = lambda: availableMachines
        # pre-select fields
        if current_user.role == 'Technician':
            if current_user.currentPatch is None:  # current user currently not busy
                form.user.query_factory = lambda: [current_user]  # technicians can assign patches only to themselves
            else:  # current user is busy. We cannot assign new work
                flash('Please report finished current work before starting new one.', 'warning')
                return redirect(url_for('shopfloormanagement.index'))
        else:
            if patch.user is availableWorkers:
                form.user.default = patch.user
        if patch.machine in availableMachines:
            form.machine.default = patch.machine
        return render_template(
            'forms/patch.start.html',
            form=form,
            form_patch_id=patchNumber,
            form_operation_id=operationNumber,
            form_machine_id=machineNumber,
            form_user_id=userNumber,
            patch=patch,
            currentDatetime=datetime.datetime.now()
        )

def timeWithinWorkHours(user, dateTime):
    # convert resource database object to simulation objects. These include a calendar with availabilities
    # resourceSimulationObjects = 
    resourceSimulationObject = createLaborResourcesFromDatabaseObject(user,
        startDate=dateTime.date(),
        endDate=dateTime.date())
    # find out the availabilities of the users along the time interval
    availabilityDictionary = {
        resourceSimulationObject.id: [(a[0], a[1]) for a in resourceSimulationObject.availabilities]
    }
    withinWorkingHours = False

    for resourceId, availability in availabilityDictionary.iteritems():
        if availability:
            for a in availability:
                if a[0] <= dateTime and dateTime <= a[1]:
                    withinWorkingHours = True
    return withinWorkingHours

@shopfloormanagement.route("/patch/<int:patchNumber>/end", methods=['GET', 'POST'])
def endPatchNow(patchNumber):
    if not current_user.role in ['Technician', 'Manager']:
        return redirect(url_for('index'))
    patch = Patch.query.get(patchNumber)
    if patch.startDatetime is None:
        flash('This patch has not been started yet, work cannot be reported back.', 'warning')
        redirect(url_for('shopfloormanagement.index'))

    # Only managers and the technician who worked on the patch are allowed to report back work
    if current_user.role != 'Manager' and patch.user != current_user:
        flash('You are not allowed to report back on this operation.', 'warning')
        redirect(url_for('shopfloormanagement.index'))

    form = EndPatchNowForm()
    # update the max value validator for the workContentCovered validator
    v = filter(
        lambda e: 'NumberRange' in str(type(e)),
        form.workContentCovered.validators
    )[0]
    v.max = int(math.ceil(patch.operation.outstandingWorkContent))

    if form.validate_on_submit():
        ## update user and machine from form
        form.populate_obj(patch)

        simUser = simCal = createCalendarFromDatabaseObject(patch.user, patch.startDatetime.date(), datetime.date.today() + datetime.timedelta(days=7))
        # get timestamp that we want to use as end
        patchEnd = datetime.datetime.now()
        # It might be that the start of the current patch was in another availability, because the
        # employees forgot to report work back. So check if we need to split up the patch into
        # multiple patches that fall into different availabilities
        l = []  # create list of start-/end datetime tuples
        # Initialize start and end helper variables
        start = patch.startDatetime
        end = min(
            simUser.becomesUnavailableNextAfter(start) if simUser.becomesUnavailableNextAfter(start) is not None else patchEnd,
            patchEnd
        )
        # If the end of the availability does not stretch until the intended end of patch,
        # generate additional patches
        if end < patchEnd:
            while end < patchEnd:
                # Check when next availability would start
                newStart = simUser.becomesAvailableNextAfter(end)
                if newStart < patchEnd:  # Patch reaches into next availability
                    l.append((start, end))
                    start = newStart
                    end = min(
                        simUser.becomesUnavailableNextAfter(start),
                        patchEnd
                    )
                else:  # patchEnd lies between before the start of the next availability
                    l.append((start, patchEnd))
                    break  # do not continue iterating  through availabilities
            # If we moved the start helper variable, we have to add the last start/end tuple
            if start > patch.startDatetime:
                l.append((start, end))
        else:
            # add the only patch
            l.append((start, end))

        # Now that we got the timeinformation, we need to distribute the accomplished
        # work on all possible patches
        workContentCovered = max(form.workContentCovered.data, patch.operation.outstandingWorkContent)
        # Calculate length of all patches (in seconds)
        totalLength = sum([(end-start).total_seconds() for end, start in l])
        # Calcualte work content per start/end tuple
        workContentsPerPatch = [
            (end-start).total_seconds()/totalLength * workContentCovered
            for end, start in l
        ]

        # Now we start updating the database
        # First we update the started patch, by setting endtime, accomplished workcontent
        # and removing the planEndDatetime
        patch.endDatetime = l[0][1]  # endpoint of the first start/end tuple
        patch.planEndDatetime = None
        patch.workContentCovered = workContentsPerPatch[0]
        # now check if we need more patch instances
        if len(l) > 1:
            for i in range(1, len(l)):
                start, end = l[i]
                newPatch = Patch(
                    operation=patch.operation,
                    workContentCovered=workContentsPerPatch[i],
                    startDatetime=start,
                    endDatetime=end,
                    user=patch.user,
                    machine=patch.machine
                )
                connection.session.add(newPatch)

        connection.session.commit()
        # if the operation is to be paused here, this has to be saved in the operation object
        if form.restartAfterDatetime.data is not None:
            patch.operation.restartAfterDatetime = form.restartAfterDatetime.data

        connection.session.commit()

        # start simulation to reflect changes
        simulationConnector.startSimulation()
        if not timeWithinWorkHours(patch.user,datetime.datetime.now()):
            flash('The patch is stopped/paused out of working hours', 'warning')

        return redirect(url_for('shopfloormanagement.index'))
    else:
        return render_template(
            'forms/patch.end.html',
            form=form,
            form_patch_id=patchNumber
        )

def createSchedule(startDate, endDate, dHour, resources, events):
    """
    Create schedule object that can be passed to template to create schedule of either appointments
    or work patches

    Parameters
    ----------
    startDate : datetime.datetime
    endDate : datetime.datetime
        End of the schedule. This date is NOT included in the schedule
    dHour : float
        time difference (in hours) between timestamps on the y-axis
    resources : iterable
        List of Users to be considered. Each resource gets one column.
    events : iterable
        List of events to be shown. Events are expected to be object instances and have to have
        startDatetime and endDatetime properties
    prevURL, nextURL : str
        URLs that allow the user to see the schedule for a shifted timewindow

    Returns
    -------
    schedule : dict
    """
    # convert resource database object to simulation objects. These include a calendar with availabilities
    resourceSimulationObjects = map(
        lambda e: createLaborResourcesFromDatabaseObject(
            e,
            startDate=startDate,
            endDate=endDate-datetime.timedelta(days=1)
        ),
        resources
    )
    # find out the availabilities of the users along the time interval
    availabilityDictionary = {
        s.id: [(a[0], a[1]) for a in s.availabilities]
        for s in resourceSimulationObjects
    }
    # find minimum start and max endtimes across all users so we can layout the time-axis
    dailyStartEndTimes = {}
    for s, availabilities in availabilityDictionary.iteritems():
        for start, end in availabilities:
            if start.date() not in dailyStartEndTimes.keys():
                dailyStartEndTimes[start.date()] = (start.time(), end.time())
            else:
                dailyStartEndTimes[start.date()] = (
                    min(dailyStartEndTimes[start.date()][0], start.time()),
                    max(dailyStartEndTimes[start.date()][1], end.time())
                )
    # create timestamps for timeline (dHour brackets)
    timestamps = []
    for day in sorted(dailyStartEndTimes.keys()):
        
        startHour = min(datetime.datetime.now().hour, dailyStartEndTimes[day][0].hour)
        endHour = max(datetime.datetime.now().hour + dHour, dailyStartEndTimes[day][1].hour + dHour)
        h = startHour
        while h <= endHour:
            timestamps.append(
                datetime.datetime.combine(day, datetime.time(0, 0, 0)) + datetime.timedelta(hours=h)
            )
            h += dHour
    schedule = {}
    schedule['now'] = datetime.datetime.now()
    schedule['resources'] = {s.id: s.name for s in resources}
    schedule['timestamps'] = timestamps
    schedule['availabilities'] = availabilityDictionary
    # convert start and enddates to datetime objects
    startDate = datetime.datetime.combine(startDate, datetime.datetime.min.time())
    endDate = datetime.datetime.combine(endDate, datetime.datetime.min.time()) + datetime.timedelta(days=1)

    eventDict = {s.id: [] for s in resources}
    for e in events:
        if e.user in resources:
            eventDict[e.user.id].append(e)
    schedule['events'] = eventDict

    return schedule


def createCapacityProfile(startDate, endDate):
    """
    Create overview over used and available capacity for all technicians.

    Parameters
    ----------
    startDate, endDate : datetime.Date

    Returns
    -------
    d : dict
    """
    # get list of all technicians
    technicians = User.query.filter(User.role == 'Technician').all()
    # get list of all groups that technicians can be assigned to
    groups = Group.query.all()
    # get list of all days between startDate and (including) endDate
    days = [startDate + datetime.timedelta(days=i) for i in range((endDate - startDate).days + 1)]
    # start creating the resulting datastructure
    dailyStats = {
        d: {
            'presentCapacity': 0,
            'presentCapacity_perGroup': {
                g: 0
                for g in groups
            },
            'usedCapacity': 0,
            'usedCapacity_perGroup': {
                g: 0
                for g in groups
            }
        }
        for d in days
    }
    # first calculate the present capacity and free capacity before accounting for any patches
    for t in technicians:
        # translate the calendar to a simulation calendar instance
        simCal = createCalendarFromDatabaseObject(t, startDate, endDate)
        for d in days:
            # get the capacity (in TUs) the worker provided this day, taking into account the productivity
            presentCapacity = simCal.workingHoursBetween(
                datetime.datetime.combine(d, datetime.datetime.min.time()),
                datetime.datetime.combine(d, datetime.datetime.max.time())
            ) * 100 * t.productivity
            # add this to the present and available capacity
            dailyStats[d]['presentCapacity'] += presentCapacity
            for g in t.groups:
                dailyStats[d]['presentCapacity_perGroup'][g] += presentCapacity
    # now iterate through patches, calculate the used capacity and subtract the free capacity
    startDatetime = datetime.datetime.combine(startDate, datetime.datetime.min.time())
    endDatetime = datetime.datetime.combine(endDate, datetime.datetime.max.time())
    startedPatches = Patch.query.filter(Patch.startDatetime >= startDatetime).filter(Patch.startDatetime <= endDatetime)
    plannedPatches = Patch.query.filter(Patch.planStartDatetime >= startDatetime).filter(Patch.planStartDatetime <= endDatetime)
    patches = startedPatches.union(plannedPatches).all()
    for p in patches:
        # find the technician who worked on this patch
        t = p.user
        # calculate the capacity in hours used by this patch
        capacityDemand = p.length * 100 * t.productivity
        # find startdate
        start = p.startDatetime if p.startDatetime is not None else p.planStartDatetime
        dailyStats[start.date()]['usedCapacity'] += capacityDemand
        # subtract free capacity for all groups to which the technician belongs
        for g in t.groups:
            dailyStats[start.date()]['usedCapacity_perGroup'][g] += capacityDemand

    # we are done
    return dailyStats


@shopfloormanagement.route(
    "/job/<int:jobNumber>/addappointment/<inbound>",
    methods=['GET', 'POST'],
    defaults={'startDate': None}
)
@shopfloormanagement.route("/job/<int:jobNumber>/addappointment/<inbound>/<startDate>", methods=['GET', 'POST'])
@login_required
def addAppointment(jobNumber, inbound, startDate):
    job = Job.query.get(jobNumber)
    form = AppointmentForm()

    # calculate recommended release times and pickup appointments
    recommendedDropOffEnd = None
    recommendedPickUpStart = None
    if inbound == 'inbound':
        recommendedDropOffEnd = calculateRecommendedReleaseDatetime(job)
    else:
        recommendedPickUpStart = calculateRecommendedEndDatetime(job)

    if form.validate_on_submit():
        endAppointmentDatetime = form.startDatetime.data + datetime.timedelta(minutes=form.duration.data)
        # If scheduling a pick up appointment, make sure it is not earlier than planned drop off
        if inbound != 'inbound' and job.dropOffAppointment is not None and form.startDatetime.data <= job.dropOffAppointment.endDatetime:
            flash('Pick up appointment cannot be scheduled before end of drop off appointment!', 'warning')
        # If scheduling a drop off appointment, make sure it is not later than planned pick up
        elif inbound == 'inbound' and job.pickUpAppointment is not None and form.startDatetime.data + datetime.timedelta(minutes=form.duration.data) >= job.pickUpAppointment.startDatetime:
            flash('Drop off appointment cannot be scheduled after start of pick up appointment!', 'warning')
        # Check appointment overlappings
        elif isAppointmentOverlapping(form.startDatetime.data, endAppointmentDatetime, form.user.data.id, job.id, inbound == 'inbound'):
            flash('The advisor already has an appointment that intersects with the chosen timeslot.', 'warning')
        else:  # everything is alright, go ahead.
            appointment = Appointment(
                startDatetime=form.startDatetime.data,
                endDatetime=form.startDatetime.data + datetime.timedelta(minutes=form.duration.data),
                inbound=(inbound == 'inbound'),
                user=form.user.data
            )
            connection.session.add(appointment)

            # update job object and delete existing appointments where necessary
            if inbound == 'inbound':
                if job.dropOffAppointment is not None:
                    connection.session.delete(job.dropOffAppointment)
                    connection.session.commit()
                job.dropOffAppointment = appointment
                if recommendedDropOffEnd is not None and job.dropOffAppointment.endDatetime > recommendedDropOffEnd:
                    flash("""Drop-off appointment will be completed after recommended date and time. 
                        It may lead to capacity overbooking. Please, change the date later to avoid it.""", "warning")
            else:
                if job.pickUpAppointment is not None:
                    connection.session.delete(job.pickUpAppointment)
                    connection.session.commit()
                job.pickUpAppointment = appointment
                if recommendedPickUpStart is not None and job.pickUpAppointment.startDatetime < recommendedPickUpStart:
                    flash("""Pick-up appointment will start before recommended date and time. 
                    It may lead to capacity overbooking. Please, change the date later to avoid it.""", "warning")
            connection.session.commit()

            flash('Appointment added to {}.'.format(job.name), 'success')
            if (job.pickUpAppointment is not None and job.dropOffAppointment is not None):
                jobDaysDuration = (job.pickUpAppointment.endDatetime - job.dropOffAppointment.startDatetime).days
                if (jobDaysDuration >= 1):
                    flash('The job will take {} days'.format(jobDaysDuration), 'warning')
            # start simulation to reflect changes
            simulationConnector.startSimulation()

            return redirect(url_for('shopfloormanagement.editJob', jobNumber=jobNumber))

    # Else:  # no valid form submitted -> show form
    # if either appointment is set for this job, preset the start date

    if startDate is None:  # no startDate was given
        if job.dropOffAppointment is not None:
            startDate = job.dropOffAppointment.startDatetime.date()
        elif job.pickUpAppointment is not None:
            startDate = job.pickUpAppointment.startDatetime.date()
        else:
            startDate = datetime.date.today()
    else:
        startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    # by default, do not start with date in the past
    startDate = max(startDate, datetime.date.today())

    form = AppointmentForm()
    return render_template(
        'forms/appointment.add.html',
        form=form,
        form_job_id=jobNumber,
        form_appointment_inbound=inbound,
        recommendedDropOffEnd=recommendedDropOffEnd,
        recommendedPickUpStart=recommendedPickUpStart,
        startDate=startDate,
        urlTemplate=url_for(
            'shopfloormanagement.addAppointment',
            jobNumber=jobNumber,
            inbound=inbound,
            startDate=startDate.strftime('%Y-%m-%d')
        ).replace(startDate.strftime('%Y-%m-%d'), '%Y-%m-%d')
    )


def isAppointmentOverlapping(startAppointmentDatetime, endAppointmentDatetime, userId, jobId, isInbound):
    """Looks for ovelapping appointments for user

    Keyword arguments:
    startAppointmentDatetime -- datetime of the start of the planned appointment
    endAppointmentDatetime -- datetime of the end of the planned appointment
    userId -- id of the user that appointment is planned for 
    """
    #Get all pick up appointments of the chosen user
    userAppointments = Appointment.query.filter( \
        Appointment.user_id == userId \
        and (Appointment.startDatetime.date() == startAppointmentDatetime.date() \
            or Appointment.endDatetime.date() == endAppointmentDatetime.date())) \
        .all()

    #Check time intersections for each existing appointment
    for userAppointment in userAppointments:
        #Do not check for overlapping for the same appointment (previously set -> will be overriden by current). Happens when we change an appointment.
        if isInbound:
            if (userAppointment.dropOffJob is not None and userAppointment.dropOffJob.id == jobId):
                continue 
        else:
            if (userAppointment.pickUpJob is not None and userAppointment.pickUpJob.id == jobId):
                continue 

        if not (startAppointmentDatetime > userAppointment.endDatetime or userAppointment.startDatetime > endAppointmentDatetime):
            return True
    return False


@shopfloormanagement.route("/appointment/<int:appointmentNumber>/prepare")
@login_required
def prepareAppointment(appointmentNumber):
    appointment = Appointment.query.get(appointmentNumber)
    if appointment.dropOffJob is None:   # this appointment was not a drop off appointment
        flash("Only drop-off appointments may be prepared.", 'danger')
        return redirect(url_for('shopfloormanagement.index'))
    if not current_user.role in ['ServiceAdvisor', 'Manager']:
        flash("You are not allowed to perform this operation.", 'danger')
        return redirect(url_for('shopfloormanagement.index', jobNumber=appointment.dropOffJob.id))
    appointment.prepared = True
    flash("Appointment marked as prepared.", 'Success')
    connection.session.commit()
    return redirect(url_for('shopfloormanagement.editJob', jobNumber=appointment.dropOffJob.id))


@shopfloormanagement.route("/appointment/<int:appointmentNumber>/delete")
@login_required
def deleteAppointment(appointmentNumber):
    appointment = Appointment.query.get(appointmentNumber)
    job = appointment.dropOffJob if appointment.dropOffJob is not None else appointment.pickUpJob

    connection.session.delete(appointment)
    connection.session.commit()

    # start simulation to reflect changes
    simulationConnector.startSimulation()

    flash('Appointment deleted to {}.'.format(job.name), 'success')
    return redirect(url_for('shopfloormanagement.editJob', jobNumber=job.id))
