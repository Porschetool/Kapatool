from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from flask_login import login_required, login_user, logout_user, current_user

from . import calendarmanagement
from forms import *
from app import connection
from app.db.models import Calendar, RepeatingCalendarEntry, CalendarEntry
from app.simulationmanagement import simulationConnector
from datetime import datetime


@calendarmanagement.route("/")
@calendarmanagement.route("/index")
@login_required
def index():
    if not current_user.is_admin:
        flash("The requsted page is only available for users for admin rights.", 'danger')
        return redirect(url_for('index'))
    calendars = connection.session.query(Calendar).all()
    # prepare add form. Remove finalized field, since it does not make sense to finalize calendar before
    # adding availabilities
    calendarform = CalendarForm()
    del calendarform.finalized
    # Get list of regular calendar entries not in the past
    calendarEntries = CalendarEntry.query.filter(
        CalendarEntry.endDatetime >= datetime.now()
    ).order_by(
        CalendarEntry.startDatetime
    ).all()

    return render_template(
        'pages/calendar.index.html',
        calendars=calendars,
        calendarAddForm=calendarform,
        calendarEntryAddForm=CalendarEntryForm(),
        calendarEntries=calendarEntries
    )


@calendarmanagement.route("/add", methods=['GET', 'POST'])
@login_required
def add():
    if not current_user.is_admin:
        flash("The requsted page is only available for users for admin rights.", 'danger')
        return redirect(url_for('index'))
    form = CalendarForm()
    del form.repeatingEntries  # do not offer repeating entries form during creation state
    if form.validate_on_submit():
        calendar = Calendar()
        form.populate_obj(calendar)

        connection.session.add(calendar)
        connection.session.commit()

        flash('New Calendar {} created.'.format(calendar.name), 'success')

        return redirect(url_for('calendarmanagement.edit', calendarNumber=calendar.id))

    return render_template(
        'forms/calendar.add.html',
        form=form
    )


@calendarmanagement.route("/<int:calendarNumber>/edit", methods=['GET', 'POST'])
@login_required
def edit(calendarNumber):
    if not current_user.is_admin:
        flash("The requsted page is only available for users for admin rights.", 'danger')
        return redirect(url_for('calendarmanagement.index'))
    calendar = Calendar.query.get(calendarNumber)
    if calendar.finalized:
        flash("This calendar has been locked from further editing.", 'danger')
        return redirect(url_for('calendarmanagement.index'))
    form = CalendarForm(obj=calendar)
    if form.validate_on_submit():
        # save changes to edited calendar
        form.populate_obj(calendar)
        connection.session.commit()

        flash('Calendar {} edited.'.format(calendar.name), 'success')

        return redirect(url_for('calendarmanagement.index'))

    return render_template(
        'forms/calendar.edit.html',
        form=form,
        form_calendar_id=calendarNumber,
        repeatingEntryAddForm=RepeatingCalendarEntryForm()
    )


@calendarmanagement.route("/<int:calendarNumber>/finalize", methods=['GET'])
@login_required
def finalize(calendarNumber):
    if not current_user.is_admin:
        flash("The requsted page is only available for users for admin rights.", 'danger')
        return redirect(url_for('index'))
    calendar = Calendar.query.get(calendarNumber)
    calendar.finalized = True
    connection.session.commit()

    flash('Calendar {} finalized.'.format(calendar.name), 'success')

    return redirect(url_for('calendarmanagement.index'))


@calendarmanagement.route("/entry/add", methods=['GET', 'POST'])
@login_required
def addCalendarEntry():
    if not current_user.role == 'Manager':
        flash("The requsted page is only available for shop managers.", 'danger')
        return redirect(url_for('index'))
    form = CalendarEntryForm()
    if form.validate_on_submit():
        entry = CalendarEntry()
        form.populate_obj(entry)
        # check for errors
        if len(entry.users) == 0 or entry.startDatetime < datetime.now() or entry.endDatetime <= entry.startDatetime:
            if len(entry.users) == 0:
                flash('No user selected.', 'warning')
            else:
                flash('Chosen dates incorrect. Startdate must lay in the future. Enddate must be after start.', 'warning')
            return render_template(
                'forms/CalendarEntry.add.html',
                form=form
            )
        connection.session.add(entry)
        connection.session.commit()
        flash('Calendar entry added.', 'success')
        return redirect(url_for('calendarmanagement.index'))
    else:
        return render_template(
            'forms/CalendarEntry.add.html',
            form=form
        )


@calendarmanagement.route("/<int:calendarNumber>/repeatingEntry/add/", methods=['GET', 'POST'])
@login_required
def addRepeatingEntry(calendarNumber):
    if not current_user.is_admin:
        flash("The requsted page is only available for users with admin rights.", 'danger')
        return redirect(url_for('index'))
    form = RepeatingCalendarEntryForm()
    if form.validate_on_submit():
        calendar = Calendar.query.get(calendarNumber)
        if calendar.finalized:
            flash("This calendar has been marked as finalized and can no longer be edited.", 'danger')
            return redirect(url_for('calendarmanagement.index'))

        connection.session.add(
            RepeatingCalendarEntry(
                form.dayOfWeek.data,
                form.startTime.data,
                form.endTime.data,
                calendarNumber
            )
        )
        connection.session.commit()

        flash('New Entry added to calendar {}.'.format(calendar.name), 'success')
        # start simulation to reflect changes
        simulationConnector.startSimulation()

        return redirect(url_for('calendarmanagement.edit', calendarNumber=calendarNumber))

    return render_template(
        'forms/repeatingEntry.add.html',
        form=form,
        form_calendar_id=calendarNumber
    )
