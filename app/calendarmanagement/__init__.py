from flask import Blueprint

calendarmanagement = Blueprint(
    'calendarmanagement',
    __name__,
    template_folder='templates',
    static_folder='static'
)

from . import views

