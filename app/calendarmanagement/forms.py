#!/usr/bin/env python
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, FormField, FieldList, SelectField
from wtforms_components import TimeField, DateTimeField
from wtforms.validators import DataRequired, EqualTo, Length

from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField
from flask_admin.form.widgets import Select2Widget

from app.db.models import User, Group, RepeatingCalendarEntry, Setting


class CalendarEntryForm(FlaskForm):
    startDatetime = DateTimeField(
        'Start',
        format=Setting.query.get('defaultDatetimeFormat').formattedValue,
        validators=[DataRequired()]
    )
    endDatetime = DateTimeField(
        'End',
        format=Setting.query.get('defaultDatetimeFormat').formattedValue,
        validators=[DataRequired()]
    )
    present = SelectField(
        'Type',
        choices=[
            (True, 'Present'),
            (False, 'Absent')
        ],
        coerce=bool, # Original: lambda x: x == 'False',  # properly coerce string to boolean, c.f. https://stackoverflow.com/a/33430086; But this doesn't work. Simply added coerce=bool is enough, c.f. https://stackoverflow.com/questions/35460282/flask-wtf-dynamic-select-field-gives-none-as-a-string
        description="Determines if the users are present or absent from the shop during the specified time."
    )
    users = QuerySelectMultipleField(
        query_factory=lambda: User.query.all(),
        get_label=lambda g: '{}{}'.format(
            g.name,
            ' (shop calendar)' if g.shopCalendar else ''
        ),
        description="The calendar entry will be applied to all users selected here."
    )


class RepeatingCalendarEntryForm(FlaskForm):
    dayOfWeek = SelectField(
        'Day of Week',
        choices=[
            (0, 'Monday'),
            (1, 'Tuesday'),
            (2, 'Wednesday'),
            (3, 'Thursday'),
            (4, 'Friday'),
            (5, 'Saturday'),
            (6, 'Sunday')
        ],
        coerce=int
    )
    startTime = TimeField(
        'Start time',
        validators=[DataRequired()],
        format=Setting.query.get('defaultTimeFormat').formattedValue
    )
    endTime = TimeField(
        'End time',
        validators=[DataRequired()],
        format=Setting.query.get('defaultTimeFormat').formattedValue
    )


class CalendarForm(FlaskForm):
    name = StringField('Calendar Name', validators=[DataRequired()])
    repeatingEntries = FieldList(
        FormField(RepeatingCalendarEntryForm),
        min_entries=0
    )
    finalized = BooleanField(
        'Calendar is finalized',
        description="Users flagged as quality inspectors will be allowed to confirm a car's quality"
    )

