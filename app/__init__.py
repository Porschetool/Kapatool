from flask import Flask
from flask_sqlalchemy import SQLAlchemy
# libraries to minimify and bundle dynamic output and static files (JS/CSS)
from flask_htmlmin import HTMLMIN
from flask_assets import Bundle, Environment
from flask_sqlalchemy import SQLAlchemy

import os
import shutil
import time
import datetime

# Run App in UTF-8 to fix string encoding issues, http://stackoverflow.com/a/14919377
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# initiate Database connection
connection = SQLAlchemy()
# Initiate Flask Assets
# the actual assets (collections of JS/CSS files) are defined directly in the layouts
assets = Environment()
# inititate HTML Minimization
htmlmin = HTMLMIN()
# load login functionality
from flask_login import LoginManager, login_user, login_required
# instantiate login manager
global login_manager
login_manager = LoginManager()


# start CSRF Protection
from flask_wtf.csrf import CsrfProtect
global csrf
csrf = CsrfProtect()


# Define Jinja2 Helper functions, c.f. http://stackoverflow.com/a/41531941
def filterInstancesForClass(l, className):
    """
    Filter list of instances l and only return elements of type className

    Parameters
    ----------
    l : iterable
    className : str

    Returns
    -------
    l : list
    """
    return filter(
        lambda e: className in str(type(e)),
        l
    )


def create_app(config_name='config'):
    """
    Main function that creates an Flask app object on request.

    Parameters
    ----------
    config_name : str
        Name under which the configuration object can be found

    Returns
    -------
    app : Flask.app
    """
    app = Flask(__name__)
    app.config.from_object(config_name)

    assets.init_app(app)
    htmlmin.init_app(app)
    login_manager.init_app(app)
    csrf.init_app(app)
    connection.init_app(app)

    from app.db.models import User, Setting

    with app.app_context():
        # Intialize Tables
        connection.create_all()
        connection.session.commit()
        # create admin user in case no user currently exists
        if len(User.query.all()) == 0:
            print 'Creating default user: admin / password'
            admin = User(
                name='admin',
                password='password',
                admin=True,
                productivity=1.0,
                setupTime=0,
                role='Manager',
                shopCalendar=True
            )
            connection.session.add(admin)
            connection.session.commit()

        # Set default config
        if len(Setting.query.all()) == 0:
            connection.session.add(Setting(id='flowFactor', label="Flow Factor", value="1.5"))
            connection.session.add(Setting(id='postProcessingWorkContent', label="Post processing Work Content [TU]", value="50"))
            connection.session.add(Setting(id='defaultDateFormat', label='Default Date Format', value="%Y-%m-%d"))
            connection.session.add(Setting(id='shortDateFormat', label='Short Date Format', value="%m-%d"))
            connection.session.add(Setting(id='defaultDatetimeFormat', label='Default Datetime Format', value="%Y-%m-%d %H:%M"))
            connection.session.add(Setting(id='defaultTimeFormat', label='Default Time Format', value="%H:%M"))
            connection.session.add(Setting(id='timezone', label='Time Zone (please edit using dropdown menu)', value='Europe/Berlin'))
            connection.session.commit()
            print 'Default setup imported'

        # if this is a UNIX operating system, switch to the selected timezone
        if sys.platform == "linux" or sys.platform == "linux2":
            tz = Setting.query.get('timezone')
            if tz is not None:
                os.environ['TZ'] = 'Europe/Berlin'
            else:
                os.environ['TZ'] = tz.formattedValue
            time.tzset()

        # Define application view for user login
        login_manager.login_view = "main.login"

        # User loader function required to translate user ID into User object. Should return None if ID not foudn
        @login_manager.user_loader
        def load_user(userid):
            return User.query.get(userid)

        # register helper function with template engine
        app.jinja_env.globals.update(filterInstancesForClass=filterInstancesForClass)

        # add date/time formatting strings to the template namespace
        # c.f. http://flask.pocoo.org/docs/0.10/templating/#context-processors
        @app.context_processor
        def inject_formatStrings():
            return dict(formatString={
                'date': Setting.query.get('defaultDateFormat').formattedValue,
                'shortDate': Setting.query.get('shortDateFormat').formattedValue,
                'datetime': Setting.query.get('defaultDatetimeFormat').formattedValue,
                'time': Setting.query.get('defaultTimeFormat').formattedValue
            })

        # Add blueprints to application
        # load main blueprint for login/logout/index views
        from app.views import main
        app.register_blueprint(main)
        # load other blueprints
        from usermanagement import usermanagement
        from machinemanagement import machinemanagement
        from calendarmanagement import calendarmanagement
        from simulationmanagement import simulationmanagement
        from shopfloormanagement import shopfloormanagement
        from apomanagement import apomanagement
        from settingmanagement import settingmanagement
        from api import api
        # add user management to application
        app.register_blueprint(
            usermanagement,
            url_prefix='/user',
            template_folder='usermanagement/templates'
        )

        # add machine (workplace) management to application
        app.register_blueprint(
            machinemanagement,
            url_prefix='/machine',
            template_folder='machinemanagement/templates'
        )

        # add user management to application
        app.register_blueprint(
            calendarmanagement,
            url_prefix='/calendar',
            template_folder='calendarmanagement/templates'
        )

        # add capacity to communicate with simulation server
        app.register_blueprint(
            simulationmanagement,
            url_prefix='/provider',
            template_folder='simulationmanagement/templates'
        )

        # add capacity to manage APOS
        app.register_blueprint(
            apomanagement,
            url_prefix='/apos',
            template_folder='apomanagement/templates'
        )
        # add capacity to manage jobs and operations
        app.register_blueprint(
            shopfloormanagement,
            url_prefix='/shopfloor',
            template_folder='operationsmanagement/templates'
        )

        # add capacity to change settings
        app.register_blueprint(
            settingmanagement,
            url_prefix='/settings',
            template_folder='settingsmanagement/templates'
        )

        app.register_blueprint(
            api,
            url_prefix='/api',
            template_folder='api/templates'
        )

        # Set up code to be run before any request.
        @app.before_request
        def before_request():
            dbname = os.path.dirname(__file__) + '/../porsche.db'
            backupname = dbname.replace(
                '.db',
                '_{}.db'.format(
                    datetime.date.today().strftime('%Y%m%d')
                )
            )

            if not os.path.isfile(backupname):
                print 'need to backup'
                backup_database()

    return app
## DEBUGGING
#from flask_debugtoolbar import DebugToolbarExtension
#toolbar = DebugToolbarExtension(app)


def backup_database():
    dbname = os.path.dirname(__file__) + '/../porsche.db'
    backupname = dbname.replace(
        '.db',
        '_{}.db'.format(
            datetime.date.today().strftime('%Y%m%d')
        )
    )

    shutil.copy2(
        dbname,
        backupname
    )
    print 'Backed up database'

    # Check if we need to delete old database versions
    databases = [
        f
        for f in os.listdir(os.path.dirname(__file__) + '/../')
        if f.startswith('porsche_')
        and f.endswith('.db')
    ]
    for f in databases:
        # extract date from filename
        try:
            date = datetime.datetime.strptime(f, 'porsche_%Y%m%d.db')
            print date
            if (datetime.datetime.now() - date).total_seconds() > 3600*24*7:  # date is older than one week
                print 'removing file'
                os.remove(os.path.dirname(__file__) + '/../' + f)
        except ValueError:  # filename did not match pattern, ignore
            continue

