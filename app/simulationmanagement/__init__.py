from flask import Blueprint

simulationmanagement = Blueprint(
    'simulationmanagement',
    __name__,
    template_folder='templates',
    static_folder='static'
)

from . import views

