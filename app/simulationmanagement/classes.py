import numpy as np
import pandas as pd
from datetime import date, timedelta, time, datetime
import itertools
import uuid
from intervaltree import Interval, IntervalTree
from sortedcontainers import SortedDict
import random
import re


class enhancedIntervalTree(IntervalTree):
    """
    Class extends the IntervalTree object provided by the intervaltree package by a possibility
    to extract the right-most and left-most interval objects (not just the start and end values)
    """
    def endElement(self):
        """
        Returns one element that defines the end of the intervaltree,
        i.e. where Interval.end == IntervalTree.end()

        Returns
        -------
        i : intervaltree.Interval
        """
        n = self.top_node
        while n.right_node is not None:
            n = n.right_node
        return max(n.s_center, key=lambda i: i.end)

    def startElement(self):
        """
        Returns one element that defines the end of the intervaltree,
        i.e. where Interval.begin == IntervalTree.begin()

        Returns
        -------
        i : intervaltree.Interval
        """
        n = self.top_node
        while n.left_node is not None:
            n = n.left_node
        return min(n.s_center, key=lambda i: i.start)


class scheduledResource(object):
    """
    Base class that provides inheriting classes with the ability to manage availabilities in schedule.
    """
    def __init__(
        self,
        startDate=date.today(),
        endDate=date.today().replace(year=date.today().year+1)
    ):
        """
        Constructor

        Parameters
        ----------
        startDate : datetime.date
            First day of the calendar. Default: Today
        endDate : datetime.date
            Last day of the calendar. Default: Today + 1 year
        """
        self._startDate = startDate
        self._endDate = endDate
        self._availabilities = enhancedIntervalTree()

    def addResourceAvailability(self, startDatetime, endDatetime):
        """
        Add resource availability to the calendar. Both parameters can also be iterable of equal length.
        In this case, the corresponding entries in both lists will be considered start and enddatetimes
        of availabilities.

        Parameters
        ----------
        startDatetime : datetime or list
            (list of) Start datetime of the availability
        endDatetime : datetime or list
            (list of) End datetime of the availability
        """
        # check if parameters are iterables.
        if hasattr(startDatetime, '__iter__'):
            assert len(startDatetime) == len(endDatetime), 'Lengths of lists do not match'
            self._availabilities = self._availabilities.union(
                Interval(*e) for e in zip(startDatetime, endDatetime)
            )
        else:
            assert isinstance(startDatetime, datetime) & isinstance(endDatetime, datetime), 'Incorrect variable types'
            # store resource availability in dataframe
            self._availabilities.add(Interval(startDatetime, endDatetime))

    def addConstantAvailability(self):
        """
        Function is called for resources (machines) that are constantly (24h/day) available.
        Marks this resource as available throughout the scope of the calendar.
        """
        self._availabilities.add(
            Interval(
                datetime.combine(self._startDate, time(0, 0)),
                datetime.combine(self._endDate, time(23, 59, 59))
            )
        )

    def addDailyAvailability(self, mapFunc, startDate=None, endDate=None, ignoreDates=[]):
        """
        Provides a simple method to add reoccuring availabilities to the schedule.

        Parameters
        ----------
        startDate, endDate : datetime.date
            First and last date for which the availability should be added. If None (default), the
            availability will be added for the entire date span of the scheduled resource.
        mapFunc : callable
            Function should accept a single parameter which is a day (date object) and should return
            a list of tuples of 2 two datetime.time objects indicating the start and endtime of the resource
            availability at that date. If either value is None, no availability is added.
        ignoreDates : iterable
            List of datetime.date objects to ignore (e.g. vacation days). Default: Empty list
        """
        startDate = max(self._startDate, startDate) if startDate is not None else self._startDate
        endDate = min(self._endDate, endDate) if endDate is not None else self._endDate
        # calculate number between start and end of calendar
        dayRange = (endDate - startDate).days

        # prepare lists to collect start and end points
        startDatetimes = []
        endDatetimes = []
        # iterate through all days of the calendar
        for d in [startDate+timedelta(days=n) for n in range(dayRange+1)]:  # +1 to cover also endDate
            if d in ignoreDates:
                continue
            # get start and endtimes as list of (start, end) tuples
            for startTime, endTime in mapFunc(d):
                if None in [startTime, endTime]:
                    continue
                # convert results to datetime instances
                startDatetimes.append(datetime.combine(d, startTime))
                endDatetimes.append(datetime.combine(d, endTime))
        self.addResourceAvailability(tuple(startDatetimes), tuple(endDatetimes))

    def addAbsense(self, startDatetime, endDatetime):
        """
        Remove potentially registered availabilities between startDatetime and endDatetime. Has to be
        called after adding availabilities first. Will not block the addition of availabilities for
        this time horizon in the future.

        Parameters
        ----------
        startDatetime : datetime
            Start datetime of the absense
        endDatetime : datetime
            End datetime of the absense
        """
        self._availabilities.chop(startDatetime, endDatetime)

    def isAvailableAt(self, startDatetime):
        """
        Check if resource is available at time point

        Parameters
        ----------
        startDatetime : datetime

        Returns
        -------
        d : boolean
        """
        # machine is available if and only if there exists a time window in the set of availabilities
        # that covers the datetime.
        return len(self._availabilities[startDatetime]) > 0

    def availabilitiesBetween(self, startDatetime, endDatetime):
        """
        Return set of availabilities that fall within the time range.

        Parameters
        ----------
        startDatetime : datetime
        endDatetime : datetime

        Returns
        -------
        l : list
            list of tuples
        """
        return [
            (e[0], e[1])
            for e in self._availabilities.search(
                startDatetime,
                endDatetime,
                strict=True
            )
        ]

    def becomesAvailableNextAfter(self, startDatetime):
        """
        If resource is available at startDatetime, returns startDatetime, otherwise returns the start
        of the next resource availability. if no other availability is found, returns None.

        Parameters
        ----------
        startDatetime : datetime

        Returns
        -------
        d : datetime
        """
        if self.isAvailableAt(startDatetime):
            return startDatetime
        else:  # resource currently unavailable, find start of next availability
            # get all availabilities between startDatetime and end
            a = self._availabilities[startDatetime:datetime.combine(self._endDate, time(23, 59, 59))]
            if len(a) == 0:
                return None
            else:
                # find earliest start datetime of elements in list
                return sorted(list(a))[0].begin

    def becomesUnavailableNextAfter(self, startDatetime):
        """
        If resource is available at startDatetime, function returns the end of the availability.
        If resource is not available, returns None.

        Parameters
        ----------
        startDatetime : datetime

        Returns
        -------
        r : datetime
            Datetime at which availability of resource ends.
            None, if resource is not available at startDatetime.
        """
        subset = self._availabilities[startDatetime]
        if len(subset) == 0:
            return None
        else:
            assert len(subset) == 1, 'Overlapping availabilities detected'
            # return endDatetime entry of first(and only) row.
            return subset.pop()[1]  # remove sole item from set and return the end point of the interval

    def addWorkingHoursTo(self, startDatetime, n):
        """
        Returns a datetime, by adding n working hours (hours where the resource is available) to startDatetime.
        Returns None if insufficient availabilities are left.
        If n is positive, the returned datetime is later than startDatetime, if n is negative, if will be earlier.

        Parameters
        ---------
        startDatetime : datetime
        n : float
            number of working hours

        Returns
        --------
        endDatetime : datetime
        """
        # get all availabilities that might be relevant
        if n >= 0:
            a = self._availabilities[startDatetime:datetime.combine(self._endDate, time.max)]
        else:
            a = self._availabilities[datetime.combine(self._startDate, time.min):startDatetime+timedelta(days=1)]
        if len(a) == 0:
            return None
        # convert to list and sort in approriate order
        a = sorted(list(a), reverse=(n < 0))  # sort descending, if n < 0
        # iterate through availabilities until we covered n hours
        absN = float(abs(n))
        for availability in a:
            if n >= 0:
                duration = availability.end - max(startDatetime, availability.begin)
            else:
                duration = min(startDatetime, availability.end) - availability.begin
            # convert duration to hours (float)
            duration = float(duration.days)*24 + float(duration.seconds) / 3600 + float(duration.microseconds) / (3600*10**6)
            # check if we need another availability
            if duration < absN:
                absN -= duration  # adjust remaining time to be covered
            else:  # this availability is long enough, so we need to find the datetime within this range
                if n >= 0:
                    return max(startDatetime, availability.begin) + timedelta(hours=absN)
                else:  # n < 0
                    return min(startDatetime, availability.end) - timedelta(hours=absN)

    def workingHoursBetween(self, startDatetime, endDatetime):
        """
        Calculate the number of working hours (hours covered by availabilities) between startDatetime and endDatetime.
        If startDatetime > endDatetime, the result is < 0.

        Parameters
        ----------
        startDatetime : datetime
        endDatetime : datetime

        Returns
        -------
        n : float
            Number of hours
        """
        # if endDatetime is before startdatetime, swap the two values and return negative result in the end
        if endDatetime < startDatetime:
            startDatetime, endDatetime = endDatetime, startDatetime
            resultFactor = -1
        else:
            resultFactor = 1

        # find all availabilities that (partially) cover this period
        a = self._availabilities[startDatetime:endDatetime]
        # add up time
        n = 0
        for availability in a:
            # calculate duration as timedelta
            duration = min(endDatetime, availability.end) - max(startDatetime, availability.begin)
            # convert to hours and add up
            n += float(duration.days)*24 + float(duration.seconds) / 3600 + float(duration.microseconds) / (3600*10**6)
        return resultFactor * n

    def removeAvailabilityOverlaps(self):
        """
        Adjust stored availabilities such that availabilities do not overlap.
        Adjustments are made directly to the interval tree.
        """
        for i in self._availabilities.items():
            # find overlaps with this interval
            overlaps = self._availabilities.search(*i)
            if len(overlaps) > 1:  # more than the current element was found
                # find earliest start and latest end point of all availabilities
                start = min(overlaps, key=lambda e: e[0])[0]  # start point of interval with the lowest start point
                end = max(overlaps, key=lambda e: e[1])[1]  # end point of interval with the highest end point
                # remove all old availabilities
                self._availabilities.remove_overlap(i[0], i[1])
                # add merged one
                self._availabilities.add(Interval(start, end))

    availabilities = property(
        lambda self: self._availabilities.items(),
        lambda self, newValue: False
    )
    """Read-only access to dataframe of availabilities (resource calendar)"""


class simulationManager(object):
    """
    Simulation manager maintains the simulation.
    """
    def __init__(self, laborResources, machineResources, jobs):
        """
        Constructor.

        Parameters
        ----------
        laborResources : iterable
        machineResources : iterable
        jobs : iterable
        """
        self._laborResources = laborResources
        self._machineResources = machineResources
        assert len(set(self._laborResources).intersection(self._machineResources)) == 0, \
            'Overlap between labor and machine resources'
        self._jobs = jobs

    def registerEvent(self, eventDatetime, callback=None, **callParams):
        """
        Register event in the future event list. Event is defined by a datetime, the function to call back
        and potentially a set of named parameters that will be passed to the callback function upon execution

        Parameters
        ----------
        eventDatetime : datetime
        callback : callable
            Optional callable function. If none is given, an "empty" event is created, that simply ensures
            that the simulation watch will stop at that datetime and resource assignments will be checked.
        callParams : kwargs
            Further parameters passed to callback as named parameters
        """
        if eventDatetime < self._simulationDatetime:
            print 'Trying to register event in the past at {}, now it is {}'.format(eventDatetime, self._simulationDatetime)
        assert eventDatetime >= self._simulationDatetime, 'Event lies in the past'
        felEntry = (callback, callParams) if callback is not None else None
        if eventDatetime in self._futureEventList:
            if felEntry is not None:  # time point is already in future event list, no need to add empty events
                self._futureEventList[eventDatetime].append((callback, callParams))
        else:
            if felEntry is not None:
                self._futureEventList[eventDatetime] = [(callback, callParams)]
            else:  # create empty event
                self._futureEventList[eventDatetime] = []

    def startSimulation(
        self,
        startDatetime,
        endDatetime,
        loopCondition=lambda: True
    ):
        """
        (re-)starts the simulation.

        Parameters
        ----------
        startDatetime : datetime
        endDatetime : datetime
        loopCondition : callable
            Function executed at every new simulation step. Simulation is aborted, once the function returns False
        """
        def maintainSimulation():
            """
            Internal function, called by :meth:`startSimulation` to go keep the simulation running
            until we have reached the endDatetime or have run out of events to process.
            """
            while len(self._futureEventList) > 0 and loopCondition():
                # get entry with smallest key and also removes it from dictionary
                nextDatetime, events = self._futureEventList.popitem(last=False)
                # make sure we are not beyond the simulation period yet
                if nextDatetime > endDatetime:
                    break
                self._simulationDatetime = nextDatetime
                # execute events scheduled for this time point
                for e in events:
                    e[0](**e[1])  # call callback with parameters
                # assign resources
                self.assignResources()

        self._startDatetime = startDatetime
        self._endDatetime = endDatetime
        self._simulationDatetime = self._startDatetime
        self._futureEventList = SortedDict()
        self._currentPatches = set()
        self._currentlyRunningJobs = set()
        # delete all patches that start after startDatetime
        for j in self._jobs:
            j.resetStateTo(startDatetime)
        for r in self._laborResources:
            r.resetStateTo(startDatetime)
        for r in self._machineResources:
            r.resetStateTo(startDatetime)

        # identify all currently ongoing patches (Interval objects for now)
        if len(self._jobs) > 0:
            currentIntervalObjects = set.union(*[
                o.patches[self._simulationDatetime]
                for j in self._jobs
                for o in j.operations
            ])
        else:
            currentIntervalObjects = set()
        # From this, identify the set of currently processed patches and jobs
        self._currentPatches = set([i[2] for i in currentIntervalObjects])
        self._currentlyRunningJobs = set(
            [
                j
                for j in self._jobs
                if j.releaseDatetime <= self._simulationDatetime and not j.isCompleted
            ]
        )
        # identify the most recent resources assigned to every job
        self._lastResourcesPerJob = {
            j: j.getLastResources()
            for j in self._jobs
        }

        # load end events of current patches in future event list
        for p in self._currentPatches:
            self.registerEvent(p.endDatetime, self.endPatch, patchObj=p)

        # add all future events where machines become available to the Future Event List
        for r in set(self._laborResources).union(self._machineResources):
            # iterate through all starts of availability patches that lie in the future
            for start, end in r.availabilitiesBetween(self._startDatetime, self._endDatetime):
                self.registerEvent(start)  # register empty event. We just want to initiate assignment

        # add all points in time, where operations become available (restartAfter property is reached) to the future event list
        map(
            lambda o: self.registerEvent(o.restartAfterDatetime),  # add empty event, we just want to initiate assignments
            [o for j in self._jobs for o in j.operations if o.restartAfterDatetime is not None and o.restartAfterDatetime > self._startDatetime]
        )

        # add call events at the start job for which there is currently no active patch (so which will not be
        # reconsidered automatically)
        for j in self._jobs:
            if j not in self.currentlyOccupiedJobs:
                self.registerEvent(
                    max(
                        j.releaseDatetime,
                        self._simulationDatetime
                    ),
                    self.startJob,
                    jobObj=j
                )
        # maintain simulation
        maintainSimulation()

    def getPatches(self):
        """
        Get iterable over all patches in the simulation

        Returns
        -------
        i : iterable
            elements are patch instances
        """
        return [
            i[2]  # i are in fact intervaltree.Interval objects. i[2] gives the stored patch instance
            for j in self._jobs
            for o in j.operations
            for i in o.patches.items()
        ]

    def getPatchDataFrame(self):
        """
        Return pandas dataframe with all patches associated with this simulation.
        Every patch also has a column indiciating the operation and job it belongs to.

        Returns
        -------
        df : pandas.DataFrame
        """
        return pd.DataFrame(
            map(
                lambda p: [
                    p,
                    p.operation,
                    p.job,
                    p.job.id,
                    p.startDatetime,
                    p.endDatetime,
                    p.workContentCovered,
                    p.laborResource,
                    p.laborResource.id,
                    p.machineResource,
                    p.machineResource.id,
                    p.laborSetupTime,
                    p.machineSetupTime
                ],
                [
                    i[2]  # i are in fact intervaltree.Interval objects. i[2] gives the stored patch instance
                    for j in self._jobs
                    for o in j.operations
                    for i in o.patches.items()
                ]
            ),
            columns=(
                'patch',
                'operation',
                'job',
                'job_id',
                'startDatetime',
                'endDatetime',
                'workContentCovered',
                'laborResource',
                'laborResource_id',
                'machineResource',
                'machineResource_id',
                'laborSetupTime',
                'machineSetupTime'
            )
        )

    def startJob(self, jobObj):
        """
        Callback function invoced once new job is released to the shop.

        Parameters
        ----------
        jobObj : job
        """
        self._currentlyRunningJobs.add(jobObj)

    def endPatch(self, patchObj):
        """
        Function called once a patch comes to an end. Releases both resources and takes job from the
        set of jobs under processing.
        """
        # remove patch from currently processed patches
        self._currentPatches.remove(patchObj)
        # just because patch ended, does not mean, that job was in self._currentlyRunningJobs.
        # In particular, a job for which the last patch had already started before the start of the
        # simulation, will be marked as complete and hence not be included in the set of running jobs.
        if patchObj.operation.job.isCompleted and patchObj.operation.job in self._currentlyRunningJobs:
            self._currentlyRunningJobs.remove(patchObj.operation.job)

    def assignResources(self):
        """
        Callback function called at every time step the future event list jumps to (any time point with
        at least one event). To check, if new assignment are now possible.

        Parameters
        ----------
        resObj : resouce
            Resource instance to be assigned.
        """
        def getAssignableOperations():
            """
            Get the queue waiting in front of machine resources.

            Returns
            -------
            l : np.array
                Array of queue elements
            """
            # list operations waiting for machine assignments
            # We want to filter for jobs started yet, but currently not begin processed
            return np.array(
                map(
                    lambda o: {
                        'operation': o,
                        'fractionCompleted': o.fractionCompleted,
                        'planWorkContent': o.planWorkContent,
                        'jobReleaseDatetime': o.job.releaseDatetime,
                        'jobDueDatetime': o.job.dueDatetime,
                        'jobOutstandingWorkContent': o.job.outstandingWorkContent,
                        'simulationDatetime': self._simulationDatetime
                    },
                    [
                        o
                        for j in self._currentlyRunningJobs.difference(self.currentlyOccupiedJobs)
                        for o in j.operations
                        if (not o.isCompleted) and o.predecessorsFinished and (o.restartAfterDatetime is None or o.restartAfterDatetime <= self._simulationDatetime)
                    ]
                )
            )

        def assignPriorities(resObj, choices):
            """
            Assigns to every entry in a list of possible operations the machine priority for resObj.
            If resObj cannot perform the operation, None is returned as priority.

            Parameters
            ----------
            resObj : resource
            choices : np.array

            Returns
            -------
            l : list
                With list entries being tuples of (resource, listEntry, priority), where listEntry is an element
                from choices and priority is the priority (float) assigned or None, if operation cannot be
                performed
            """
            # first: Filter out impossible operation/resource matchings
            filterFunc = lambda c: len(c['operation'].groups.intersection(resObj.groups)) > 0 if len(c['operation'].groups) > 0 else resObj.canPerformAPOS(c['operation'].APOS)
            choices = filter(filterFunc, choices)

            # create copy of choices list to make sure we can add resource specific attributes here
            # note that we do a shallow copy, so we do not create copies of the referenced objects
            choices = map(lambda c: c.copy(), choices)
            # amend the copy of choices iterable by also storing the setup time requirements
            # dependend on the type of resource, this is either a labor or machine setup time
            setupTimeKey = 'labor' if resObj in self._laborResources else 'machine'

            for c in choices:
                setupRequired = (
                    (resObj not in self._lastResourcesPerJob[c['operation'].job])
                    or
                    (resObj.getLastJob() != c['operation'].job)
                )
                c[setupTimeKey + 'SetupTime'] = resObj.setupTime * setupRequired
                c[setupTimeKey + 'SetupRequired'] = setupRequired

            # Now get priorities and create return list of tuples
            return zip(
                [resObj] * len(choices),
                choices,
                map(resObj.priorityFunc, choices)
            )

        # Step 0: Get lists of
        #   - all possible operations (operations that can be started, from jobs that are not under processing)
        #   - all possible machines (currently not occupied)
        #   - all possible workers (currently not occupied)
        # get list of operations
        availableOperations = getAssignableOperations()
        # get list of machines
        availableMachines = sorted(
            filter(
                lambda m: m.isAvailableAt(self._simulationDatetime),
                self.currentlyIdlingMachineResources
            ),
            key=lambda m: m.allocationPriority,
            reverse=True
        )
        # get list of workers
        availableWorkers = sorted(
            filter(
                lambda m: m.isAvailableAt(self._simulationDatetime),
                self.currentlyIdlingLaborResources
            ),
            key=lambda m: m.allocationPriority,
            reverse=True
        )

        # Step 1: Get the priorities of all machine resources on all entries in l
        # form dict of last resources used on every job
        machinePriorities = itertools.chain(
            *map(
                lambda m: assignPriorities(m, availableOperations),
                availableMachines
            )
        )
        # sort by decreasing priority
        machinePriorities = tuple(sorted(machinePriorities, key=lambda e: e[2], reverse=True))
        #print 'Considered jobs after machine filter: {}'.format(list(e[1]['operation'].job.id for e in machinePriorities))

        # Step 2: While we haven't exhausted our options, make assignments from top of table
        operationResourceQueue = []
        jobsInResourceQueue = set()
        machinesInResourceQueue = set()
        for e in machinePriorities:
            # skip all entries for machines or jobs that are already in the resource queue
            if e[0] in machinesInResourceQueue or e[1]['operation'].job in jobsInResourceQueue:
                continue
            # add entry to machineResource queue
            e[1]['machine'] = e[0]
            operationResourceQueue.append(e[1])
            # add machine and job to filter
            jobsInResourceQueue.add(e[1]['operation'].job)
            machinesInResourceQueue.add(e[0])

        # Step 3: Accordingly, get the priorities of all available workers on all entries in the
        # operationResourceQueue
        workerPriorities = itertools.chain(
            *map(
                lambda m: assignPriorities(m, operationResourceQueue),
                availableWorkers
            )
        )
        # sort by decreasing priority
        workerPriorities = tuple(sorted(workerPriorities, key=lambda e: e[2], reverse=True))

        # Step 4: While we haven't exhausted our options, start new patches based on priorities
        operationResourceQueue = []
        jobsAssigned = set()
        workersAssigned = set()
        for e in workerPriorities:
            # skip all entries where for machines or jobs that are already in the resource queue
            if e[0] in workersAssigned or e[1]['operation'].job in jobsAssigned:
                continue
            # add patch
            newPatch = e[1]['operation'].addPatch(  # add patch to operation
                id=None,
                startDatetime=self._simulationDatetime,
                laborResource=e[0],
                machineResource=e[1]['machine'],  # the machine stored in the queue tuple,
                laborSetupTime=e[1]['laborSetupTime'],
                machineSetupTime=e[1]['machineSetupTime']
            )
            # update the dictionary with most recent resource assignments
            self._lastResourcesPerJob[e[1]['operation'].job] = (e[0], e[1]['machine'])
            # add new patch to the set of active patches
            self._currentPatches.add(newPatch)
            # add callback event after end of patch
            self.registerEvent(
                newPatch.endDatetime,
                self.endPatch,
                patchObj=newPatch
            )
            # add worker and job to filter
            jobsAssigned.add(e[1]['operation'].job)
            workersAssigned.add(e[0])

    jobs = property(
        lambda self: self._jobs,
        lambda self, newValue: False
    )
    """Read-only access on list of jobs considered in the simulation"""
    currentlyOccupiedResources = property(
        lambda self: set(
            itertools.chain(
                *[
                    (p.laborResource, p.machineResource)
                    for p in self._currentPatches
                ]
            )
        ),
        lambda self, newValue: False
    )
    """Read-only access on set of all resources currently occupied by patches"""
    currentlyOccupiedJobs = property(
        lambda self: [p.operation.job for p in self._currentPatches],
        lambda self, newValue: False
    )
    """Read-only access on set of jobs currently under processing"""
    currentlyIdlingLaborResources = property(
        lambda self: set(self._laborResources).difference(self.currentlyOccupiedResources),
        lambda self, newValue: False
    )
    """Read-only access on set of labor resources currenty available to take up new tasks"""
    currentlyIdlingMachineResources = property(
        lambda self: set(  # idle machines are
            self._machineResources  # all machines
        ).difference(  # not
            self.currentlyOccupiedResources  # currently working on patch
        ),
        lambda self, newValue: False
    )
    """Read-only access on set of machine resources currently not working"""


class job(object):
    """
    Class representing one job. A job is a network of operations, connected through precedence
    constraints.
    """
    def __init__(self, id, releaseDatetime, dueDatetime):
        """
        Constructor.

        Parameters
        ----------
        id : Integer
            Identifier (used for cross linking with database)
        releaseDatetime : datetime.datetime
            Timepoint at which job is released.
        dueDatetime : datetime.datetime
            Timepoint at which job is due.
        """
        self._id = id
        self._releaseDatetime = releaseDatetime
        self._dueDatetime = dueDatetime
        self._operations = []
        self._outstandingWorkContent = None
        self._totalWorkContent = 0.0  # total plan time workcontent. As float.
        self._mostRecentPatch = None

    def __str__(self):
        """
        Stringify the resource for printout

        Returns
        -------
        s : string
        """
        return str(self._id)

    def addOperation(self, op):
        """
        Add operation to to job.

        Parameters
        ----------
        op : classes.operation
            Operations Instance to be added
        """
        if op not in self._operations:
            self._operations.append(op)
            op.setJob(self)
            self._outstandingWorkContent = None  # reset outstanding work content calculation
            # add work content to total workcontent measure
            self._totalWorkContent += op.planWorkContent

    def addPrecedenceConstraint(self, fromOp, toOp):
        """
        Add precedence constraint from fromOp to toOp in the sense that fromOp has to be completed
        before work on toOp can be started.

        Parameters
        ----------
        fromOp : classes.operation
        toOp : classes.operation
        """
        # add operations to job if not already in there
        self.addOperation(fromOp)
        self.addOperation(toOp)
        # add constraint
        fromOp.addSuccessor(toOp)
        toOp.addPredecessor(fromOp)

    def addPrecedencePath(self, *ops):
        """
        Add linear precedence constraints between a sequence of operations.
        Precendence constraints are added such that the first operation passed
        has to be finished before the second is started, which has to be finished,
        before the third is started, etc.

        Thus, this function provides a more convenient way to add precedence
        constraints in situations where there is a fixed sequence in which
        operations have to be completed.

        Parameters
        ----------
        ops : Operation instances.
        """
        if len(ops) == 0:
            return
        # add first operations to job if not already in there
        self.addOperation(ops[0])
        for i in range(len(ops)-1):
            # add operations to job if not already in there
            self.addOperation(ops[i+1])
            # add constraint
            self.addPrecedenceConstraint(ops[i], ops[i+1])

    def getOutstandingWorkContent(self):
        """
        Recalculate if necessary and return the outstanding work content.
        """
        if self._outstandingWorkContent is None:
            self._outstandingWorkContent = float(
                np.sum(
                    [
                        op.planWorkContent*(1 - op.fractionCompleted)
                        for op in self._operations
                    ]
                )
            )
        return self._outstandingWorkContent

    def resetOutstandingWorkContent(self):
        """
        Reset stored information about outstanding work-content so it is re-calculated the next time.
        """
        self._outstandingWorkContent = None

    def resetStateTo(self, dialbackDatetime):
        """
        Undo all work (patches) not yet started at dialbackDatetime

        Parameters
        ----------
        dialbackDatetime : datetime
        """
        for o in self._operations:
            if o.endDatetime is not None:
                if o.endDatetime > dialbackDatetime:
                    o.resetActualWorkContent()  # reset actual work content realization from distribution
            o.resetStateTo(dialbackDatetime)
        self._outstandingWorkContent = None
        self._mostRecentPatch

    def addPatch(self, p):
        """
        Inform job about new patch on one of the operations.
        While the job instance does not maintain an interval tree to keep track of all patches,
        we update the mostRecentPatch variable when necessary.
        """
        if self._mostRecentPatch is None or self._mostRecentPatch.endDatetime < p.endDatetime:
            self._mostRecentPatch = p

    def getPatches(self):
        """
        Return intervaltree that is the union of all operation patch intervaltrees

        Returns
        -------
        t : intervaltree.IntervalTree
        """
        t = enhancedIntervalTree()
        # form union over all interval trees
        return reduce(lambda t1, t2: t1 | t2, [o.patches for o in self._operations])

    def getLastResources(self):
        """
        Retrieve the last tuple of resources (machine and labour) that were assigned to work on this
        job (regardless of operation)

        Returns
        -------
        m, l : resource
        """
        if self._mostRecentPatch is None:
            # We compare the mostrecentpatch across all operations
            # first we retrieve those and filter out the none entries,
            filteredMostRecentPatchByOperation = filter(
                lambda x: x is not None,
                [
                    o.getMostRecentPatch()
                    for o in self._operations
                ]
            )
            if len(filteredMostRecentPatchByOperation) > 0:
                # get the one most recent (highest endDatetime)
                self._mostRecentPatch = max(
                    filteredMostRecentPatchByOperation,
                    key=lambda p: p.endDatetime
                )
            # if we still couldn't find a patch, return none
            else:
                return (None, None)
        # if we already had, or just found, a most recent patch, return the patch properties
        return (self._mostRecentPatch.machineResource, self._mostRecentPatch.laborResource)

    def isUnderProcessingAt(self, startDatetime):
        """
        Returns binary indicator if any job operation is under processing at startDatetime.

        Parameters
        ----------
        startDatetime : datetime

        Returns
        -------
        d : boolean
        """
        return max([o.isUnderProcessingAt(startDatetime) for o in self._operations])

    id = property(
        lambda self: self._id,
        lambda self, newValue: False
    )
    """Read-only access on job id"""
    name = property(
        lambda self: self._name,
        lambda self, newValue: False
    )
    """Read-only access on job name"""
    releaseDatetime = property(
        lambda self: self._releaseDatetime,
        lambda self, newValue: False
    )
    """Read-only access on job's release datetime object"""
    dueDatetime = property(
        lambda self: self._dueDatetime,
        lambda self, newValue: False
    )
    """Read-only access on job's release datetime object"""
    operations = property(
        lambda self: self._operations,
        lambda self, newValue: False
    )
    """Read-only access on list of job operations"""
    totalWorkContent = property(
        lambda self: self._totalWorkContent,
        lambda self, newValue: False
    )
    """Read-only access on planned work content over all operations."""
    outstandingWorkContent = property(
        lambda self: self.getOutstandingWorkContent(),
        lambda self, newValue: False
    )
    """Read-only access on the planned work content of all operations not yet 'patched' """
    fractionCompleted = property(
        lambda self: 1.0 - (self.outstandingWorkContent / self.totalWorkContent) if self.totalWorkContent != 0 else 0,
        lambda self, newValue: False
    )
    """Read-only access on the share of work content completed so far."""
    isCompleted = property(
        lambda self: self.fractionCompleted == 1,
        lambda self, newValue: False
    )
    """Read-only access on binary completed flag."""
    startDatetime = property(
        lambda self: min(o.startDatetime for o in self._operations) if self.fractionCompleted > 0 else None,
        lambda self, newValue: False
    )
    """Start date of the job. Read only access"""
    endDatetime = property(
        lambda self: max(o.endDatetime for o in self._operations) if self.isCompleted else None,
        lambda self, newValue: False
    )
    """End of the job. Read only access"""
    lateness = property(
        lambda self: self.endDatetime - self._dueDatetime if self.isCompleted else None,
        lambda self, newValue: False
    )
    """Job lateness. Only avaiable after completion. Returns timedelta object."""
    tardiness = property(
        lambda self: np.max(
            [
                self.lateness,
                timedelta(0, 0, 0)  # 0, expressed as timedelta object
            ]
        ) if self.isCompleted else None,
        lambda self, newValue: False
    )
    """Job tardiness. Only avaiable after completion. Returns timedelta object."""


class operation(object):
    """
    Class representing a single operation.
    """
    def __init__(
        self,
        id,
        APOS,
        planWorkContent,
        groups=[],
        actualWorkContentDistribution=None,
        restartAfterDatetime=None
    ):
        """
        Constructor.

        Parameters
        ----------
        id : int
            ID used for connection with database entries
        APOS : int
            ID used to determine if resources can perform this operation. If None is given, every
            resource is deemed capable of performing operation
        planWorkContent : float
            planned work content time in TU.
        groups: list
            list of integers, represeting groups that can perform this operation.
            If at least one group is given, the operation will only be assigned to resources that
            are in that group. APOS will be ignored.
        actualWorkContentDistribution : callable
            Callable function that expects no parameters and returns a float that is drawn from the
            probability distribution assumed for the actual work content.
        restartAfterDatetime : datetime.datetime
            Datetime object after which new work can be performed on this operation.
            If None is given (default), new work can be performed immediatly
        """
        self._id = id
        self._APOS = APOS
        self._planWorkContent = float(planWorkContent)
        self._actualWorkContent = None
        # if no work content distribution is given, assume deterministic
        if actualWorkContentDistribution is None:
            self._actualWorkContentDistribution = lambda: self._planWorkContent
        else:
            self._actualWorkContentDistribution = actualWorkContentDistribution
        self._restartAfterDatetime = restartAfterDatetime
        self._predecessors = []
        self._successors = []
        self._groups = set(groups)
        self._patches = enhancedIntervalTree()  # store patches as intervaltree to improve information retrieval
        self._job = None
        self._fractionCompleted = 0  # to improve performance, we do not always calculate this from the set of patches
        self._predecessorsFinished = None
        self._mostRecentPatch = None

    def __str__(self):
        """
        Stringify the resource for printout

        Returns
        -------
        s : string
        """
        return str(self._id)

    def setJob(self, job):
        self._job = job

    def addPredecessor(self, op):
        """
        Add operation to the list of predecessors.

        Parameters
        ----------
        op : classes.operation
        """
        if op not in self._predecessors:
            self._predecessors.append(op)

    def getPredecessorsFinished(self):
        """
        (Recalculate) and return binary indicator, if all predecessors are finished

        Returns
        -------
        i : boolean
        """
        if self._predecessorsFinished is None:
            self._predecessorsFinished = min(
                [
                    p.isCompleted
                    for p in self._predecessors
                ]
            ) if len(self._predecessors) > 0 else True
        return self._predecessorsFinished

    def resetPredecessorsFinished(self):
        """
        Reset stored information if all predecessors are finished, so the information is actually
        re-calculated the next time.
        """
        self._predecessorsFinished = None

    def addSuccessor(self, op):
        """
        Add operation to the list of successors.

        Parameters
        ----------
        op : classes.operation
        """
        if op not in self._successors:
            self._successors.append(op)
            op.resetPredecessorsFinished()

    def getActualWorkContent(self):
        """
        Get actual work content as realization of work content function.
        Make sure this realization is only drawn once.

        Returns
        -------
        t : float
            Actual work content in TUs
        """
        if self._actualWorkContent is None:
            self._actualWorkContent = float(self._actualWorkContentDistribution())
        return self._actualWorkContent

    def resetActualWorkContent(self):
        """
        Called at the beginning/between simulation runs to reset the actualWorkContent so it will receive a new
        realization of the random variable when called next time.
        """
        self._actualWorkContent = None

    def addPatch(self, **patchProperties):
        """
        Adds a patch to this operation. Patch object is returned.

        Parameters
        ----------
        kwargs : dict
            Tuple of named parameters which are forwarded to the :class:`patch` constructor.

        Returns
        -------
        p : patch
            Patch object created
        """
        p = patch(op=self, **patchProperties)

        # get most recent patch until now
        m = self.getMostRecentPatch()
        # check if the new patch is more recent
        if m is None or m.endDatetime < p.endDatetime:
            self._mostRecentPatch = p
        # add patch to patch tree, storing the patch object as additional information

        self._patches.add(Interval(p.startDatetime, p.endDatetime, data=p))
        # store patch with both resources and the job object
        p.machineResource.addPatch(p)
        p.laborResource.addPatch(p)
        # recalculate fractionCompleted. Avoid rounding issues.
        self._fractionCompleted += float(p.workContentCovered) / self._planWorkContent
        if np.abs(1 - self._fractionCompleted) < 10**-5:  # much faster then numpy.allclose()
            self._fractionCompleted = 1
            # now that the operation is finished, inform successors
            [s.resetPredecessorsFinished() for s in self._successors]
        # inform job to re-calculate outstanding workcontent
        self.job.resetOutstandingWorkContent()
        return p

    def getMostRecentPatch(self):
        """
        Get the most recent patch (the one with the higest endDatetime)

        Returns
        -------
        p : patch
        """
        if self._mostRecentPatch is None:  # most recent patch currently not stored, retrieve it
            if len(self._patches) > 0:
                self._mostRecentPatch = self._patches.endElement().data
            # if no patches are stored, _mostRecentPatch stays at None
        return self._mostRecentPatch

    def resetStateTo(self, dialbackDatetime):
        """
        Undo all work (patches) not yet started at dialbackDatetime

        Parameters
        ----------
        dialbackDatetime : datetime
        """
        # remove patches that were not started at dialbackDatetime
        if len(self._patches) > 0:
            self._patches.remove_envelop(dialbackDatetime, self._patches.end())
        # update share of completed work
        self._fractionCompleted = float(
            np.sum([
                p[2].workContentCovered
                for p in self._patches
            ])
        ) / self._planWorkContent
        # Reset flag about completed predecessors.
        [s.resetPredecessorsFinished() for s in self._successors]
        # reset most recent patch
        self._mostRecentPatch = None

    def isUnderProcessingAt(self, startDatetime):
        """
        Returns binary indicator if the operation is under processing at startDatetime.

        Parameters
        ----------
        startDatetime : datetime

        Returns
        -------
        d : boolean
        """
        return len(self._patchIntervals[startDatetime]) > 0

    id = property(
        lambda self: self._id,
        lambda self, newValue: False
    )
    """Read-only access on operation ID"""
    APOS = property(
        lambda self: self._APOS,
        lambda self, newValue: False
    )
    """Read-only access on operation APOS"""
    groups = property(
        lambda self: self._groups,
        lambda self, newValue: False
    )
    """read-only access on set of groups that can perform this operation"""
    patches = property(
        lambda self: self._patches,
        lambda self, newValue: False
    )
    """read-only access on intervaltree holding patches"""
    job = property(
        lambda self: self._job,
        lambda self, newValue: False
    )
    """Read-only access on job instance to which the operation belongs"""
    actualWorkContent = property(
        lambda self: self.getActualWorkContent(),
        lambda self, newValue: False
    )
    """Read-only on actual work content (in TUs)"""
    planWorkContent = property(
        lambda self: self._planWorkContent,
        lambda self, newValue: False
    )
    """Read-only access on plan work content (in TUs)"""
    predecessors = property(
        lambda self: self._predecessors,
        lambda self, newValue: self.addPredecessor(newValue)
    )
    """Read-only access on list of predecessor operations in the job"""
    successors = property(
        lambda self: self._successors,
        lambda self, newValue: self.addSuccessor(newValue)
    )
    """Read-only access on list of successor operations in the job"""
    predecessorsFinished = property(
        lambda self: self.getPredecessorsFinished(),
        lambda self, newValue: False
    )
    """Read-only access on flag, if all predecessors are finished."""
    hasStarted = property(lambda self: len(self.patches) == 0, lambda self, newValue: False)
    """Read-only access on binary indicator, if the processing of this operation has started so far"""
    fractionCompleted = property(
        lambda self: self._fractionCompleted,
        lambda self, newValue: False
    )
    """Read-only access on the share of work content completed so far."""
    isCompleted = property(lambda self: self.fractionCompleted == 1, lambda self, newValue: False)
    """Read-only access on binary completed flag."""
    startDatetime = property(
        lambda self: self._patches.begin() if len(self._patches) > 0 else None,
        lambda self, newValue: False
    )
    """Read-only access on start time of operation"""
    endDatetime = property(
        lambda self: self._patches.end() if self.isCompleted > 0 else None,
        lambda self, newValue: False
    )
    """Endtime of operation. Equal to endtime of last patch. Only available if operation is completed"""
    throughputTime = property(
        lambda self: self.endDatetime - self.startDatetime if self.isCompleted else None,
        lambda self, newValue: None
    )
    """
    Timedifference between start and end of first and last patch respectively.
    Only available if operation is completed
    """
    restartAfterDatetime = property(
        lambda self: self._restartAfterDatetime,
        lambda self, newValue: False
    )
    """Read-only access on restart time (may be None)"""


class patch(object):
    """
    Patches represent "rectangles in the Gantt Chart", parts of operations that are completed in one
    piece by one worker and machine. Endtime and covered workcontent can be passed as parameters or
    calculated automatically.
    """
    def __init__(
        self,
        id,
        op,
        startDatetime,
        laborResource,
        machineResource,
        laborSetupTime=0,
        machineSetupTime=0,
        endDatetime=None,
        workContentCovered=None
    ):
        """
        Constructor.

        Parameters
        ----------
        id : int
            identifier (optional, used to connect simulation model entitiy to database entry)
        op : classes.operation
            Operation to which patch belongs
        startDatetime : datetime
            Start point of patch
        laborResource, machineResource : resource
        laborSetupTime, machineSetupTime : float
            Setup times in hours to set up the labor and machine resources for the new job respectively
        endDatetime : datetime
            Optional endtime of the activity. If None is given (default), endtime is calculated
            automatically.
        planWorkContent : float
            Optional value, identifiying the amount of work content (in TU) that was completed
            in this patch. Value is only interpreted if endDatetime was given, otherwise it is
            calculated automatically. If endDatetime is given, but planWorkContent=None, covered
            workcontent is estimated.
        """
        self._id = id
        self._operation = op
        self._startDatetime = startDatetime
        self._laborResource = laborResource
        self._machineResource = machineResource
        self._workContentCovered = None  # default value, overwritten later in this constructor.
        self._laborSetupTime = laborSetupTime
        self._machineSetupTime = machineSetupTime
        # if not endDatetime is given, we have to calculate it
        if endDatetime is None:
            # calculate end of patch
            remainingWorkContent = self._operation.actualWorkContent*(1-self._operation.fractionCompleted)
            expectedLaborProcessingTime = remainingWorkContent / 100 / self._laborResource.productivity
            processingFinishedAt = self._startDatetime + timedelta(hours=expectedLaborProcessingTime) + timedelta(hours=self._laborSetupTime + self._machineSetupTime)
            laborUnavailableAt = self._laborResource.becomesUnavailableNextAfter(self._startDatetime)
            machineUnavailableAt = self._machineResource.becomesUnavailableNextAfter(self._startDatetime)
            self._endDatetime = min(
					processingFinishedAt,
					laborUnavailableAt,
					machineUnavailableAt
				)
            # store information
            self._operationFinishedAfterPatch = (self._endDatetime == processingFinishedAt)
        else:
            self._endDatetime = endDatetime
            if workContentCovered is not None:
                self._workContentCovered = float(min(workContentCovered, self.operation.planWorkContent))
        if self._workContentCovered is None:  # covered plantime not set, we have to estimate it.
            # calculate share of work content completed in this time
            duration = self._endDatetime - self._startDatetime
            # convert to float (indicating number of hours)
            duration = float(duration.days)*24 + float(duration.seconds) / 3600 + float(duration.microseconds) / (3600*10**6)
            # subtract setup times
            noSetupDuration = max(
                duration - self._laborSetupTime - self._machineSetupTime,
                0
            )
            # convert duration into estimated covered work content, taking labor productivity factor into account
            coveredWorkContent = noSetupDuration * 100 * self._laborResource.productivity
            # calculate remaining processing time acc. to plan
            remainingWorkContent = self._operation.planWorkContent*(1-self._operation.fractionCompleted)
            # in calculating the remaining work content, make sure we are not left with rounding issues
            self._workContentCovered = min(
                coveredWorkContent,
                remainingWorkContent
            )

    def __str__(self):
        """
        Stringify the resource for printout.

        Returns
        -------
        s : string
        """
        return str(self._id)

    id = property(
        lambda self: self._id,
        lambda self, newValue: False
    )
    """Read-only access to patch id"""
    operation = property(
        lambda self: self._operation,
        lambda self, newValue: False
    )
    """Read-only access on operation instance to which this patch belongs"""
    job = property(
        lambda self: self._operation.job,
        lambda self, newValue: False
    )
    """Read-only access on the job object to which this operation belongs"""
    workContentCovered = property(
        lambda self: self._workContentCovered,
        lambda self, newValue: False
    )
    """Read-only access on share of planned work hours covered by this patch"""
    startDatetime = property(
        lambda self: self._startDatetime,
        lambda self, newValue: False
    )
    """Read-only access on start point of this patch"""
    endDatetime = property(
        lambda self: self._endDatetime,
        lambda self, newValue: False
    )
    """Read-only access on end point of this patch"""
    laborResource = property(
        lambda self: self._laborResource,
        lambda self, newValue: False
    )
    """Read-only access on labor resource associated with this patch"""
    machineResource = property(
        lambda self: self._machineResource,
        lambda self, newValue: False
    )
    """Read-only access on machine resource associated with this patch"""
    laborSetupTime = property(
        lambda self: self._laborSetupTime,
        lambda self, newValue: False
    )
    """Read-only access on worker setup time considered in this patch"""
    machineSetupTime = property(
        lambda self: self._machineSetupTime,
        lambda self, newValue: False
    )
    """Read-only access on machine setup time considered in this patch"""
    operationFinishedAfterPatch = property(
        lambda self: self._operationFinishedAfterPatch,
        lambda self, newValue: False
    )
    """Read-only access to binary indicator, if this patch ended because the operation was finished"""


class resource(scheduledResource):
    """
    Baseclass for all resources (both labor and machines) in the shop.
    """
    def __init__(
        self,
        id,
        productivity=1.0,
        setupTime=0,
        allocationPriority=0,
        priorityFunc=lambda x: 1,
        startDate=date.today(),
        endDate=date.today().replace(year=date.today().year+1),
        APOSCapabilities=['.*'],
        groups=[]
    ):
        """
        Constructor. Parameter determine the behavior of the resource in terms of scheduling decisions.

        Parameters
        ----------
        id : Integer
            Identifier (used for cross-linking with database entries)
        productivity : float
            Relative productivity. For a given worker, the expected time to complete a task is calculated
            as plantime divided by productivity factor. For machine resources, a productivity factor of
            1 is globally assumed (passed value is ignored). Default: 1.0
        setupTime : float
            Time (in hours) that is required for setup if we switch between jobs. Default: 0
        allocationPriority : float
            Priority with which a resource is allocated. Resources with higher allocation priority are
            allocated first, potentially leaving idle resources with lower allocation priority.
        priorityFunc : callable
            Priority function applied by this worker. Assumes a callable function that accepts a
            named tuple and returns a priority value (float). Whenever applicable, the list of waiting
            operations will be assessed with this function and the operation with the highest priority
            will be started next. Default: 1
        startDate : datetime.date
            First day of the calendar. Default: Today
        endDate : datetime.date
            Last day of the calendar. Default: Today + 1 year
        APOSCapabilities : iterable
            List of strings used for RE matching against the APOS number of an operation.
            One of the strings should match any operation that can be performed on this resource.
            Start and end of string symbols ('^' and '$') are added automatically. Default: '.*'
            => All APOS can be performed
        groups: list
            list of integers, represeting groups that can perform this operation.
            If an operation is given to at least one group, only resources in that group will be
            assigned to this operation.
        """
        scheduledResource.__init__(self, startDate, endDate)
        self.priorityFunc = priorityFunc
        self._id = id
        self._productivity = productivity
        self._setupTime = setupTime
        self._allocationPriority = allocationPriority
        # store all patches that were handled on this resource
        self._patches = []
        self._mostRecentPatch = None
        self._APOSCapabilities = APOSCapabilities
        # transform list of strings into list of RE objects for faster pattern matching
        self._APOSCapabilitiesREObjects = map(
            lambda s: re.compile('^' + s + '$'),
            self._APOSCapabilities
        )
        self._groups = set(groups)

    def canPerformAPOS(self, apos):
        """
        Check if resource can perform a given APOS number.

        Parameters
        ----------
        apos : int
            APOS Number

        Returns
        -------
        d : boolean
            Decision (True or False)
        """
        if apos is None:  # if no APOS is specified, assume the resource can handle it
            return True
        apos = str(apos)
        return any(  # one match is sufficient
            r is not None  # match means, RE object and not None is returned by match() function
            for r in map(  # execute match() on all RE objects
                lambda e: e.match(apos),
                self._APOSCapabilitiesREObjects
            )
        )

    def addPatch(self, p):
        """
        Add patch to the set of patches handled on this resource

        Parameters
        ----------
        p : patch
        """
        assert self in [p.machineResource, p.laborResource], 'Resource not involved in patch'
        # get most recent patch until now
        m = self.getMostRecentPatch()
        # check if the new patch is more recent
        if m is None or m.endDatetime < p.endDatetime:
            self._mostRecentPatch = p
        # store information about patch
        self._patches.append(p)

    def resetStateTo(self, dialbackDatetime):
        """
        Undo all work (patches) not yet started at dialbackDatetime

        Parameters
        ----------
        dialbackDatetime : datetime
        """
        # remove patches that were not started at dialbackDatetime
        self._patches = filter(
            lambda p: p.startDatetime < dialbackDatetime,
            self._patches
        )
        # reset most recent patch
        self._mostRecentPatch = None

    def getMostRecentPatch(self):
        """
        Get the most recent patch (the one with the higest endDatetime)

        Returns
        -------
        p : patch
        """
        if self._mostRecentPatch is None:  # most recent patch currently not stored, retrieve it
            if len(self._patches) > 0:
                self._mostRecentPatch = max(self._patches, key=lambda p: p.endDatetime)
            # if no patches are stored, _mostRecentPatch stays at None
        return self._mostRecentPatch

    def getLastJob(self):
        """
        Get the job object associated with the most recent patch, i.e. the job this resource was most
        recently involved with

        Returns
        -------
        j : job
        """
        p = self.getMostRecentPatch()
        return None if p is None else p.operation.job

    def getCapacityProfile(self, startDatetime, endDatetime, timeDelta):
        """
        Get a load profile for the given resource that indicates for every time interval the used capacity
        and available capacity in TUs.
        The result is returned for every time interval of length timeDelta from startDatetime until endDatetime

        Parameters
        ----------
        startDatetime, endDatetime : datetime.datetime
            start and endpoint of the analysis respectively
        timeDelta : datetime.timedelta
            Interval length

        Returns
        -------
        timeBins : tuple
            tuple of dictionaries carrying the relevant information for every time interval
        """
        numberOfBins = np.ceil((endDatetime - startDatetime).total_seconds() / timeDelta.total_seconds())
        # create time bins as tuple of dictionaries
        timeBins = tuple(
            {
                'startDatetime': startDatetime + i * timeDelta,
                'endDatetime': startDatetime + (i+1) * timeDelta,
                'coveredHours': 0.0,
                'coveredWorkContent': 0.0
            }
            for i in range(numberOfBins)
        )
        # total available working hours to each dictionary
        map(
            lambda d: d.update({
                'availableHours': self.workingHoursBetween(
                    d['startDatetime'],
                    d['endDatetime']
                )
            }),
            timeBins
        )
        # iterate through all patches for this resource and distribute the TUs onto the time bins
        for p in self._patches:
            patchDurationInSeconds = (p.endDatetime - p.startDatetime).total_seconds()
            start = end = p.startDatetime
            while end < p.endDatetime:
                # calculate first time bin into which this patch belongs
                i = (start - startDatetime).total_seconds() // timeDelta.total_seconds()
                # calculate end
                end = min(
                    p.endDatetime,  # end of patch
                    timeBins[i]['endDatetime']  # end of current time bin
                )
                # Calculate time this patch covers in this time bin
                timeBins[i]['coveredHours'] += float((end - start).total_seconds()) / 3600
                # calculate proportional amount of TUs from current patch that belong to this time bin
                timeBins[i]['coveredWorkContent'] += (end - start).total_seconds() / parchDurationInSeconds * p.workContentCovered

                # update start mark for next iteration
                start = end

        # calculate available work content in for every time bin. Taking into account resource
        # productivity
        map(
            lambda d: d.update(
                {
                    'availableWorkContent': (d['availabeHours'] - d['coveredHours']) * 100 * self.productivity
                }
            ),
            timeBins
        )

        return timeBins

    def __str__(self):
        """
        Stringify the resource for printout

        Returns
        -------
        s : string
        """
        return str(self._id)

    id = property(
        lambda self: self._id,
        lambda self, newValue: False
    )
    """Read-only access on resource id"""
    name = property(
        lambda self: self._name,
        lambda self, newValue: False
    )
    """Read-only access to resource name"""
    productivity = property(
        lambda self: self._productivity,
        lambda self, newValue: False
    )
    """Resource productivity as read-only property"""
    setupTime = property(
        lambda self: self._setupTime,
        lambda self, newValue: False
    )
    """Resource setup time (in hours) as read-only property"""
    allocationPriority = property(
        lambda self: self._allocationPriority,
        lambda self, newValue: False
    )
    """Read-only access on the allocation priority of the resource"""
    groups = property(
        lambda self: self._groups,
        lambda self, newValue: False
    )
    """read-only access on set of groups, this resource belongs to"""
