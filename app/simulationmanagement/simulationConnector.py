#!/usr/bin/env python
import Pyro4
import Pyro4.errors


def findProvider():
    print "Finding simulation servers."
    providers = []
    with Pyro4.locateNS() as ns:
        for providerName, provider_uri in ns.list(prefix="provider.").items():
            print "found simulation provider: {}".format(providerName)
            provider = Pyro4.Proxy(provider_uri)
            try:
                provider._pyroBind()
            except Pyro4.errors.CommunicationError, Pyro4.errors.ConnectionClosedError:
                print "Simulation server could not be reached. Cleaning up nameserver."
                ns.remove(providerName)
                continue
            except Exception as e:
                print 'Unexpected error'
                continue
            # if everything went well, add provider to list of providers
            providers.append(provider)
    return providers

# Define global list of providers.
global provider
provider = findProvider()


def getProvider():
    global provider
    if provider is None:
        provider = findProvider()
    return provider


def startSimulation():
    global provider
    [p.startSimulation() for p in provider]


def updateAPOS(filePath):
    global provider
    providerFound = False
    for p in provider:
        if not p.busy:
            providerFound = True
            p.updateAPOS(filePath)
            return True
    if not providerFound:
        return False


def stop(providerName=None):
    """
    Stop simulation provider. If name is specified, stop only this provider, if providerName is None (default)
    stop all providers.

    Parameters
    ----------
    providerName : str
    """
    global provider
    for p in provider:
        if p is None or p.name == providerName:
            p.stop()


def shutdownProvider(providerName):
    global provider
    for p in provider:
        if p.name == providerName:
            p.shutdown()
            p._pyroRelease()
    provider = None


def refreshProviderList():
    global provider
    provider = findProvider()
