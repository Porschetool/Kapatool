import classes
from datetime import datetime, date, timedelta
import itertools
import priorityRules

from app.db.models import *


def getShopCalendarSimObject(startDate, endDate):
    """
    Load the shop calendar into a simulation scheduledResource object and return it.

    Parameters
    ----------
    startDate, endDate : datetime.date
        Time period for which availabilities should be loaded into the calendar

    Returns
    -------
    c : scheduledResource
        returns None, if no shop calendar is set
    """
    queryResult = Calendar.query.filter(
        Calendar.shopCalendar
    ).all()
    if len(queryResult) > 0:
        return createCalendarFromDatabaseObject(
            calDbObj=queryResult[0],
            startDate=startDate,
            endDate=endDate
        )
    else:
        return None


def createCalendarFromDatabaseObject(calDbObj, startDate, endDate):
    """
    Convert Calendar SQLAlchemy Object into calendar simulation object

    Parameters
    ----------
    calDbObj : Calender
    startDate, endDate : datetime.date
        Time period for which availabilities should be loaded into the calendar

    Returns
    -------
    cal : scheduledResource
    """
    cal = classes.scheduledResource(
        startDate,
        endDate
    )
    createAvailabilitiesBasedOnDatabaseObject(calDbObj, cal)
    return cal


def createAvailabilitiesBasedOnDatabaseObject(calDbObj, calSimObj):
    """
    Add availabilities based on a Calendar database object (calDbObj) to a scheduled resource simulation instance
    (calSimObj)

    Parameters
    ----------
    calDbObj : Calendar
    calSimObj : scheduledResource

    Returns
    -------
    calSimObj : scheduledResource
    """
    # Read out regular availabilities
    regAvailabilities = {i: [] for i in range(7)}
    for e in calDbObj.repeatingEntries:
        regAvailabilities[e.dayOfWeek].append((e.startTime, e.endTime))
    # load regular avilabilities into shop calendar
    calSimObj.addDailyAvailability(
        lambda day: regAvailabilities[day.weekday()] if len(regAvailabilities[day.weekday()]) > 0 else [(None, None)]
    )
    return calSimObj


def createLaborResourcesFromDatabaseObject(resDbObj, startDate, endDate):
    """
    Convert User SQLAlchemy User Object into resource simulation object.
    Availabilities are created between startDate and endDate

    Parameters
    ----------
    resDbObj : User
    startDate, endDate : datetime.date

    Returns
    -------
    user : scheduledResource
    """
    user = classes.resource(
        id=resDbObj.id,
        productivity=resDbObj.productivity,
        startDate=startDate,
        endDate=endDate,
        APOSCapabilities=[] if len(resDbObj.groups) == 0 else itertools.chain.from_iterable(
            [
                g.APOSRegEx.splitlines() if g.APOSRegEx is not None else []
                for g in resDbObj.groups
            ]
        ),
        groups=[g.id for g in resDbObj.groups],
        priorityFunc=priorityRules.EDDPriorityRule,
        allocationPriority=resDbObj.allocationPriority,
        setupTime=float(resDbObj.setupTime)/60  # convert setup-time to hours
    )
    createAvailabilitiesBasedOnDatabaseObject(resDbObj.calendar, user)
    return user


def createMachineResourcesFromDatabaseObject(resDbObj, startDate, endDate):
    """
    Convert Machine SQLAlchemy Object into resource simulation object

    Parameters
    ----------
    resDbObj : Machine
    startDate, endDate : datetime.date

    Returns
    -------
    machine : scheduledResource
    """
    machine = classes.resource(
        id=resDbObj.id,
        startDate=startDate,
        endDate=endDate,
        APOSCapabilities=resDbObj.APOSRegEx.splitlines(),
        groups=[g.id for g in Group.query.all()],  # resources are part of all groups, so assignment is possible for all operations
        priorityFunc=priorityRules.EDDPriorityRule,
        setupTime=float(resDbObj.setupTime)/60  # convert setup-time to hours
    )
    # machines are assumed to be constantly available. Not subject to schedules
    machine.addConstantAvailability()
    return machine


def createJobFromDatabaseObject(jobDbObj, calSimObj=None, laborResources={}, machineResources={}):
    """
    Convert Calendar SQLAlchemy Object into calendar simulation object

    Parameters
    ----------
    jobDbObj : Job Object
    calSimObj : scheduledResource
        Shop calendar
    laborResources, machineResources : dict
        Dictionaries of resource simulation objects, keyed by their respecitve ids

    Returns
    -------
    j : Job (simulation instance)
    """
    # we need some start or end date to include job in simulation. Startdate could be the release date
    # or the end of the drop off appointment
    # if no sufficient information is given, we cannot handle this job
    if (jobDbObj.dropOffAppointment is None and jobDbObj.releaseDatetime is None) and jobDbObj.pickUpAppointment is None:
        return None
    # if job has no operations, return None
    if len(jobDbObj.operations) == 0:
        return None
    # calculate release date if any hints are given
    releaseDatetime = None
    if jobDbObj.releaseDatetime is not None:  # job already released
        releaseDatetime = jobDbObj.releaseDatetime
    elif jobDbObj.dropOffAppointment is not None:  # drop off appointment set
        releaseDatetime = jobDbObj.dropOffAppointment.endDatetime
    # calculate finishing time if any hints are given
    finishingDatetime = None
    if jobDbObj.pickUpAppointment is not None:
        finishingDatetime = jobDbObj.pickUpAppointment.startDatetime

    # if any of the two dates are missing, calculate them manually
    if releaseDatetime is None:
        releaseDatetime = calculateRecommendedReleaseDatetime(jobDbObj, calSimObj)
    elif finishingDatetime is None:  # finishingDatetime is None
        finishingDatetime = calculateRecommendedEndDatetime(jobDbObj, calSimObj)

    # now discount the post processing from the finishing datetime to get the time, processing in the
    # shop must be finished
    dueDatetime = finishingDatetime - timedelta(hours=float(jobDbObj.postProcessingWorkContent)/100)

    # create overall job object
    j = classes.job(
        id=jobDbObj.id,
        releaseDatetime=releaseDatetime,
        dueDatetime=dueDatetime
    )
    # add operations to job
    operations = ()
    for i, opDbObj in enumerate(jobDbObj.operations):
        op = classes.operation(
            id=opDbObj.id,
            APOS=None if opDbObj.planTime is None else opDbObj.planTime.laborOperation.operationId,
            planWorkContent=opDbObj.workContent,
            restartAfterDatetime=opDbObj.restartAfterDatetime,
            groups=[g.id for g in opDbObj.groups]
        )
        j.addOperation(op)  # add operation to job
        operations += (op,)
        # go through operation database objects translated so far and create precedence constraints,
        # where the position is lower
        for k in range(i):
            if jobDbObj.operations[k].position < opDbObj.position:
                j.addPrecedencePath(operations[k], operations[i])

        # add patches to operation. In the simulation model, we do not differentiate between planned and actual
        # start and endtimes
        for patchDbObj in opDbObj.patches:
            # do not transfer patches into the simulation that are purely planned (not started yet)
            if patchDbObj.startDatetime is None:
                continue
            op.addPatch(
                id=patchDbObj.id,
                startDatetime=patchDbObj.startDatetime,  # cannot be None (filtered accordingly)
                endDatetime=patchDbObj.endDatetime,  # might be None (Patch is ongoing)
                workContentCovered=patchDbObj.workContentCovered,
                laborResource=laborResources[patchDbObj.user.id],
                machineResource=machineResources[patchDbObj.machine.id]
            )

    # add operations to job including the precedence relationships
    # operations were automatically returned (and hence handled) ordered by position
    j.addPrecedencePath(*operations)
    return j


def calculateRecommendedReleaseDatetime(jobDbObj, calSimObj=None):
    """
    Calculate the recommended release of a job, assuming that the pick up appointment is set.

    Parameters
    ----------
    jobDbObj : Job
        database job instance
    calSimObj : classes.scheduledResource
        simulation scheduled resource instance. If none is given, the shop calendar is loaded automatically

    Returns
    -------
    d : datetime.datetime
        Returns None if neither pick-up nor drop-off appointment (or both) have been set
    """
    # check we have exactly one appointment set
    if jobDbObj.pickUpAppointment is None:
        return None

    referenceDate = jobDbObj.pickUpAppointment.startDatetime
    forward = False  # search backward
    # get shop calendar if not present already
    calSimObj = calSimObj if calSimObj is not None else getShopCalendarSimObject(
        referenceDate.date() - timedelta(weeks=4),
        referenceDate.date() + timedelta(days=1)
    )
    if calSimObj is None:
        return None
    # calculate expected duration in hours, applying the flow factor only to the duration of the work content
    # of operations, not to post processing
    duration = Setting.query.get("flowFactor").formattedValue * float(jobDbObj.totalWorkContent)/100 + float(jobDbObj.postProcessingWorkContent)/100

    # subtract duration from reference date and return
    return calSimObj.addWorkingHoursTo(referenceDate, -duration)


def calculateRecommendedEndDatetime(jobDbObj, calSimObj=None):
    """
    Calculate the recommended release or end of a job, assuming that either the pick up appointment or
    the drop-off appointment have been set. In that case, the other date is calculated based on the assumed
    shop flow rate.

    Parameters
    ----------
    jobDbObj : Job
        database job instance
    calSimObj : classes.scheduledResource
        simulation scheduled resource instance. If none is given, the shop calendar is loaded automatically

    Returns
    -------
    d : datetime.datetime
        Returns None if neither pick-up nor drop-off appointment (or both) have been set
    """
    # check we have exactly one appointment set
    if jobDbObj.dropOffAppointment is None and jobDbObj.releaseDatetime is None:
        return None
    # find out which date we can use
    referenceDate = jobDbObj.dropOffAppointment.endDatetime if jobDbObj.dropOffAppointment is not None else jobDbObj.releaseDatetime
    # get shop calendar if not present already
    calSimObj = calSimObj if calSimObj is not None else getShopCalendarSimObject(
        referenceDate.date() - timedelta(days=1),
        referenceDate.date() + timedelta(weeks=4)
    )
    if calSimObj is None:
        return None
    # calculate expected duration in hours, applying the flow factor only to the duration of the work content
    # of operations, not to post processing
    duration = Setting.query.get("flowFactor").formattedValue * float(jobDbObj.totalWorkContent)/100 + float(jobDbObj.postProcessingWorkContent)/100

    # subtract duration from reference date and return
    return calSimObj.addWorkingHoursTo(referenceDate, duration)
