from datetime import datetime


def EDDPriorityRule(entry):
    """
    Earliest Due Date (EDD) rule gives highest priority to the job with the earliest due date.
    Function automatically substracts priority in case, setup is required, giving precedence
    to resource assignments that do not require setup.

    Paramters
    ---------
    entry : dict

    Returns
    -------
    priority : float
    """
    return (
        datetime(2200, 1, 1) - entry['jobDueDatetime']
    ).total_seconds() - (
        # subtract 1 second in "priority" if...
        # labor setup is required (key only available for labor resources) or
        # or machine setup is required (otherwise)
        entry['laborSetupRequired'] if 'laborSetupRequired' in entry.keys() else entry['machineSetupRequired']
    )


def FIFOPriorityRule(entry):
    """
    First In First Out (FIFO) rule gives highest priority to the job longest in the system

    Paramters
    ---------
    entry : dict

    Returns
    -------
    priority : float
    """
    return (datetime(2200, 1, 1) - entry['jobReleaseDatetime']).total_seconds() - (
        # subtract 1 second in "priority" if...
        # labor setup is required (key only available for labor resources) or
        # or machine setup is required (otherwise)
        entry['laborSetupRequired'] if 'laborSetupRequired' in entry.keys() else entry['machineSetupRequired']
    )


def LeastSlackPriorityRule(entry):
    """
    least slack rule gives highest priority to the entry with the smallest slack (largest value of -slack)

    Paramters
    ---------
    entry : dict

    Returns
    -------
    priority : float
    """
    return entry['jobOutstandingWorkContent'] - shopCal.workingHoursBetween(
        entry['simulationDatetime'],
        entry['jobDueDatetime']
    )
