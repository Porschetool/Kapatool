from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from flask_login import login_required, login_user, logout_user, current_user

from . import simulationmanagement
import simulationConnector
import sys
import subprocess
import os


@simulationmanagement.route('/')
@simulationmanagement.route('/index')
def index():
    provider = simulationConnector.getProvider()
    try:
        d = [
            {
                'name': p.name,
                'status': p.status
            }
            for p in provider
        ]
        return render_template('pages/provider.index.html', provider=d)
    except:
        return redirect(url_for('simulationmanagement.refresh'))


@simulationmanagement.route('/start')
def start():
    simulationConnector.startSimulation()
    return redirect(url_for('simulationmanagement.index'))


@simulationmanagement.route('/stop', defaults={'providerName': None})
@simulationmanagement.route('/stop/<providerName>')
def stop(providerName):
    if current_user.role not in ['Manager']:
        flash('You are not allowed to perform this operation.', 'danger')
        return redirect(url_for('index'))
    simulationConnector.stop(providerName)
    return redirect(url_for('simulationmanagement.index'))


@simulationmanagement.route('/shutdown/<providerName>')
def shutdown(providerName):
    if current_user.role not in ['Manager']:
        flash('You are not allowed to perform this operation.', 'danger')
        return redirect(url_for('index'))
    simulationConnector.shutdownProvider(providerName)
    return redirect(url_for('simulationmanagement.index'))


@simulationmanagement.route('/refresh')
def refresh():
    simulationConnector.refreshProviderList()
    return redirect(url_for('simulationmanagement.index'))


@simulationmanagement.route('/spawn')
def spawn():
    """
    Spawn new simulation provider.
    """
    subprocess.Popen([sys.executable, os.path.dirname(__file__) + '/../../simulationProvider.py'])
    flash('New simulation provider started.', 'success')
    return redirect(url_for('simulationmanagement.refresh'))


