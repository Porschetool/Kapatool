#!/usr/bin/env python
from flask_wtf import FlaskForm
from wtforms import FieldList, FormField, StringField, PasswordField, BooleanField, DecimalField, TextAreaField, SelectField, IntegerField, DateField
from wtforms.validators import DataRequired, EqualTo, Length, NumberRange, Optional

from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField, QuerySelectField
from flask_admin.form.widgets import Select2Widget

from app.db.models import User, Group, Calendar, Setting


class CalendarSubscriptionForm(FlaskForm):
    calendar = QuerySelectField(
        query_factory=lambda: Calendar.query.filter(Calendar.finalized).all(),
        get_label=lambda g: g.name
    )
    startDate = DateField(
        'Starting from',
        format=Setting.query.get('defaultDateFormat').formattedValue
    )
    endDate = DateField(
        'Until (and including)',
        validators=[Optional()],
        description='Leave empty for this subscription to run indefinetly (can be ended later).',
        format=Setting.query.get('defaultDateFormat').formattedValue
    )


class UserAddForm(FlaskForm):
    name = StringField(
        'Username',
        validators=[DataRequired()],
        description='Used both for login and to identify the user across the system.'
    )
    password = PasswordField(
        'Password', validators=[DataRequired()]
    )
    confirm = PasswordField(
        'Repeat Password',
        [
            DataRequired(),
            EqualTo('password', message='Passwords must match')
        ]
    )
    role = SelectField(
        'User Role',
        choices=[
            ('', '---None Selected---'),
            ('ServiceAdvisor', 'Service Advisor'),
            ('ServiceAssistant', 'Service Assistant'),
            ('Technician', 'Technician'),
            ('Manager', 'Workshop Management')
        ],
        description='Role determines the rights given to the user how it will be scheduled (if at all).'
    )
    productivity = DecimalField(
        'Productivity Factor',
        validators=[DataRequired()],
        default=1.0,
        description="Productivity determines how much time the technician is expected to require to complete a certain work. To complete 100TU will take 1 hour at productivity 1.0, 30 minutes at productivity 2.0, and 2 hours at productivity 0.5"
    )
    setupTime = IntegerField('Setup Time [min]', default=0)
    qualityInspector = BooleanField(
        'User is Quality Inspector',
        description="Users flagged as quality inspectors will be allowed to confirm a car's quality"
    )
    admin = BooleanField(
        'User has Admin Rights'
    )
    shopCalendar = BooleanField(
        'User represents shop calendar',
        description="This option can only be checked for one user. The availabilities of this user will be considered as the shop's opening times."
    )
    groups = QuerySelectMultipleField(
        query_factory=lambda: Group.query.all(),
        get_label=lambda g: g.name
    )
    allocationPriority = IntegerField(
        'Allocation Priority for Technicians',
        default=0,
        description='Determines, in which order technicians are allocated. Technicians with higher allocation priority are assigned to operations during planning first. Technicians with lower priority are more likely to idle.'
    )


class UserEditForm(FlaskForm):
    name = StringField(
        'Username',
        validators=[DataRequired()],
        description='Used both for login and to identify the user across the system.'
    )
    oldPassword = PasswordField(
        'Old Password', validators=[]
    )
    newPassword = PasswordField(
        'New Password', validators=[]
    )
    confirm = PasswordField(
        'Repeat Password',
        validators=[
            EqualTo('newPassword', message='Passwords must match')
        ]
    )
    role = SelectField(
        'User Role',
        choices=[
            ('', '---None Selected---'),
            ('ServiceAdvisor', 'Service Advisor'),
            ('ServiceAssistant', 'Service Assistant'),
            ('Technician', 'Technician'),
            ('Manager', 'Workshop Management')
        ],
        description='Role determines the rights given to the user how it will be scheduled (if at all).'
    )
    productivity = DecimalField(
        'Productivity Factor',
        validators=[DataRequired()],
        default=1.0,
        description="Productivity determines how much time the technician is expected to require to complete a certain work. To complete 100TU will take 1 hour at productivity 1.0, 30 minutes at productivity 2.0, and 2 hours at productivity 0.5"
    )
    setupTime = IntegerField('Setup Time [min]', default=0)
    qualityInspector = BooleanField(
        'User is Quality Inspector',
        description="Users flagged as quality inspectors will be allowed to confirm a car's quality"
    )
    admin = BooleanField(
        'User has Admin Rights'
    )
    shopCalendar = BooleanField(
        'User represents shop calendar',
        description="This option can only be checked for one user. The availabilities of this user will be considered as the shop's opening times."
    )
    groups = QuerySelectMultipleField(
        query_factory=lambda: Group.query.all(),
        get_label=lambda g: g.name
    )
    allocationPriority = IntegerField(
        'Allocation Priority for Technicians',
        default=0,
        description='Determines, in which order technicians are allocated. Technicians with higher allocation priority are assigned to operations during planning first. Technicians with lower priority are more likely to idle.'
    )


class GroupAddForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    users = QuerySelectMultipleField(
        query_factory=lambda: User.query.filter(User.role == 'Technician').all(),
        get_label=lambda g: g.name,
        #widget=Select2Widget(),
    )
    APOSRegEx = TextAreaField('Possible APOS')


class GroupEditForm(GroupAddForm):
    pass
