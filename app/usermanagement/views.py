from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from flask_login import login_required, login_user, logout_user, current_user
from sqlalchemy.sql import func

from . import usermanagement
from forms import *
from app.calendarmanagement.forms import CalendarEntryForm

from app import connection
from app.db.models import *

from app.simulationmanagement import simulationConnector

from datetime import date


@usermanagement.route("/")
@usermanagement.route("/index")
@login_required
def index():
    if not current_user.is_admin:
        flash("The requsted page is only available for users for admin rights.", 'danger')
        return redirect(url_for('index'))

    return render_template(
        'pages/user.index.html',
        users=User.query.all(),
        groups=Group.query.all(),
        groupAddForm=GroupAddForm()
    )


@usermanagement.route("/add", methods=['GET', 'POST'])
@login_required
def add():
    if not current_user.is_admin:
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))
    form = UserAddForm()
    if form.validate_on_submit():
        # check if there is already a user with that name
        if User.query.filter(User.name == form.name.data).first() is None:
            user = User()
            form.populate_obj(user)

            # if this user is supposed to be represent the shop calendar, set all other users to false
            if user.shopCalendar:
                User.query.update({'shopCalendar': False})

            # form validation already enforced that passwords match, so we do not need to check that here
            connection.session.add(user)
            connection.session.commit()

            # start simulation to reflect changes
            simulationConnector.startSimulation()

            flash('New User {} created.'.format(form.name.data), 'success')

            return redirect(url_for('usermanagement.edit', userNumber=user.id))
        else:
            flash("A user with this name already exists. Please choose unique username.", "warning")

    return render_template(
        'forms/user.add.html',
        form=form
    )


@usermanagement.route("/<int:userNumber>/edit", methods=['GET', 'POST'])
@login_required
def edit(userNumber):
    # non-admin can only edit themselfes. If no user id is given, assume user edits him/herself
    if userNumber is None or not current_user.is_admin:
        userNumber = current_user.id
    # retrieve user from database
    user = User.query.get(userNumber)
    # load form
    form = UserEditForm(obj=user)
    if form.validate_on_submit():
        # store password information
        oldPassword = form.oldPassword.data
        newPassword = form.newPassword.data
        confirm = form.confirm.data
        del form.oldPassword
        del form.newPassword
        del form.confirm

        user.name = form.name.data  # all users are allowed to change their username
        # if the current user is admin, we can take over the remaining entries
        if current_user.is_admin:
            # only allow one user marked as shop calendar
            if form.shopCalendar.data:
                # set all other users to false
                l = User.query.update({'shopCalendar': False})
            else:  # user is not supposed to be shop calendar, make sure that there is another one
                shopCalendarUser = User.query.filter(User.shopCalendar).first()
                if shopCalendarUser == user:  # user to be edited is the shop calendar user and has to remain that
                    form.shopCalendar.data = True
            form.populate_obj(user)
        # set password only if values match
        # only change password, when security is checked:
        if (current_user.is_admin or user.check_password(oldPassword)) and newPassword == confirm and newPassword != '':
            user.set_password(newPassword)
            flash('Password changed', 'success')

        connection.session.commit()

        # start simulation to reflect potential changes
        simulationConnector.startSimulation()

        flash('User Details edited.', 'success')
        return redirect(url_for('usermanagement.edit', userNumber=userNumber))

    # load calendar entries
    calendarEntries = CalendarEntry.query.join(
        CalendarEntry.users
    ).filter(
        User.id == user.id
    ).filter(
        func.date(CalendarEntry.endDatetime) >= date.today()
    ).all()
    # prepopulate form fields before rendering form
    return render_template(
        'forms/user.edit.html',
        form=form,
        form_user_id=userNumber,
        user=user,
        calendarSubscriptionForm=CalendarSubscriptionForm(),
        calendarEntries=calendarEntries,
        calendarEntryAddForm=CalendarEntryForm()
    )


@usermanagement.route("/<int:userNumber>/delete", methods=['GET', 'POST'])
@login_required
def delete(userNumber):
    # retrieve user from database
    user = User.query.get(userNumber)
    # only admins can delete users but not themselves
    if not current_user.is_admin or current_user.id == userNumber:
        flash('You are not allowed to delete this user.', 'warning')
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))
    if user.shopCalendar:
        flash('The user you are trying to delete represents the shop calendar. Deletion is currently not possible.', 'warning')
        return redirect(url_for('usermanagement.index'))

    connection.session.delete(user)
    connection.session.commit()

    # start simulation to reflect changes
    simulationConnector.startSimulation()

    flash('User deleted.')
    return redirect(url_for('usermanagement.index'))


@usermanagement.route("/<int:userNumber>/calendarsubscription/add", methods=['GET', 'POST'])
@login_required
def addCalendarSubscription(userNumber):
    if not current_user.is_admin:
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))
    form = CalendarSubscriptionForm()
    if form.validate_on_submit():
        subscription = CalendarSubscription()
        form.populate_obj(subscription)

        user = User.query.get(userNumber)
        subscription.user = user

        # make sure, we do not allow dates in the past
        subscription.startDate = max(subscription.startDate, date.today())

        connection.session.add(subscription)
        connection.session.commit()

        flash('Calendar subscription added to user {}.'.format(user.name), 'success')

        # start simulation to reflect changes
        simulationConnector.startSimulation()

        return redirect(url_for('usermanagement.edit', userNumber=userNumber))
    else:
        return render_template(
            'forms/calendarSubscription.add.html',
            form=form,
            form_user_id=userNumber
        )


@usermanagement.route("/<int:userNumber>/calendarsubscription/<int:subscriptionNumber>/edit", methods=['GET', 'POST'])
@login_required
def editCalendarSubscription(userNumber, subscriptionNumber):
    subscription = CalendarSubscription.query.get(subscriptionNumber)
    form = CalendarSubscriptionForm(obj=subscription)
    if not current_user.is_admin:
        flash('You are not allowed to perform this operation.', 'warning')
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))
    if form.validate_on_submit():
        # Start and end dates can only be changes within limits
        if form.endDate.data is not None and (subscription.endDate is None or subscription.endDate > date.today()):
            subscription.endDate = max(form.endDate.data, date.today())
        if subscription.startDate >= date.today() and form.startDate.data is not None:
            newEndDate = max(form.startDate.data, date.today())
            if newEndDate > subscription.startDate:
                subscription.startDate = newEndDate
        return redirect(url_for('usermanagement.edit', userNumber=userNumber))

    return render_template(
        'forms/calendarSubscription.edit.html',
        form=form,
        form_user_id=userNumber,
        form_subscription_id=subscriptionNumber
    )


@usermanagement.route("/group/add", methods=['GET', 'POST'])
@login_required
def addGroup():
    if not current_user.is_admin:
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))
    form = GroupAddForm()
    if form.validate_on_submit():
        group = Group()
        form.populate_obj(group)
        connection.session.add(group)
        connection.session.commit()

        flash('New Group {} created.'.format(group.name), 'success')

        # start simulation to reflect changes
        simulationConnector.startSimulation()

        return redirect(url_for('usermanagement.editGroup', groupNumber=group.id))

    return render_template(
        'forms/group.add.html',
        form=form
    )


@usermanagement.route("/group/<int:groupNumber>/edit", methods=['GET', 'POST'])
@login_required
def editGroup(groupNumber):
    if not current_user.is_admin:
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))
    group = Group.query.get(groupNumber)
    form = GroupAddForm(obj=group)
    if form.validate_on_submit():
        form.populate_obj(group)
        connection.session.commit()

        # start simulation to reflect changes
        simulationConnector.startSimulation()

        flash('Group edited', 'success')

    # create list of all covered APOS
    coveredAPOS = filter(
        lambda a: group.canPerformAPOS(a.operationId),
        LaborOperation.query.all()
    )

    return render_template(
        'forms/group.edit.html',
        form=form,
        form_group_id=groupNumber,
        coveredAPOS=coveredAPOS
    )


@usermanagement.route("/group/<int:groupNumber>/delete", methods=['GET'])
@login_required
def deleteGroup(groupNumber):
    if not current_user.is_admin:
        return redirect(url_for('usermanagement.edit', userNumber=current_user.id))
    group = Group.query.get(groupNumber)
    connection.session.delete(group)
    connection.session.commit()

    # start simulation to reflect changes
    simulationConnector.startSimulation()

    flash('Group deleted', 'success')

    return redirect(url_for('usermanagement.index'))
