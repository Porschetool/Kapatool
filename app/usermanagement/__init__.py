from flask import Blueprint

usermanagement = Blueprint(
    'usermanagement',
    __name__,
    template_folder='templates',
    static_folder='static'
)

from . import views

