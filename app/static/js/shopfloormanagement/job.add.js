const apiYears = api + "apos/api/availableyears/";
let availableYears = [];

$(document).ready(function () {
    let modelTypeSelect = $('select#carModel');
    modelTypeSelect.on("change", () => fillAvailableYears());

    let displayedSelectForYear = $('[aria-labelledby="select2-year-container"]');
    displayedSelectForYear.on('click', () => onClickDisplayedSelectForYear());
});

function makeLightGreenBackground(element) {
    let lightGreen = "#D9FBCA";
    element.css("background-color", lightGreen);
    element.css("color", "black");
}

function makeDarkGreenBackground(element) {
    let darkGreenColor = "#23570C";
    element.css("background-color", darkGreenColor);
    element.css("color", "white");
}

//todo: refactor to use the method
function highlightAvailableYears(yearElement) {
    let element = $(yearElement);
    if (availableYears.includes(parseInt(element.text()))) {
        makeLightGreenBackground(element);
        element.hover(makeDarkGreenBackground(element), makeLightGreenBackground(element));
    }
}

function onClickDisplayedSelectForYear() {
    // Maybe doesn't work for Edge, IE. Investigate more. Works for FF, Chrome and Safari.
    //ref: https://hacks.mozilla.org/2012/05/dom-mutationobserver-reacting-to-dom-changes-without-killing-browser-performance/
    //ref: https://stackoverflow.com/questions/7001376/event-to-determine-when-innerhtml-has-loaded (second answer, not accepted one)
    let MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
    let yearsList = document.querySelector('ul#select2-year-results');
    let yearResultsObserver = new MutationObserver(function (mutations) {
        mutations.forEach(function () {
            let yearResults = [].slice.call(yearsList.children);
            yearResults = $(yearResults);
            yearResults.each(function () {
                if (availableYears.includes(parseInt($(this).text()))) {
                    makeLightGreenBackground($(this));
                    $(this).hover(() => makeDarkGreenBackground($(this)), () => makeLightGreenBackground($(this)));
                }
            });
        });
    });

    //todo: handle exception. The if does not work as typeof(yearsList) returns object.
    //If listof available years was appended (would throw an exeption without the if in case of clicking at the input for closing it)
    // if (typeof (yearsList) == Node)
    yearResultsObserver.observe(yearsList, {
        childList: true
    });

    //Todo: extract to separate method (use highlightAvailableYears)
    $(yearsList).find('li').each(function () {
        if (availableYears.includes(parseInt($(this).text()))) {
            makeLightGreenBackground($(this));
            $(this).hover(() => makeDarkGreenBackground($(this)), () => makeLightGreenBackground($(this)));
        }
    });
}

async function fillAvailableYears() {
    let modelTypeSelect = $('select#carModel');
    // Todo: handle failure
    availableYears = await $.get(apiYears + modelTypeSelect.val());
};