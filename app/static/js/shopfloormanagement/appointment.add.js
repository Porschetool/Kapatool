$(function() {
  launchScheduleComponent("#appointments", getAppointmentScheduleData, getAppointmentSchedule);
})

function launchScheduleComponent(elementId, getScheduleData, getSchedule) {
  let elementToPlaceTo = $(elementId);
  let dateTime = getDateTime();

  getScheduleData(dateTime).then(scheduleData => {
    getSchedule(dateTime, scheduleData).then(schedule => {
      scheduleComponent = new ScheduleComponent(
        elementToPlaceTo,
        schedule
      );
      scheduleComponent.init();
    });
  });
}

function getDateTime() {
  //todo: change to get date from datepicker
  let path = window.location.pathname;
  let i = path.lastIndexOf("/");
  let dateTimeString = path.substr(i + 1, path.length);
  let dateTime = null;

  dateTime = moment(dateTimeString);
  if (!dateTime.isValid()) {
    dateTime = moment();
  }
  return dateTime;
}
