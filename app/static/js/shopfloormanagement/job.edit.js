let apiTechnicians = api + "api/technicians";
let apiMachines = api + "api/machines";
let apiPatches = api + "api/patches";

$(document).ready(function() {
  console.log(currentUserRole);
  if (
    "Manager" === currentUserRole
  ) {
    initTechniciansModifiers();
    initMachinesModifiers();
  }
});

function initTechniciansModifiers() {
  technicians = $(".technicians");
  Array.from(technicians).forEach(t => {
    let patchId = $(t).attr("patchid");
    let actionCell = $(".actions[patchid=" + patchId + "]");
    let selectModifier = new SelectModifierComponent(
      $(t),
      $(actionCell),
      apiGetTechniciansCall,
      apiPutTechnicianCall(patchId),
      { cellTooltip: "Modify technician", saveTooltip: "Save technician" }
    );
    selectModifier.init();
  });
}

function initMachinesModifiers() {
  machines = $(".machines");
  Array.from(machines).forEach(m => {
    let patchId = $(m).attr("patchid");
    let actionCell = $(".actions[patchid=" + patchId + "]");
    let selectModifier = new SelectModifierComponent(
      $(m),
      $(actionCell),
      apiGetMachinesCall,
      apiPutMachineCall(patchId),
      { cellTooltip: "Modify machine", saveTooltip: "Save machine" }
    );
    selectModifier.init();
  });
}

function apiGetTechniciansCall() {
  return $.get(apiTechnicians).then(data =>
    data.map(d => ({ value: d.id, text: d.name }))
  );
}

const apiPutTechnicianCall = patchId => chosenValue => {
  return $.ajax({
    url: apiPatches + "/" + patchId + "/technician",
    type: "PUT",
    data: JSON.stringify({ technicianUserId: chosenValue }),
    contentType: "application/json",
    dataType: "text"
  });
};

function apiGetMachinesCall() {
  return $.get(apiMachines).then(data =>
    data.map(d => ({ value: d.id, text: d.name }))
  );
}

const apiPutMachineCall = patchId => chosenValue => {
  return $.ajax({
    url: apiPatches + "/" + patchId + "/machine",
    type: "PUT",
    data: JSON.stringify({ machineId: chosenValue }),
    contentType: "application/json",
    dataType: "text"
  });
};
