//the whole class just moves to moment datetimes. Add it responsibility for the Schedule creation
//or rename it to simple 'schedule datetimes to moment transformer'
class ScheduleBuilder {
    static getSchedule(scheduleData) {
      let scheduleNormalized = ScheduleBuilder.getScheduleFromScheduleJson(
        scheduleData
      );
      let timestamps = scheduleNormalized.timestamps.map(
        ts => new ScheduleTimestamp(ts)
      );
      let resources = scheduleNormalized.resources.map(
        r => new ScheduleSubject(r.id, r.name)
      );
      let availabilities = scheduleNormalized.availabilities.map(
        a =>
          new ScheduleAvailability(
            a.from,
            a.to,
            resources.find(r => r.id === a.resourceId)
          )
      );
      let events = scheduleNormalized.events.map(
        e =>
          new ScheduleEvent(
            e.id,
            e.startDateTime,
            e.endDateTime,
            resources.find(r => r.id === e.resourceId)
          )
      );
      return new Schedule(
        timestamps,
        resources,
        availabilities,
        events,
        "Employee"
      );
    }
  
    static getScheduleFromScheduleJson(schedule) {
      let properSchedule = schedule;
      properSchedule.availabilities = ScheduleBuilder.getAvailabilitiesWithMomentDates(
        schedule.availabilities
      );
      properSchedule.timestamps = ScheduleBuilder.getTimestampsMoment(
        schedule.timestamps
      );
      properSchedule.events = ScheduleBuilder.getEventsWithMomentDates(
        schedule.events
      );
      return properSchedule;
    }
  
    static getEventsWithMomentDates(events) {
      let eventsWithMomentDates = events;
      for (let eventWithMomentDate of eventsWithMomentDates) {
        eventWithMomentDate.startDateTime = moment(
          eventWithMomentDate.startDateTime,
          dateTimePythonFormat
        );
        eventWithMomentDate.endDateTime = moment(
          eventWithMomentDate.endDateTime,
          dateTimePythonFormat
        );
      }
      return eventsWithMomentDates;
    }
  
    static getTimestampsMoment(timestamps) {
      let timestampsMoment = timestamps;
      for (let i = 0; i < timestamps.length; i++) {
        timestampsMoment[i] = moment(timestamps[i], dateTimePythonFormat);
      }
      return timestampsMoment;
    }
  
    static getAvailabilitiesWithMomentDates(availabilities) {
      let availabilitiesWithMomentDates = availabilities;
      for (let availabilityWithMomentDates of availabilitiesWithMomentDates) {
        availabilityWithMomentDates.from = moment(
          availabilityWithMomentDates.from,
          dateTimePythonFormat
        );
        availabilityWithMomentDates.to = moment(
          availabilityWithMomentDates.to,
          dateTimePythonFormat
        );
      }
      return availabilitiesWithMomentDates;
    }
  }
  