class AppointmentEventContentBuilder {
  constructor(appointmentEvent) {
    this.appointmentEvent = appointmentEvent;
  }

  getEventContent() {
    let eventInfo = `<div style='display:inline-block;'>
      ${this.getIcon()}
      <p style="display:inline-block">
      ${this.appointmentEvent.licensePlate} \n (${
      this.appointmentEvent.name
    })  \n (${this.getFormattedEventTime()})
      </p>
      <br>
      <a href=${this.getJobEditHref(this.appointmentEvent.jobId)}>${
      this.appointmentEvent.carModelName
    }</a>
      </div>`;
    return eventInfo;
  }

  getIcon() {
    if (this.appointmentEvent.type == "dropOff") {
      return '<i class="fa fa-level-down"> </i>';
    } else if (this.appointmentEvent.type == "pickUp") {
      return '<i class="fa fa-level-up"></i>';
    }
  }

  //todo: those 2 methods are the same for both patch and appointment events. Maybe extract them somewhere.
  getJobEditHref(jobId) {
    return `/shopfloor/job/${jobId}/edit`;
  }
  getFormattedEventTime() {
    let times = "";
    let timeFormat = "HH:mm";
    times += moment(this.appointmentEvent.startDateTime).format(timeFormat);
    times += "-";
    times += moment(this.appointmentEvent.endDateTime).format(timeFormat);

    return times;
  }
}

class AppointmentEvent {
  constructor(
    id,
    name,
    type,
    isPrepared,
    startDateTime,
    endDateTime,
    licensePlate,
    carModelName,
    jobId
  ) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.isPrepared = isPrepared;
    this.startDateTime = startDateTime;
    this.endDateTime = endDateTime;
    this.licensePlate = licensePlate;
    this.carModelName = carModelName;
    this.jobId = jobId;
  }
}
