let apiGetAppointmentSchedule = api + "shopfloor/api/appointmentschedule";
let apiGetAppointmentEvents = api + "shopfloor/api/appointmentevents";

function getAppointmentScheduleData(dateTime) {
  //todo: change to fetch
  return $.get(apiGetAppointmentSchedule + "/" + dateTime.format("YYYY-MM-DD"));
}

function getAppointmentSchedule(dateTime, scheduleData) {
  //todo: change to fetch
  return $.get(
    apiGetAppointmentEvents + "/" + dateTime.format("YYYY-MM-DD")
  ).then(appointmentEventsData => {
    let appointmentEvents = appointmentEventsData.map(
      aed =>
        new AppointmentEvent(
          aed.id,
          aed.name,
          aed.type,
          aed.isPrepared,
          moment(aed.startDateTime, dateTimePythonFormat),
          moment(aed.endDateTime, dateTimePythonFormat),
          aed.licensePlate,
          aed.carModelName,
          aed.jobId
        )
    );

    let schedule = ScheduleBuilder.getSchedule(scheduleData);
    schedule.events.map(e => {
      let appointmentEvent = appointmentEvents.find(ae => ae.id == e.id);
      e.content = new AppointmentEventContentBuilder(
        appointmentEvent
      ).getEventContent();
      e.classNames = appointmentEvent.isPrepared ? "prepared" : "unprepared";
      if (appointmentEvent.type == "dropOff") {
        e.classNames += " dropoff";
      } else {
        e.classNames += " pickup";
      }
    });

    return schedule;
  });
}
