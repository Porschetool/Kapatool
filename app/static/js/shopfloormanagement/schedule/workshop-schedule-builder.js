let apiGetPatchEvents = api + "shopfloor/api/patchevents";
let apiGetShopSchedule = api + "shopfloor/api/shopschedule";

function getWorkshopScheduleData(dateTime) {
  //todo: change to fetch
  return $.get(apiGetShopSchedule + "/" + dateTime.format("YYYY-MM-DD"));
}

function getWorkshopSchedule(dateTime, scheduleData) {
  //todo: change to fetch
  return $.get(apiGetPatchEvents + "/" + dateTime.format("YYYY-MM-DD")).then(
    patchEventsData => {
      let patchEvents = buildPatchEvents(patchEventsData);
      let schedule = ScheduleBuilder.getSchedule(scheduleData);
      let patchEventContentBuilder = new PatchEventContentBuilder();
      schedule.events.map(e => {
        let patchEvent = patchEvents.find(pe => pe.id == e.id);
        e.content = patchEventContentBuilder.getEventContent(patchEvent);
        e.classNames = patchEvent.status;
        if (patchEvent.job.isReleased) {
          e.classNames += " released";
        }
      });
      return schedule;
    }
  );
}

function buildPatchEvents(patchEventsData) {
  let patchEvents = [];
  for (let patchEventData of patchEventsData.events) {
    patchEvents.push(
      new PatchEvent(
        patchEventData.id,
        moment(patchEventData.startDateTime, dateTimePythonFormat),
        moment(patchEventData.endDateTime, dateTimePythonFormat),
        patchEventData.operation.description,
        patchEventData.status,
        patchEventData.userId,
        patchEventData.operation.id,
        patchEventData.machineId,
        patchEventData.readyToReportWork,
        patchEventData.readyToStartWork,
        patchEventData.materialsReady,
        new Job(
          patchEventData.operation.jobId,
          patchEventData.operation.isReleased,
          patchEventData.operation.jobCarModelName,
          patchEventData.operation.jobLicensePlate
        )
      )
    );
  }
  return patchEvents;
}
