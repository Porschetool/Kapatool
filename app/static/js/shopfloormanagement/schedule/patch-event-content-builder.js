class PatchEventContentBuilder {
  getEventContent(patchEvent) {
    let eventInfo = `<div style="display:inline-block">
      <p>
      ${patchEvent.job.licensePlate} \n (${
      patchEvent.description
    })  \n (${this.getFormattedEventTime(patchEvent)})
      </p>
      <a href=${this.getJobEditHref(patchEvent.job.id)}>${
      patchEvent.job.carModelName
    }</a>
      ${this.getPatchWorkLink(patchEvent)}
      </div>`;
    return eventInfo;
  }

  getJobEditHref(jobId) {
    return `/shopfloor/job/${jobId}/edit`;
  }

  getFormattedEventTime(patchEvent) {
    let times = "";
    let timeFormat = "HH:mm";
    times += moment(patchEvent.startDateTime).format(timeFormat);
    times += "-";
    times += moment(patchEvent.endDateTime).format(timeFormat);

    return times;
  }

  getStartWorkingOnPatchLink(patchEvent) {
    let patchStartingLink = `<a href='/shopfloor/patch/${patchEvent.id}/${
      patchEvent.operationId
    }/${patchEvent.userId}/${
      patchEvent.machineId
    }/start'><br><i class="fa fa-play" aria-hidden="true"></i> Start Working`;
    if (!patchEvent.materialsReady) {
      patchStartingLink += " (Waiting for material!)";
    }
    patchStartingLink += "</a>";
    return patchStartingLink;
  }

  getReportWorkBackLink(patchEvent) {
    return `<a href="/shopfloor/patch/${
      patchEvent.id
    }/end"><i class="fa fa-stop" aria-hidden="true"></i> Report back work</a>`;
  }

  getPatchWorkLink(patchEvent) {
    if (patchEvent.readyToStartWork) {
      return this.getStartWorkingOnPatchLink(patchEvent);
    } else if (patchEvent.readyToReportWork) {
      return this.getReportWorkBackLink(patchEvent);
    } else {
      return "";
    }
  }
}

class PatchEvent {
  constructor(
    id,
    startDateTime,
    endDateTime,
    description,
    status,
    userId,
    operationId,
    machineId,
    readyToReportWork,
    readyToStartWork,
    materialsReady,
    job
  ) {
    this.id = id;
    this.startDateTime = startDateTime;
    this.endDateTime = endDateTime;
    this.description = description;
    this.status = status;
    this.userId = userId;
    this.operationId = operationId;
    this.machineId = machineId;
    this.readyToReportWork = readyToReportWork;
    this.readyToStartWork = readyToStartWork;
    this.materialsReady = materialsReady;
    this.job = job;
  }
}

class Job {
  constructor(id, isReleased, carModelName, licensePlate) {
    this.id = id;
    this.isReleased = isReleased;
    this.carModelName = carModelName;
    this.licensePlate = licensePlate;
  }
}
