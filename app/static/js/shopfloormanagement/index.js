let isScheduleLaunched = [];
let isScheduleShown = false;

$(function() {
  //todo: merge getShopScheduleData and getWorkshopSchedule to one method (getWorkshopSchedule)
  setLaunchOnOpening("#gantt", getWorkshopScheduleData, getWorkshopSchedule);
  setLaunchOnOpening(
    "#appointments",
    getAppointmentScheduleData,
    getAppointmentSchedule
  );
});

function setLaunchOnOpening(elementId, getScheduleData, getSchedule) {
  if (window.location.href.indexOf(elementId) > 0) {
    launch(elementId, getScheduleData, getSchedule);
  }
  $(`a[data-target="${elementId}"]`).on("shown.bs.tab", () => {
    launch(elementId, getScheduleData, getSchedule);
  });
}

function launch(elementId, getScheduleData, getSchedule) {
  if (!isScheduleLaunched[elementId]) {
    launchScheduleComponent(elementId, getScheduleData, getSchedule);
    isScheduleLaunched[elementId] = true;
    // $(`${elementId}`).after(getLegend());
  }
}

function getLegend() {
  let legendLabels = {
    "rgba(243, 226, 138, 1)": "Event Scheduled",
    "rgba(223, 240, 216, 1)": "Event completed",
    "rgba(242, 222, 222, 1)": "Event in progress",
    "rgba(200, 200, 200, 0.4)": "Availability"
  };

  let legend = `<hr/><div id="schedule-legend" class="row text-center" style="margin-left:10px; margin-top:20px">
  <div class="col-xs-2">
  <div class="row">
  <div class="col-xs-12">`;

  for (var key in legendLabels) {
    var value = legendLabels[key];

    legend += `<div class="row">
    <div class="col-cs-12" style="background-color:${key}">${value}</div>
    </div>`;
  }
  return legend;
}

function launchScheduleComponent(elementId, getScheduleData, getSchedule) {
  let elementToPlaceTo = $(elementId);
  let dateTime = getDateTime();
  getScheduleData(dateTime).then(scheduleData => {
    getSchedule(dateTime, scheduleData).then(schedule => {
      scheduleComponent = new ScheduleComponent(
        elementToPlaceTo,
        schedule,
        getScheduleComponentConfigs()
      );
      scheduleComponent.init();
    });
  });
}

function getScheduleComponentConfigs() {
  let scheduleComponentConfigs = new ScheduleComponentConfigs();
  return scheduleComponentConfigs;
}

function getDateTime() {
  //todo: change to get date from datepicker
  let path = window.location.pathname;
  let i = path.lastIndexOf("/");
  let dateTimeString = path.substr(i + 1, path.length);
  let dateTime = null;

  dateTime = moment(dateTimeString);
  if (!dateTime.isValid()) {
    dateTime = moment();
  }
  return dateTime;
}
