/**
 * @author Dimitry Kudrayvtsev
 * @version 2.0
 *
 * Ported to d3 v4 by Keyvan Fatehi on October 16th, 2016; http://bl.ocks.org/baramuyu/ad1f10371a305db47ebaf9779202f7ac
 */

d3.gantt = function(selectorString, dateRangeSelectorString) {
    var FIT_TIME_DOMAIN_MODE = "fit";
    var FIXED_TIME_DOMAIN_MODE = "fixed";

    var selectorString = selectorString;
    var dateRangeSelectorString = dateRangeSelectorString;

    var margin = {
        top : 20,
        right : 40,
        bottom : 20,
        left : 80
    };
    var timeDomainStart = d3.timeDay.offset(new Date(),-3);
    var userTimeDomainStart = timeDomainStart;
    var timeDomainEnd = d3.timeHour.offset(new Date(),+3);
    var userTimeDomainEnd = timeDomainEnd;
    var timeDomainMode = FIT_TIME_DOMAIN_MODE;// fixed or fit
    var taskTypes = [];
    var taskStatus = [];
    var height = d3.select(selectorString).node().getBoundingClientRect().height - margin.top - margin.bottom-5;
    var width = d3.select(selectorString).node().getBoundingClientRect().width - margin.left - margin.right-5;
    var minDate, maxDate;
    
    var highlightString = "";
  
    // Initialize tool tips
    var tool_tip = d3.tip()
        .attr("class", "d3-tip")
        .offset([-8, 0])
        .html(
            function(d) {
                totalSetup = (d.machineSetupTime + d.laborSetupTime)*60;
                return "<table> \
                    <tr><td>Operation:</td><td>" + d.operation + "</td></tr> \
                    <tr><td>Start:</td><td>" + d3.timeFormat("%d.%m.%Y %H:%M")(d.startDate) + "</td></tr> \
                    <tr><td>End:</td><td>" + d3.timeFormat("%d.%m.%Y %H:%M")(d.endDate) + "</td></tr> \
                    <tr><td>Job:</td><td>" + d.job + "</td></tr> \
                    <tr><td>End:</td><td>" + d3.timeFormat("%d.%m.%Y %H:%M")(gantt.getJobEndDate(d.job)) + "</td></tr> \
                    <tr><td>Setup:</td><td>" + totalSetup + " </td></tr> \
                </table>";
            }
        );

    var tickFormat = "%d.%m. %H:%M"; //"%H:%M";

    var keyFunction = function(d) {
        return d.patch;
    };

    var rectTransform = function(d) {
        return "translate(" + x(d.startDate) + "," + y(d.taskName) + ")";
    };

    var x,y,xAxis,yAxis;

    initAxis();

    var initTimeDomain = function() {
        if (timeDomainMode === FIT_TIME_DOMAIN_MODE) {
            if (tasks === undefined || tasks.length < 1) {
                userTimeDomainStart = timeDomainStart = d3.timeDay.offset(new Date(), -3);
                userTimeDomainEnd = timeDomainEnd = d3.timeHour.offset(new Date(), +3);
                return;
            }
            userTimeDomainStart = timeDomainStart = minDate;
            userTimeDomainEnd = timeDomainEnd = maxDate;
        }
    };

    function initAxis() {
        x = d3.scaleTime().domain([ timeDomainStart, timeDomainEnd ]).range([ 0, width ]).clamp(true);
        y = d3.scaleBand().domain(taskTypes).range([ 0, height - margin.top - margin.bottom ]).padding(0.1);

        xAxis = d3.axisBottom()
            .scale(x)
            .tickFormat(d3.timeFormat(tickFormat))
            .tickSize(8)
            .tickPadding(8);
        yAxis = d3.axisLeft().scale(y).tickSize(0);
    };

    function gantt(tasks) {
        // Determine min and max dates in tasks
        maxDate = new Date(
            Math.max.apply(
                null,
                tasks.map(function(i) { return i.endDate; })
            )
        );
        minDate = new Date(
            Math.min.apply(
                null,
                tasks.map(function(i) { return i.startDate; })
            )
        );
        
        
        // Adjust axes
        initTimeDomain();
        initAxis();

        var svg = d3.select(selectorString)
            .attr("class", "chart")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
                .attr("class", "gantt-chart")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
            
        // Initialize Tool Tip
        svg.call(tool_tip);

        svg.selectAll(".chart")
            .data(tasks, keyFunction).enter()
            .append("rect")
            .attr("rx", 2)
            .attr("ry", 2)
            .attr("class", "bar") 
            .attr("y", 0)
            .attr("transform", rectTransform)
            .attr("height", function(d) { return  y.bandwidth(); })
            .attr("width", function(d) { 
                return (x(d.endDate) - x(d.startDate)); 
            })
            .attr("fill", function(d) { return d.color; })
            .on('mouseover', tool_tip.show) // Tool Tip Events
            .on('mouseout', tool_tip.hide);
              
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0, " + (height - margin.top - margin.bottom) + ")")
            .transition()
            .call(xAxis);

        svg.append("g").attr("class", "y axis").transition().call(yAxis);

        return gantt;
    };

    gantt.redraw = function(tasks) {
        initTimeDomain();
        initAxis();

        var svg = d3.select("svg");
        var ganttChartGroup = svg.select(".gantt-chart");
        var rect = ganttChartGroup.selectAll("rect").data(tasks, keyFunction);
        
        /*rect.enter()
          .insert("rect",":first-child")
          .attr("rx", 2)
          .attr("ry", 2)
          .attr("class", function(d){ 
            if(taskStatus[d.status] == null){ return "bar";}
            return taskStatus[d.status];
          }) 
          .transition()
          .attr("y", 0)
          .attr("transform", rectTransform)
          .attr("height", function(d) { return y.bandwidth(); })
          .attr("width", function(d) { 
             return (x(d.endDate) - x(d.startDate)); 
          })
          .attr("fill", function(d) { return d.color; });*/

        rect.merge(rect).transition()
            .attr("transform", rectTransform)
            .attr("height", function(d) { return y.bandwidth(); })
            .attr("width", function(d) { 
                return (x(d.endDate) - x(d.startDate));
            });

        rect.exit().remove();

        svg.select(".x").transition().call(xAxis);
        svg.select(".y").transition().call(yAxis);
        
        // Update date-range indicator
        d3.select(dateRangeSelectorString).html(
            (d3.timeDay.floor(timeDomainStart).getTime() == d3.timeDay.floor(timeDomainEnd).getTime()) ? d3.timeFormat("%d.%m.%Y")(timeDomainStart) : d3.timeFormat("%d.%m.%Y")(timeDomainStart) + " - " + d3.timeFormat("%d.%m.%Y")(timeDomainEnd)
        );

        return gantt;
    };

    gantt.margin = function(value) {
        if (!arguments.length)
            return margin;
        margin = value;
        return gantt;
    };
    
    gantt.userTimeDomain = function(value){
        if (!arguments.length)
            return [ userTimeDomainStart, userTimeDomainEnd ];
        userTimeDomainStart = +value[0], userTimeDomainEnd = +value[1];
        // Adjust start and end dates to avoid blank space on either end
        // find all tasks that are overlapping with the interval or entirely enveloped
        overlappingTasks = tasks.filter(function(i){
            return (i.endDate > userTimeDomainStart) && (i.startDate < userTimeDomainEnd);
        });
        // If there are no tasks, just use the user-provided dates
        if (overlappingTasks.length == 0){
            gantt.timeDomain([userTimeDomainStart, userTimeDomainEnd]);
            return gantt;
        }
        // Otherwise:
        // the adjusted start date is the maximum of the earliest filter startdate and the original startdate
        minimumObservedStartDate = new Date(
            Math.min.apply(
                null,
                overlappingTasks.map(function(i) { return i.startDate; })
            )
        );
        adjustedStartDate = new Date(Math.max(minimumObservedStartDate, userTimeDomainStart));
        // the adjusted end date is the minimum of the latest filtered enddate and the original enddate
        maximumObservedEndDate = new Date(
            Math.max.apply(
                null,
                overlappingTasks.map(function(i) { return i.endDate; })
            )
        );
        adjustedEndDate = new Date(Math.min(maximumObservedEndDate, userTimeDomainEnd));
        // Now, we "beautify" the dates, by rounding to full hours or days (depending on range of values)
        d = d3.timeHour.count(adjustedStartDate, adjustedEndDate); // count range in hours/days
        if(d <= 200)
            timeInterval = d3.timeHour
        else
            timeInterval = d3.timeDay
        gantt.timeDomain([timeInterval.floor(adjustedStartDate), timeInterval.ceil(adjustedEndDate)]);
        return gantt;
    }
    
    // Move the start and end of the user time window by offset units of timeInterval (d3.timeHour, d3.timeDay, d3.timeWeek, ...)
    gantt.moveUserTimeDomain = function(timeInterval, offset) {
        gantt.userTimeDomain([
            timeInterval.offset(userTimeDomainStart, offset),
            timeInterval.offset(userTimeDomainEnd, offset)
        ]);
        return gantt;
    }

    gantt.timeDomain = function(value) {
        if (!arguments.length)
            return [ timeDomainStart, timeDomainEnd ];
        timeDomainStart = +value[0], timeDomainEnd = +value[1];
        gantt.timeDomainMode("fixed"); // As soon as we set start/end times, we have to be in "fixed" mode
        return gantt;
    };

    /**
    * @param {string}
    *                value The value can be "fit" - the domain fits the data or
    *                "fixed" - fixed domain.
    */
    gantt.timeDomainMode = function(value) {
        if (!arguments.length)
            return timeDomainMode;
        timeDomainMode = value;
        return gantt;
    };

    gantt.taskTypes = function(value) {
        if (!arguments.length)
            return taskTypes;
        taskTypes = value;
        return gantt;
    };

    gantt.taskStatus = function(value) {
        if (!arguments.length)
            return taskStatus;
        taskStatus = value;
        return gantt;
    };

    gantt.width = function(value) {
        if (!arguments.length)
            return width;
        width = +value;
        return gantt;
    };

    gantt.height = function(value) {
        if (!arguments.length)
            return height;
        height = +value;
        return gantt;
    };

    gantt.tickFormat = function(value) {
        if (!arguments.length)
            return tickFormat;
        tickFormat = value;
        return gantt;
    };
    
    gantt.highlight = function(str) {
        if (!arguments.length)
            return highlightString;
        highlightString = str;
        
        // Iterate through all drawn bars and add classes, no need to redraw afterwards
        r = new RegExp(highlightString, "gi");  // encode search query in regexp
        d3.selectAll(".bar").filter(function(i) { return i.job.match(r) === null; }).classed("nohighlight", true).classed("highlight", false);
        d3.selectAll(".bar").filter(function(i) { return i.job.match(r); }).classed("highlight", true).classed("nohighlight", false);
        return gantt;
    }
    
    gantt.resetHighlight = function() {
        highlightString = "";
        // remove all highlighting related classes, no need to redraw afterwards
        d3.selectAll(".bar").classed("nohighlight", false).classed("highlight", false);
        return gantt;
    }
    
    gantt.getMinDate = function(){
        return minDate;
    };
        
    gantt.getMaxDate = function(){
        return maxDate;
    };
    
    gantt.getJobEndDate = function(jobName) {
        patchesInJob = tasks.filter(function(i){
            return (i.job == jobName);
        });
        return new Date(
            Math.max.apply(
                null,
                patchesInJob.map(function(i) { return i.endDate; })
            )
        );
    }

  return gantt;
};
