//todo: make popovers not to influence scroll
//todo: remove moment() around each param passing. Do it inside of methods (to copy date, instead of working with passed one) probably...
class ScheduleComponent {
  constructor(elementToPlaceTo, schedule, scheduleComponentConfigs) {
    this.scheduleComponentConfigs = new ScheduleComponentConfigs();

    if (scheduleComponentConfigs) {
      this.scheduleComponentConfigs = scheduleComponentConfigs;
    }

    //set datetimeformat to pass to html
    //ref: http://momentjs.com/guides/#/warnings/js-date/
    this.dateTimeFormatForHtml = "YYYY-MM-DDTHH-mm";
    this.rootElement = $(elementToPlaceTo);
    this.rootElement.addClass("schedule");

    this.schedule = schedule;
    this.timelineElementWidth = this.scheduleComponentConfigs.timelineElementWidth;
  }

  init() {
    if (this.schedule.timestamps.length == 0) {
      this.rootElement.append(`<div class="row"> 
        <div class="col-xs-12 text-center">
        <h2>There is nothing to show for this day</h2>
        </div>
        </div>`);
      return false;
    }

    this.initScheduleLayout();

    this.makeScheduleXScrollable();
    if (this.scheduleComponentConfigs.addNowLine) {
      this.addNowLine();
    }
    if (this.scheduleComponentConfigs.addHourLines) {
      this.addHourLines();
    }
    this.setWidthBasedOnMinutesDuration([
      ...this.rootElement.find(".availability").toArray(),
      ...this.rootElement.find(".event").toArray()
    ]);
    this.setLeftPosition([
      ...this.rootElement.find(".availability").toArray(),
      ...this.rootElement.find(".event").toArray()
    ]);

    this.setRowsHeight(this.scheduleComponentConfigs.rowsHeight);

    this.rootElement.find(".event").each((_, e) => {
      const scheduleEvent = this.schedule.events.find(
        event => event.id == $(e).attr("event-id")
      );
      let eventComponent = new EventComponent(
        e,
        scheduleEvent.content,
        scheduleEvent.classNames
      );
      eventComponent.init();
    });
    return true;
  }

  setLeftPosition(elements) {
    elements = $(elements);
    elements.each((_, e) => {
      e = $(e);
      e.css("position", "absolute");

      e.css(
        "left",
        this.getLeftPosition(
          this.schedule.timestamps[0].dateTime,
          this.getStartDateTimeFromAttribute(e)
        )
      );
    });
  }

  setRowsHeight(height) {
    this.rootElement
      .find(".schedule-content .availabilities-for-subject")
      .each((_, a) => $(a).height(height));
    this.rootElement
      .find(".schedule-content .availabilities-for-subject .availability")
      .each((_, a) => $(a).height(height));
    this.rootElement.find(".subjects .subject").each((_, e) => {
      $(e).height(height - 2);
      $(e).css("line-height", height + "px");
    });
    this.rootElement.find(".event").each((_, e) => $(e).height(height));
  }

  initScheduleLayout() {
    let html = '<div class="container-fluid">';
    html += this.getTimelineHtml();
    html += this.getScheduleContainerHtml();
    html += "</div>";
    this.rootElement.append(html);
    this.rootElement
      .find(".schedule-container")
      .height(this.scheduleComponentConfigs.scheduleHeight);
  }

  setWidthBasedOnMinutesDuration(elements) {
    elements = $(elements);
    elements.each((_, e) =>
      $(e).width(
        this.getEventWidth(
          this.getStartDateTimeFromAttribute(e),
          this.getEndDateTimeFromAttribute(e)
        )
      )
    );
  }

  getEndDateTimeFromAttribute(element) {
    element = $(element);
    return moment($(element).attr("end-datetime"), this.dateTimeFormatForHtml);
  }

  getStartDateTimeFromAttribute(element) {
    element = $(element);
    return moment(
      $(element).attr("start-datetime"),
      this.dateTimeFormatForHtml
    );
  }

  addNowLine() {
    let leftPositionForNowLine = this.getLeftPosition(
      this.schedule.timestamps[0].dateTime,
      moment() //todo: remove (for test) moment("2018-11-07 14:00", "YYYY-MM-DD HH:mm")
    );
    if (leftPositionForNowLine <= this.getTimelineWidth()) {
      //todo:change to add first child to schedule-content
      this.rootElement
        .find(".schedule-content > .availabilities-wrapper")
        .prepend(
          `<div class='now-line' style='left:${leftPositionForNowLine}px'></div>`
        );
    }
  }

  addHourLines() {
    let availabilitiesWrapper = this.rootElement.find(
      ".schedule-content > .availabilities-wrapper"
    );
    for (let hour of this.schedule.timestamps) {
      let leftPositionForHourLine = this.getLeftPosition(
        this.schedule.timestamps[0].dateTime,
        hour.dateTime
      );
      //todo:change to add first child to schedule-content
      availabilitiesWrapper.prepend(
        `<div class='hour-line' style='left:${leftPositionForHourLine}px'></div>`
      );
    }
  }

  makeScheduleXScrollable() {
    let timeline = this.rootElement.find(".timeline-wrapper");

    let availabilitiesContainer = this.rootElement.find(".schedule-content");
    availabilitiesContainer.scroll(function() {
      let availabilitiesScroll = availabilitiesContainer.scrollLeft();
      timeline.scrollLeft(parseInt(availabilitiesScroll));
    });
    this.rootElement
      .find(".schedule-content > .availabilities-wrapper")
      .css("width", this.getTimelineWidth());
  }

  getScheduleContainerHtml() {
    let scheduleContentHtml = '<div class="row schedule-container">';
    scheduleContentHtml += this.getSubjectsHtml();
    scheduleContentHtml += this.getScheduleContentHtml();
    scheduleContentHtml += "</div>";
    return scheduleContentHtml;
  }

  getSubjectsHtml() {
    let subjectsHtml = '<div class="col-md-2 col-xs-2 subjects">';

    for (let subject of this.schedule.subjects) {
      subjectsHtml += '<div class="row subject" subject-id=' + subject.id + ">";
      subjectsHtml +=
        '<div class="col-md-12 col-xs-12"><span>' +
        subject.name +
        "</span></div>";
      subjectsHtml += "</div>";
    }
    subjectsHtml += "</div>";

    return subjectsHtml;
  }

  getScheduleContentHtml() {
    let scheduleContentHtml = `<div class="col-md-10 col-xs-10 schedule-content">
      <div class="availabilities-wrapper">`;
    for (let subject of this.schedule.subjects) {
      let availabilitiesForSubject = this.schedule.availabilities
        .filter(a => a.subject.id == subject.id)
        .sort((a1, a2) => moment(a1.from).diff(moment(a2.from)));
      scheduleContentHtml += '<div class="availabilities-for-subject">';

      for (let availabilityForSubject of availabilitiesForSubject) {
        scheduleContentHtml +=
          '<div class="availability" minutes-duration=' +
          moment(availabilityForSubject.to).diff(
            moment(availabilityForSubject.from),
            "minutes"
          ) +
          " start-datetime=" +
          availabilityForSubject.from.format(this.dateTimeFormatForHtml) +
          " end-datetime=" +
          availabilityForSubject.to.format(this.dateTimeFormatForHtml) +
          " subject-id=" +
          availabilityForSubject.subject.id +
          ">";

        scheduleContentHtml += "</div>";
      }
      scheduleContentHtml += this.getEventsHtml(subject.id);
      scheduleContentHtml += "</div>";
    }
    scheduleContentHtml += "</div>";
    scheduleContentHtml += "</div>";

    return scheduleContentHtml;
  }

  getEventsHtml(subjectId) {
    let eventsHtml = "";

    let eventsForSubject = this.schedule.events.filter(
      event => event.subject.id == subjectId
    );

    for (let eventForSubject of eventsForSubject) {
      eventsHtml +=
        '<div class="event" minutes-duration=' +
        moment(eventForSubject.endDateTime).diff(
          moment(eventForSubject.startDateTime),
          "minutes"
        ) +
        " start-datetime=" +
        eventForSubject.startDateTime.format(this.dateTimeFormatForHtml) +
        " end-datetime=" +
        eventForSubject.endDateTime.format(this.dateTimeFormatForHtml) +
        " event-id=" +
        eventForSubject.id +
        "></div>";
    }
    return eventsHtml;
  }

  getTimelineHtml() {
    let timelineHtml = `<div class="row">
                <div class="col-md-2 col-xs-2 text-center">${
                  this.schedule.subjectName
                }</div>
                    <div class="col-md-10 col-xs-10 timeline-wrapper">
                      <div class="timeline">
                        <ul>`;
    for (let timestamp of this.schedule.timestamps) {
      timelineHtml +=
        '<li style="width:' +
        this.timelineElementWidth +
        'px">' +
        timestamp.dateTime.format("H:mm") +
        "</li>";
    }
    timelineHtml += `</ul>
                    </div>
                  </div>
                </div>`;
    return timelineHtml;
  }

  getTimelineWidth() {
    let timeline = this.rootElement.find(".timeline");
    let timelineWidth = timeline.get(0).scrollWidth;
    return timelineWidth;
  }

  getEventWidth(dateTimeStart, dateTimeEnd) {
    let minutesDuration = dateTimeEnd.diff(dateTimeStart, "minutes");

    let timelineMinutes = this.schedule.timestamps[
      this.schedule.timestamps.length - 1
    ].dateTime.diff(this.schedule.timestamps[0].dateTime, "minutes");

    let pixelsPerMinute =
      (this.getTimelineWidth() - this.timelineElementWidth) / timelineMinutes;

    return pixelsPerMinute * minutesDuration;
  }

  getLeftPosition(relativeZeroTimestamp, startDateTime) {
    return this.getEventWidth(relativeZeroTimestamp, startDateTime);
  }
}

class Schedule {
  constructor(timestamps, subjects, availabilities, events, subjectName) {
    this.timestamps = timestamps;
    this.subjects = subjects;
    this.availabilities = availabilities;
    this.events = events;
    this.subjectName = subjectName;
  }
}

class ScheduleEvent {
  constructor(id, startDateTime, endDateTime, subject, content, classNames) {
    this.id = id;
    this.startDateTime = moment(startDateTime);
    this.endDateTime = moment(endDateTime);
    this.subject = subject;
    this.content = content;
    this.classNames = classNames;
  }
}

class ScheduleAvailability {
  constructor(from, to, subject) {
    this.from = moment(from);
    this.to = moment(to);
    this.subject = subject;
  }
}

class ScheduleTimestamp {
  constructor(dateTime) {
    this.dateTime = moment(dateTime);
  }
}

class ScheduleSubject {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

class EventComponent {
  constructor(eventElement, content, classNames) {
    this.eventElement = $(eventElement);
    this.content = content;
    this.originalWidth = this.eventElement.width();
    this.originalOpacity;
    this.classNames = classNames;
  }

  init() {
    this.eventElement.addClass(this.classNames);

    this.eventElement.append(this.content);
    let contentElement = this.eventElement.find("div");

    if (this.contentNotFits(contentElement)) {
      contentElement.remove();
      this.eventElement.popover({
        html: true,
        placement: this.getPopoverPosition(),
        content: this.content
      });
    }

    this.eventElement.popover("toggle");
    //to make the popover hidden in the beginning
    this.eventElement.popover("hide");

    this.eventElement.hover(
      () => {
        this.originalOpacity = this.eventElement.css("opacity");
        this.eventElement.css("opacity", 1);
      },
      () => this.eventElement.css("opacity", this.originalOpacity)
    );
  }

  contentNotFits(contentElement) {
    return (
      contentElement.width() - this.eventElement.width() > 1 ||
      contentElement.height() >= this.eventElement.height()
    );
  }

  getPopoverPosition() {
    let leftOffset =
      this.eventElement.offset().left -
      this.eventElement.parent().offset().left;
    let rightOffset =
      this.eventElement.parent().width() -
      (leftOffset + this.eventElement.width());

    if (rightOffset > leftOffset) {
      return "right";
    } else {
      return "left";
    }
  }
}

class ScheduleComponentConfigs {
  constructor() {
    this.rowsHeight = 120;
    this.scheduleHeight = "50vh";
    this.addNowLine = true;
    this.addHourLines = true;
    this.timelineElementWidth = 50;
  }

  addNowLine(shouldAdd) {
    this.addNowLine = shouldAdd;
  }

  addHourLines(shouldAdd) {
    this.addHourLines = shouldAdd;
  }

  setRowsHeight(rowsHeight) {
    this.rowsHeight = rowsHeight;
  }

  setScheduleHeight(scheduleHeight) {
    this.scheduleHeight = scheduleHeight;
  }

  setTimelineElementWidth(timeElementWidth) {
    this.timelineElementWidth = timeElementWidth;
  }
}
