// Capacity demand plot
// Plotting based on https://bl.ocks.org/d3noob/0e276dc70bb9184727ee47d6dd06e915
// Area based on https://bl.ocks.org/d3noob/119a138ef9bd1d8f0a8d57ea72355252 but currently not working
d3.capacityDemand = function(selectorString) {
    var margin = {
        top: 20,
        right: 40,
        bottom: 20,
        left: 80
    };
    var height = d3.select(selectorString).node().getBoundingClientRect().height - margin.top - margin.bottom-5;
    var width = d3.select(selectorString).node().getBoundingClientRect().width - margin.left - margin.right-5;
    var dateFormatString = "%d.%m.%Y";
    
    var timeParser = d3.timeParse(dateFormatString);
    var timeFormatter = d3.timeFormat(dateFormatString);
    
    var x,y,xAxis,yAxis;
    
    var selectorString = selectorString;
    
    function capacityDemand(tasks) {
        var minDate = d3.timeYear.offset(Date.now(), +2000);
        var maxDate = d3.timeYear.offset(Date.now(), -2000);
        // aggregate capacity demand per day
        var dailyDemand = {};       
        tasks.forEach(function(i) {
            day = d3.timeDay.floor(i.startDate);
            
            // adjust min/max dates
            minDate = (day < minDate) ? day : minDate;
            maxDate = (day > maxDate) ? day : maxDate;
            
            // Convert Date to string
            day = timeFormatter(day);
            
            // Aggregate capacity demand per day
            demand = d3.timeMinute.count(i.startDate, i.endDate) / 60;
            if (day in dailyDemand)
                dailyDemand[day] += demand;
            else
                dailyDemand[day] = demand;
        });

        // Convert dictionary into JSON Object, filling also the empty spots
        dailyDemandData = [];
        var d = minDate;
        while (d <= maxDate){
            dString = timeFormatter(d);
            dailyDemandData.push({
                day: d,
                demand: (dString in dailyDemand) ? dailyDemand[dString] : 0
            });
            d = d3.timeDay.offset(d, 1)
        }

        // Define the scales
        var x = d3.scaleTime().range([0, width]);
        var y = d3.scaleLinear().range([height, 0]);
        
        /*var area = d3.area()
            .x(function(d) { return x(d.day); })
            .y0(height)
            .y1(function(d) { return y(d.demand); });*/
        
        // define the line
        var valueline = d3.line()
            .curve(d3.curveStepBefore)  // Make Step Function
            .x(function(d) { return x(d.day); })
            .y(function(d) { return y(d.demand); });
            
        
        
        svg = d3.select(selectorString).append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
        // Set the domains
        x.domain(d3.extent(dailyDemandData, function(d) { return d.day; }));
        y.domain([
            0,
            Math.ceil(d3.max(dailyDemandData, function(d) { return d.demand; })/10)*10  // Axis goes to next multiple of 10
        ]);
        
        /*// Add the Area
        svg.append("path")
            .datum([dailyDemandData])
            .attr("class", "area")
            .attr("d", area);*/
        
        // Add the valueline path.
        svg.append("path")
            .data([dailyDemandData])
            .attr("class", "line")
            .attr("d", valueline);

        // Define the axes
        xAxis = d3.axisBottom()
            .scale(x)
            .tickFormat(d3.timeFormat("%d.%m."))
            .tickSize(8)
            .tickPadding(8);
        yAxis = d3.axisLeft().scale(y).tickSize(8);
        
        // Draw the axes
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0, " + height + ")")
            .transition()
            .call(xAxis);
        svg.append("g").attr("class", "y axis").call(yAxis);
    }
    
    return capacityDemand;
}