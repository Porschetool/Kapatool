
/**
 * Makes a cell modifiable via select element (drop down list)
 */
class SelectModifierComponent {
    /**
     * 
     * @param {jqery element} cellToMakeModifiable Cell that is required to make modifiable
     * @param {jqery element} actionCell Cell where save button should be located
     * @param {ajax call} apiGetSelectItemsCall Method to use to get options for drop down list
     * @param {ajax call} apiPutSelectedItemCall Method to use to save selected option (put)
     * @param {SelectModifierData} data object that contains additional info (as tooltips saveTooltip and cellTooltip)
     */
    constructor(cellToMakeModifiable, actionCell, apiGetSelectItemsCall, apiPutSelectedItemCall, data) {
        this._data = data;
        this._apiGetSelectItemsCall = apiGetSelectItemsCall;
        this._apiPutSelectedItemCall = apiPutSelectedItemCall;
        this._actionCell = actionCell;
        this._modifiableCell = cellToMakeModifiable;
        this._saveButton;
        this._previousText;
        this._previousValue;
        this._isModifying;
    };

    init() {
        this._modifiableCell.attr("title", "Modify");
        if (this._data.cellTooltip != undefined) {
            this._modifiableCell.attr("title", this._data.cellTooltip);
        }
        this._modifiableCell.css("cursor", "pointer");
        this._modifiableCell.on("click", () => this.makeModifiable());
    }

    makeModifiable() {
        if (this._isModifying) { return; };
        this._previousText = this._modifiableCell.text();
        this._previousValue = this._modifiableCell.attr("value");
        this._modifiableCell.empty();
        this._modifiableCell.append("<select></select>");

        this.getSelectItems();
        this.addSaveButton();
        this._isModifying = true;
    };

    fillSelect(selectElement, selectItems) {
        let options = '';
        for (let i = 0; i < selectItems.length; i++) {
            if (this._previousValue == selectItems[i].value) {
                options += '<option value=' + selectItems[i].value + ' selected=true>' + selectItems[i].text + '</option>';
            } else {
                options += '<option value=' + selectItems[i].value + '>' + selectItems[i].text + '</option>';
            }
        }
        selectElement.append(options);
    };

    getSelectItems() {
        //todo: maybe rewrite promises to async/await (if it's not TS)
        this._apiGetSelectItemsCall()
            .then((data) => {
                this.fillSelect(this._modifiableCell.children().last(), data);
            })
            .fail(() => {
                this._saveButton.remove();
                this._modifiableCell.empty();
                this._modifiableCell.text(this.previousValue);
                alert("Sorry, there was a server error. Try again later. Report to development team if it happens again.");
            });
    };

    addSaveButton() {
        this._actionCell.append("<div class='saveButton'><i class='fa fa-edit'></i></div>");
        this._saveButton = this._actionCell.children(".saveButton").last();
        this._saveButton.on("click", () => this.save());
        this._saveButton.css("cursor", "pointer");
        this._saveButton.attr("title", "Save");
        if (this._data.saveTooltip != undefined) {
            this._saveButton.attr("title", this._data.saveTooltip);
        }
    };

    save() {
        let chosenValue = this._modifiableCell.find("option:selected");
        this._saveButton.remove();
        this._modifiableCell.empty();
        this._apiPutSelectedItemCall(chosenValue.val())
            .done(() => {
                this._modifiableCell.text(chosenValue.text());
                this._modifiableCell.attr("value", chosenValue.val());
            })
            .fail(() => {
                this._modifiableCell.text(this.previousValue);
                alert("Sorry, there was a server error. Try again later. Report to development team if it happens again.");
            });
        this._isModifying = false;
    };
}