"""
This script creates subprocesses to run all required components (the Pyro nameserver,
the simulation server, and the webserver) in one Terminal.

The processes are started in the right order sequentially into three independent
subprocesses.
"""
import subprocess
import sys
import os
import time

nameServer = None
simulationProvider = None
webServer = None

while True:
    if nameServer is None or nameServer.poll() is not None:
        print 'Starting Nameserver'
        nameServer = subprocess.Popen([sys.executable, '-m', 'Pyro4.naming'])
        print 'Waiting 10 seconds for nameserver to start.'
        time.sleep(10)
    if simulationProvider is None or simulationProvider.poll() is not None:
        print 'Starting simulation server'
        simulationProvider = subprocess.Popen([sys.executable, os.path.dirname(__file__) + '/simulationProvider.py'])
        print 'Waiting 10 seconds for simulation server to start'
        time.sleep(10)
    if webServer is None or webServer.poll() is not None:
        print 'Starting web server'
        webServer = subprocess.Popen([sys.executable, os.path.dirname(__file__) + '/run.py'])
        print 'Waiting 10 seconds for simulation server to start'
        time.sleep(10)
    time.sleep(2)