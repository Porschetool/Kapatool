# Porsche Tool

## Getting Started

Clone this repository and install the dependencies. The following instructions are designed to work on UNIX-based systems, and have been tested on macOS 10.14, Ubuntu 16.04 LTS and Ubuntu 18.04 LTS.
It is recommended to use Atom, with the `atom-ide-ui` and `platformio-ide-terminal` packages installed.

## Directory structure
```
Kapatool
│   README.md                   // This file
|   run.py                      // Script to start the web server
|   simulationProvider.py       // Script to start simulation server
|   config.py                   // Configuration file
|   startAll.py                 // Script to start web server, simulation server and name server concurrently
|   requirements.txt            // Required python packages
|   170620_TUE_Manual.docx      // User documentation for web app
|   
└───app                         // Main web app folder
│   
└───setupData                   // Location of APOS data (to be loaded to DB)
│   
└───tmp                         // Storage location for uploaded files (APOS data ZIP files)
```
## Prerequisites

You need the following software installed on your system:
* [Python 2.7](https://www.python.org)

This project is **_not_** compatible with Python > 3.0.

You should have a good knowledge of Python (Flask in particular), if you want to contribute.

## Server
### Install Server Dependencies

The tools needed for this project can be retrieved using `pip`. To check if `pip` is installed, run the following in the console:
```bash
$ which pip
/path/to/pip    //console output with the location of pip
```
If `pip` is not installed, follow the [instructions](https://pip.pypa.io/en/stable/installing/) provided by the [Python Package Authority](https://www.pypa.io/en/latest/) to install `pip`.

The server dependencies can now be installed using `pip`:
```bash
$ pip install name_of_dependency
```
The dependencies are listed in the `requirements.txt` file, alongside the minimum version of each dependency.

### Run the Application

The project is preconfigured with a development web server. The simplest way to start this server is:

```bash
$ python ./startAll.py
```

Allow the web server, name server and simulation servers to start (about 30 seconds).
Now browse to the app at [`http://0.0.0.0:5000`](http://0.0.0.0:5000).
